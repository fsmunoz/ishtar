# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import re
import django.contrib.gis.db.models.fields
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('archaeological_finds', '0004_auto_20170802_1557'),
    ]

    operations = [
        migrations.AlterField(
            model_name='basefind',
            name='point',
            field=django.contrib.gis.db.models.fields.PointField(srid=4326, dim=3, null=True, verbose_name='Point (3D)', blank=True),
        ),
        migrations.AlterField(
            model_name='batchtype',
            name='txt_idx',
            field=models.CharField(help_text='The slug is the standardized version of the name. It contains only lowercase letters, numbers and hyphens. Each slug must be unique.', unique=True, max_length=100, verbose_name='Textual ID', validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+$'), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')]),
        ),
        migrations.AlterField(
            model_name='conservatorystate',
            name='txt_idx',
            field=models.CharField(help_text='The slug is the standardized version of the name. It contains only lowercase letters, numbers and hyphens. Each slug must be unique.', unique=True, max_length=100, verbose_name='Textual ID', validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+$'), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')]),
        ),
        migrations.AlterField(
            model_name='find',
            name='integrities',
            field=models.ManyToManyField(related_name='find', verbose_name='Integrity / interest', to='archaeological_finds.IntegrityType', blank=True),
        ),
        migrations.AlterField(
            model_name='find',
            name='material_types',
            field=models.ManyToManyField(related_name='finds', verbose_name='Material types', to='archaeological_finds.MaterialType', blank=True),
        ),
        migrations.AlterField(
            model_name='find',
            name='object_types',
            field=models.ManyToManyField(related_name='find', verbose_name='Object types', to='archaeological_finds.ObjectType', blank=True),
        ),
        migrations.AlterField(
            model_name='find',
            name='preservation_to_considers',
            field=models.ManyToManyField(related_name='finds', verbose_name='Type of preservation to consider', to='archaeological_finds.PreservationType', blank=True),
        ),
        migrations.AlterField(
            model_name='find',
            name='remarkabilities',
            field=models.ManyToManyField(related_name='find', verbose_name='Remarkability', to='archaeological_finds.RemarkabilityType', blank=True),
        ),
        migrations.AlterField(
            model_name='historicalbasefind',
            name='point',
            field=django.contrib.gis.db.models.fields.PointField(srid=4326, dim=3, null=True, verbose_name='Point (3D)', blank=True),
        ),
        migrations.AlterField(
            model_name='integritytype',
            name='txt_idx',
            field=models.CharField(help_text='The slug is the standardized version of the name. It contains only lowercase letters, numbers and hyphens. Each slug must be unique.', unique=True, max_length=100, verbose_name='Textual ID', validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+$'), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')]),
        ),
        migrations.AlterField(
            model_name='materialtype',
            name='txt_idx',
            field=models.CharField(help_text='The slug is the standardized version of the name. It contains only lowercase letters, numbers and hyphens. Each slug must be unique.', unique=True, max_length=100, verbose_name='Textual ID', validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+$'), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')]),
        ),
        migrations.AlterField(
            model_name='objecttype',
            name='txt_idx',
            field=models.CharField(help_text='The slug is the standardized version of the name. It contains only lowercase letters, numbers and hyphens. Each slug must be unique.', unique=True, max_length=100, verbose_name='Textual ID', validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+$'), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')]),
        ),
        migrations.AlterField(
            model_name='preservationtype',
            name='txt_idx',
            field=models.CharField(help_text='The slug is the standardized version of the name. It contains only lowercase letters, numbers and hyphens. Each slug must be unique.', unique=True, max_length=100, verbose_name='Textual ID', validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+$'), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')]),
        ),
        migrations.AlterField(
            model_name='remarkabilitytype',
            name='txt_idx',
            field=models.CharField(help_text='The slug is the standardized version of the name. It contains only lowercase letters, numbers and hyphens. Each slug must be unique.', unique=True, max_length=100, verbose_name='Textual ID', validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+$'), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')]),
        ),
        migrations.AlterField(
            model_name='treatmentfiletype',
            name='txt_idx',
            field=models.CharField(help_text='The slug is the standardized version of the name. It contains only lowercase letters, numbers and hyphens. Each slug must be unique.', unique=True, max_length=100, verbose_name='Textual ID', validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+$'), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')]),
        ),
        migrations.AlterField(
            model_name='treatmentstate',
            name='txt_idx',
            field=models.CharField(help_text='The slug is the standardized version of the name. It contains only lowercase letters, numbers and hyphens. Each slug must be unique.', unique=True, max_length=100, verbose_name='Textual ID', validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+$'), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')]),
        ),
        migrations.AlterField(
            model_name='treatmenttype',
            name='txt_idx',
            field=models.CharField(help_text='The slug is the standardized version of the name. It contains only lowercase letters, numbers and hyphens. Each slug must be unique.', unique=True, max_length=100, verbose_name='Textual ID', validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+$'), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')]),
        ),
    ]
