# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-12-01 19:17
from __future__ import unicode_literals

from django.db import migrations


def migrate_containers(apps, schema):
    Find = apps.get_model('archaeological_finds', 'find')
    for f in Find.objects.filter(container__isnull=False).all():
        f.skip_history_when_saving = True
        f.container_ref = f.container
        f.save()
    TreatmentType = apps.get_model('archaeological_finds', 'TreatmentType')
    TreatmentType.objects.get_or_create(
        txt_idx="loan",
        defaults={
            "label": u"Prêt",
            "virtual": False,
            "comment": u"Un prêt est un changement temporaire de contenant "
                       u"pour du mobilier."}
    )
    TreatmentType.objects.get_or_create(
        txt_idx="loan-return",
        defaults={
            "label": u"Retour de prêt",
            "virtual": False,
            "comment": u"Retour de mobilier dans son contenant de référence."}
    )


class Migration(migrations.Migration):

    dependencies = [
        ('archaeological_finds', '0044_auto_20181201_1854'),
    ]

    operations = [
        migrations.RunPython(migrate_containers)
    ]
