# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('archaeological_operations', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('archaeological_warehouse', '0001_initial'),
        ('ishtar_common', '0001_initial'),
        ('archaeological_finds', '0001_initial'),
        ('archaeological_context_records', '0003_auto_20170414_2123'),
    ]

    operations = [
        migrations.AddField(
            model_name='treatmentsource',
            name='authors',
            field=models.ManyToManyField(related_name='treatmentsource_related', verbose_name='Authors', to='ishtar_common.Author'),
        ),
        migrations.AddField(
            model_name='treatmentsource',
            name='format_type',
            field=models.ForeignKey(verbose_name='Format', blank=True, to='ishtar_common.Format', null=True),
        ),
        migrations.AddField(
            model_name='treatmentsource',
            name='source_type',
            field=models.ForeignKey(verbose_name='Type', to='ishtar_common.SourceType'),
        ),
        migrations.AddField(
            model_name='treatmentsource',
            name='support_type',
            field=models.ForeignKey(verbose_name='Support', blank=True, to='ishtar_common.SupportType', null=True),
        ),
        migrations.AddField(
            model_name='treatmentsource',
            name='treatment',
            field=models.ForeignKey(related_name='source', verbose_name='Treatment', to='archaeological_finds.Treatment'),
        ),
        migrations.AddField(
            model_name='treatmentfilesource',
            name='authors',
            field=models.ManyToManyField(related_name='treatmentfilesource_related', verbose_name='Authors', to='ishtar_common.Author'),
        ),
        migrations.AddField(
            model_name='treatmentfilesource',
            name='format_type',
            field=models.ForeignKey(verbose_name='Format', blank=True, to='ishtar_common.Format', null=True),
        ),
        migrations.AddField(
            model_name='treatmentfilesource',
            name='source_type',
            field=models.ForeignKey(verbose_name='Type', to='ishtar_common.SourceType'),
        ),
        migrations.AddField(
            model_name='treatmentfilesource',
            name='support_type',
            field=models.ForeignKey(verbose_name='Support', blank=True, to='ishtar_common.SupportType', null=True),
        ),
        migrations.AddField(
            model_name='treatmentfilesource',
            name='treatment_file',
            field=models.ForeignKey(related_name='source', verbose_name='Treatment request', to='archaeological_finds.TreatmentFile'),
        ),
        migrations.AddField(
            model_name='treatmentfile',
            name='applicant',
            field=models.ForeignKey(related_name='treatmentfile_applicant', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Applicant', blank=True, to='ishtar_common.Person', null=True),
        ),
        migrations.AddField(
            model_name='treatmentfile',
            name='applicant_organisation',
            field=models.ForeignKey(related_name='treatmentfile_applicant', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Applicant organisation', blank=True, to='ishtar_common.Organization', null=True),
        ),
        migrations.AddField(
            model_name='treatmentfile',
            name='history_creator',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Creator', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='treatmentfile',
            name='history_modifier',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Last editor', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='treatmentfile',
            name='imports',
            field=models.ManyToManyField(related_name='imported_archaeological_finds_treatmentfile', to='ishtar_common.Import', blank=True),
        ),
        migrations.AddField(
            model_name='treatmentfile',
            name='in_charge',
            field=models.ForeignKey(related_name='treatmentfile_responsability', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Person in charge', blank=True, to='ishtar_common.Person', null=True),
        ),
        migrations.AddField(
            model_name='treatmentfile',
            name='type',
            field=models.ForeignKey(verbose_name='Treatment request type', to='archaeological_finds.TreatmentFileType'),
        ),
        migrations.AddField(
            model_name='treatment',
            name='container',
            field=models.ForeignKey(verbose_name='Container', blank=True, to='archaeological_warehouse.Container', null=True),
        ),
        migrations.AddField(
            model_name='treatment',
            name='file',
            field=models.ForeignKey(related_name='treatments', verbose_name='Associated request', blank=True, to='archaeological_finds.TreatmentFile', null=True),
        ),
        migrations.AddField(
            model_name='treatment',
            name='history_creator',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Creator', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='treatment',
            name='history_modifier',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Last editor', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='treatment',
            name='imports',
            field=models.ManyToManyField(related_name='imported_archaeological_finds_treatment', to='ishtar_common.Import', blank=True),
        ),
        migrations.AddField(
            model_name='treatment',
            name='location',
            field=models.ForeignKey(blank=True, to='archaeological_warehouse.Warehouse', help_text='Location where the treatment is done. Target warehouse for a move.', null=True, verbose_name='Location'),
        ),
        migrations.AddField(
            model_name='treatment',
            name='organization',
            field=models.ForeignKey(related_name='treatments', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Organization', blank=True, to='ishtar_common.Organization', null=True),
        ),
        migrations.AddField(
            model_name='treatment',
            name='person',
            field=models.ForeignKey(related_name='treatments', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Responsible', blank=True, to='ishtar_common.Person', null=True),
        ),
        migrations.AddField(
            model_name='treatment',
            name='treatment_state',
            field=models.ForeignKey(verbose_name='State', blank=True, to='archaeological_finds.TreatmentState', null=True),
        ),
        migrations.AddField(
            model_name='treatment',
            name='treatment_types',
            field=models.ManyToManyField(to='archaeological_finds.TreatmentType', verbose_name='Treatment type'),
        ),
        migrations.AddField(
            model_name='property',
            name='administrative_act',
            field=models.ForeignKey(verbose_name='Administrative act', to='archaeological_operations.AdministrativeAct'),
        ),
        migrations.AddField(
            model_name='property',
            name='find',
            field=models.ForeignKey(verbose_name='Find', to='archaeological_finds.Find'),
        ),
        migrations.AddField(
            model_name='property',
            name='history_creator',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Creator', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='property',
            name='history_modifier',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Last editor', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='property',
            name='imports',
            field=models.ManyToManyField(related_name='imported_archaeological_finds_property', to='ishtar_common.Import', blank=True),
        ),
        migrations.AddField(
            model_name='property',
            name='person',
            field=models.ForeignKey(related_name='properties', verbose_name='Person', to='ishtar_common.Person'),
        ),
        migrations.AddField(
            model_name='objecttype',
            name='parent',
            field=models.ForeignKey(verbose_name='Parent', blank=True, to='archaeological_finds.ObjectType', null=True),
        ),
        migrations.AddField(
            model_name='materialtype',
            name='parent',
            field=models.ForeignKey(verbose_name='Parent material', blank=True, to='archaeological_finds.MaterialType', null=True),
        ),
        migrations.AddField(
            model_name='historicaltreatmentfile',
            name='applicant',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='ishtar_common.Person', null=True),
        ),
        migrations.AddField(
            model_name='historicaltreatmentfile',
            name='applicant_organisation',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='ishtar_common.Organization', null=True),
        ),
        migrations.AddField(
            model_name='historicaltreatmentfile',
            name='history_creator',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='historicaltreatmentfile',
            name='history_modifier',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='historicaltreatmentfile',
            name='history_user',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='historicaltreatmentfile',
            name='in_charge',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='ishtar_common.Person', null=True),
        ),
        migrations.AddField(
            model_name='historicaltreatmentfile',
            name='type',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='archaeological_finds.TreatmentFileType', null=True),
        ),
        migrations.AddField(
            model_name='historicaltreatment',
            name='container',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='archaeological_warehouse.Container', null=True),
        ),
        migrations.AddField(
            model_name='historicaltreatment',
            name='file',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='archaeological_finds.TreatmentFile', null=True),
        ),
        migrations.AddField(
            model_name='historicaltreatment',
            name='history_creator',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='historicaltreatment',
            name='history_modifier',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='historicaltreatment',
            name='history_user',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='historicaltreatment',
            name='location',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='archaeological_warehouse.Warehouse', null=True),
        ),
        migrations.AddField(
            model_name='historicaltreatment',
            name='organization',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='ishtar_common.Organization', null=True),
        ),
        migrations.AddField(
            model_name='historicaltreatment',
            name='person',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='ishtar_common.Person', null=True),
        ),
        migrations.AddField(
            model_name='historicaltreatment',
            name='treatment_state',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='archaeological_finds.TreatmentState', null=True),
        ),
        migrations.AddField(
            model_name='historicalfind',
            name='collection',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='archaeological_warehouse.Collection', null=True),
        ),
        migrations.AddField(
            model_name='historicalfind',
            name='conservatory_state',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='archaeological_finds.ConservatoryState', null=True),
        ),
        migrations.AddField(
            model_name='historicalfind',
            name='container',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='archaeological_warehouse.Container', null=True),
        ),
        migrations.AddField(
            model_name='historicalfind',
            name='downstream_treatment',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='archaeological_finds.Treatment', null=True),
        ),
        migrations.AddField(
            model_name='historicalfind',
            name='history_creator',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='historicalfind',
            name='history_modifier',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='historicalfind',
            name='history_user',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='historicalfind',
            name='upstream_treatment',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='archaeological_finds.Treatment', null=True),
        ),
        migrations.AddField(
            model_name='historicalbasefind',
            name='batch',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='archaeological_finds.BatchType', null=True),
        ),
        migrations.AddField(
            model_name='historicalbasefind',
            name='context_record',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='archaeological_context_records.ContextRecord', null=True),
        ),
        migrations.AddField(
            model_name='historicalbasefind',
            name='history_creator',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='historicalbasefind',
            name='history_modifier',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='historicalbasefind',
            name='history_user',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='historicalbasefind',
            name='spatial_reference_system',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='ishtar_common.SpatialReferenceSystem', null=True),
        ),
        migrations.AddField(
            model_name='findsource',
            name='authors',
            field=models.ManyToManyField(related_name='findsource_related', verbose_name='Authors', to='ishtar_common.Author'),
        ),
        migrations.AddField(
            model_name='findsource',
            name='find',
            field=models.ForeignKey(related_name='source', verbose_name='Find', to='archaeological_finds.Find'),
        ),
        migrations.AddField(
            model_name='findsource',
            name='format_type',
            field=models.ForeignKey(verbose_name='Format', blank=True, to='ishtar_common.Format', null=True),
        ),
        migrations.AddField(
            model_name='findsource',
            name='source_type',
            field=models.ForeignKey(verbose_name='Type', to='ishtar_common.SourceType'),
        ),
        migrations.AddField(
            model_name='findsource',
            name='support_type',
            field=models.ForeignKey(verbose_name='Support', blank=True, to='ishtar_common.SupportType', null=True),
        ),
        migrations.AddField(
            model_name='findbasket',
            name='items',
            field=models.ManyToManyField(related_name='basket', to='archaeological_finds.Find', blank=True),
        ),
        migrations.AddField(
            model_name='findbasket',
            name='user',
            field=models.ForeignKey(blank=True, to='ishtar_common.IshtarUser', null=True),
        ),
        migrations.AddField(
            model_name='find',
            name='base_finds',
            field=models.ManyToManyField(related_name='find', verbose_name='Base find', to='archaeological_finds.BaseFind'),
        ),
        migrations.AddField(
            model_name='find',
            name='collection',
            field=models.ForeignKey(related_name='finds', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Collection', blank=True, to='archaeological_warehouse.Collection', null=True),
        ),
        migrations.AddField(
            model_name='find',
            name='conservatory_state',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, verbose_name='Conservatory state', blank=True, to='archaeological_finds.ConservatoryState', null=True),
        ),
        migrations.AddField(
            model_name='find',
            name='container',
            field=models.ForeignKey(related_name='finds', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Container', blank=True, to='archaeological_warehouse.Container', null=True),
        ),
        migrations.AddField(
            model_name='find',
            name='datings',
            field=models.ManyToManyField(related_name='find', verbose_name='Dating', to='archaeological_context_records.Dating'),
        ),
        migrations.AddField(
            model_name='find',
            name='downstream_treatment',
            field=models.ForeignKey(related_name='upstream', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Downstream treatment', blank=True, to='archaeological_finds.Treatment', null=True),
        ),
        migrations.AddField(
            model_name='find',
            name='history_creator',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Creator', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='find',
            name='history_modifier',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Last editor', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='find',
            name='imports',
            field=models.ManyToManyField(related_name='imported_archaeological_finds_find', to='ishtar_common.Import', blank=True),
        ),
        migrations.AddField(
            model_name='find',
            name='integrities',
            field=models.ManyToManyField(related_name='find', verbose_name='Integrity / interest', to='archaeological_finds.IntegrityType'),
        ),
        migrations.AddField(
            model_name='find',
            name='material_types',
            field=models.ManyToManyField(related_name='finds', verbose_name='Material types', to='archaeological_finds.MaterialType'),
        ),
        migrations.AddField(
            model_name='find',
            name='object_types',
            field=models.ManyToManyField(related_name='find', verbose_name='Object types', to='archaeological_finds.ObjectType'),
        ),
        migrations.AddField(
            model_name='find',
            name='preservation_to_considers',
            field=models.ManyToManyField(related_name='finds', verbose_name='Type of preservation to consider', to='archaeological_finds.PreservationType'),
        ),
        migrations.AddField(
            model_name='find',
            name='remarkabilities',
            field=models.ManyToManyField(related_name='find', verbose_name='Remarkability', to='archaeological_finds.RemarkabilityType'),
        ),
        migrations.AddField(
            model_name='find',
            name='upstream_treatment',
            field=models.ForeignKey(related_name='downstream', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Upstream treatment', blank=True, to='archaeological_finds.Treatment', null=True),
        ),
        migrations.AddField(
            model_name='conservatorystate',
            name='parent',
            field=models.ForeignKey(verbose_name='Parent conservatory state', blank=True, to='archaeological_finds.ConservatoryState', null=True),
        ),
        migrations.AddField(
            model_name='basefind',
            name='batch',
            field=models.ForeignKey(verbose_name='Batch/object', blank=True, to='archaeological_finds.BatchType', null=True),
        ),
        migrations.AddField(
            model_name='basefind',
            name='context_record',
            field=models.ForeignKey(related_name='base_finds', verbose_name='Context Record', to='archaeological_context_records.ContextRecord'),
        ),
        migrations.AddField(
            model_name='basefind',
            name='history_creator',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Creator', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='basefind',
            name='history_modifier',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Last editor', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='basefind',
            name='imports',
            field=models.ManyToManyField(related_name='imported_archaeological_finds_basefind', to='ishtar_common.Import', blank=True),
        ),
        migrations.AddField(
            model_name='basefind',
            name='spatial_reference_system',
            field=models.ForeignKey(verbose_name='Spatial Reference System', blank=True, to='ishtar_common.SpatialReferenceSystem', null=True),
        ),
        migrations.AlterUniqueTogether(
            name='treatmentfile',
            unique_together=set([('year', 'index')]),
        ),
        migrations.AlterUniqueTogether(
            name='treatment',
            unique_together=set([('year', 'index')]),
        ),
        migrations.AlterUniqueTogether(
            name='findbasket',
            unique_together=set([('label', 'user')]),
        ),
    ]
