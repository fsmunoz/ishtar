# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-12-20 15:39
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ishtar_common', '0079_migrate-importers'),
        ('archaeological_finds', '0052_auto_20181211_1558'),
    ]

    operations = [
        migrations.AddField(
            model_name='find',
            name='main_image',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='main_image_finds', to='ishtar_common.Document', verbose_name='Main image'),
        ),
        migrations.AddField(
            model_name='historicalfind',
            name='main_image',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='ishtar_common.Document'),
        ),
        migrations.AddField(
            model_name='historicaltreatment',
            name='main_image',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='ishtar_common.Document'),
        ),
        migrations.AddField(
            model_name='historicaltreatmentfile',
            name='main_image',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='ishtar_common.Document'),
        ),
        migrations.AddField(
            model_name='treatment',
            name='main_image',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='main_image_treatments', to='ishtar_common.Document', verbose_name='Main image'),
        ),
        migrations.AddField(
            model_name='treatmentfile',
            name='main_image',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='main_image_treatment_files', to='ishtar_common.Document', verbose_name='Main image'),
        ),
    ]
