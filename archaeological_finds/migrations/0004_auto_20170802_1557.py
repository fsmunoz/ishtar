# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('archaeological_finds', '0003_views'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='basefind',
            options={'verbose_name': 'Base find', 'verbose_name_plural': 'Base finds', 'permissions': (('view_basefind', 'Can view all Base finds'), ('view_own_basefind', 'Can view own Base find'), ('add_own_basefind', 'Can add own Base find'), ('change_own_basefind', 'Can change own Base find'), ('delete_own_basefind', 'Can delete own Base find'))},
        ),
        migrations.AlterModelOptions(
            name='find',
            options={'ordering': ('cached_label',), 'verbose_name': 'Find', 'verbose_name_plural': 'Finds', 'permissions': (('view_find', 'Can view all Finds'), ('view_own_find', 'Can view own Find'), ('add_own_find', 'Can add own Find'), ('change_own_find', 'Can change own Find'), ('delete_own_find', 'Can delete own Find'))},
        ),
        migrations.AlterModelOptions(
            name='treatment',
            options={'verbose_name': 'Treatment', 'verbose_name_plural': 'Treatments', 'permissions': (('view_treatment', 'Can view all Treatments'), ('view_own_treatment', 'Can view own Treatment'), ('add_own_treatment', 'Can add own Treatment'), ('change_own_treatment', 'Can change own Treatment'), ('delete_own_treatment', 'Can delete own Treatment'))},
        ),
        migrations.AlterModelOptions(
            name='treatmentfile',
            options={'ordering': ('cached_label',), 'verbose_name': 'Treatment request', 'verbose_name_plural': 'Treatment requests', 'permissions': (('view_filetreatment', 'Can view all Treatment requests'), ('add_filetreatment', 'Can add Treatment request'), ('change_filetreatment', 'Can change Treatment request'), ('delete_filetreatment', 'Can delete Treatment request'), ('view_own_filetreatment', 'Can view own Treatment request'), ('add_own_filetreatment', 'Can add own Treatment request'), ('change_own_filetreatment', 'Can change own Treatment request'), ('delete_own_filetreatment', 'Can delete own Treatment request'))},
        ),
    ]
