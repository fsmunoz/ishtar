# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2019-02-13 16:51
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import ishtar_common.models


class Migration(migrations.Migration):

    dependencies = [
        ('archaeological_finds', '0060_auto_20190206_1522'),
    ]

    operations = [
        migrations.AddField(
            model_name='find',
            name='material_comment',
            field=models.TextField(blank=True, null=True, verbose_name='Comment on the material'),
        ),
        migrations.AddField(
            model_name='historicalfind',
            name='material_comment',
            field=models.TextField(blank=True, null=True, verbose_name='Comment on the material'),
        ),
    ]
