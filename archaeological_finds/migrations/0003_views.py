# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations
from archaeological_finds.models import FirstBaseFindView, FindTreatments, \
    FindDownstreamTreatments, FindUpstreamTreatments, FBulkView, BFBulkView


class Migration(migrations.Migration):

    dependencies = [
        ('archaeological_finds', '0002_auto_20170414_2123'),
    ]

    operations = [
        migrations.RunSQL(FindUpstreamTreatments.CREATE_SQL +
                          FindDownstreamTreatments.CREATE_SQL +
                          FindTreatments.CREATE_SQL +
                          FirstBaseFindView.CREATE_SQL +
                          FBulkView.CREATE_SQL +
                          BFBulkView.CREATE_SQL)
    ]
