#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2015-2017 Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from copy import deepcopy
import datetime
import json

from rest_framework.test import APITestCase
from rest_framework.authtoken.models import Token

from django.conf import settings
from django.contrib.auth.models import User, Permission, ContentType
from django.contrib.gis.geos import GEOSGeometry
from django.core.files import File
from django.core.files.uploadedfile import SimpleUploadedFile
from django.core.urlresolvers import reverse
from django.test.client import Client
from ishtar_common.models import ImporterType, IshtarUser, ImporterColumn,\
    FormaterType, ImportTarget, IshtarSiteProfile, ProfileType
from django.utils.translation import pgettext_lazy, gettext_lazy as _

from ishtar_common.models import Person, get_current_profile, UserProfile, \
    Town, Area, Document, SpatialReferenceSystem
from archaeological_context_records.models import Period, Dating, \
    ContextRecord, DatingType, DatingQuality
from archaeological_finds import models, views
from archaeological_warehouse.models import Warehouse, WarehouseType, \
    ContainerType, Container, WarehouseDivision, WarehouseDivisionLink, \
    ContainerLocalisation
from archaeological_operations.models import Operation, OperationType

from ishtar_common import forms_common

from ishtar_common.tests import WizardTest, WizardTestFormData as FormData, \
    TestCase, create_user, create_superuser, AutocompleteTestBase, AcItem, \
    FIND_FIXTURES, FIND_TOWNS_FIXTURES, WAREHOUSE_FIXTURES, \
    COMMON_FIXTURES, GenericSerializationTest
from archaeological_operations.tests import ImportTest, create_operation
from archaeological_context_records.tests import ContextRecordInit

from archaeological_operations.serializers import operation_serialization
from archaeological_context_records.serializers import cr_serialization
from archaeological_finds import serializers


class FindInit(ContextRecordInit):
    test_context_records = False

    def create_finds(self, data_base=None, data=None, user=None, force=False):
        if not data_base:
            data_base = {}
        if not data:
            data = {}
        if not getattr(self, 'finds', None):
            self.finds = []
        if not getattr(self, 'base_finds', None):
            self.base_finds = []

        default = {'label': "Base find"}
        if user:
            data_base['history_modifier'] = user
        elif not data_base.get('history_modifier') or not data_base[
            'history_modifier'].pk:
            user = self.get_default_user()
            user.save()
            data_base['history_modifier'] = user
        if force or not data_base.get('context_record'):
            data_base['context_record'] = self.get_default_context_record(
                force=force, user=user)
        default.update(data_base)
        base_find = models.BaseFind.objects.create(**default)
        self.base_finds.append(base_find)

        data["history_modifier"] = data_base["history_modifier"]
        find = models.Find.objects.create(**data)
        find.base_finds.add(base_find)
        self.finds.append(find)
        return self.finds, self.base_finds

    def get_default_find(self, force=False):
        finds, base_finds = self.create_finds(force=force)
        if force:
            return finds[-1], base_finds[-1]
        return finds[0], base_finds[0]

    def tearDown(self):
        super(FindInit, self).tearDown()
        if hasattr(self, 'finds'):
            for f in self.finds:
                try:
                    f.delete()
                except:
                    pass
            self.finds = []
        if hasattr(self, 'base_finds'):
            for f in self.base_finds:
                try:
                    f.delete()
                except:
                    pass
            self.base_find = []


class SerializationTest(GenericSerializationTest, FindInit, TestCase):
    fixtures = COMMON_FIXTURES + WAREHOUSE_FIXTURES

    def setUp(self):
        ope1 = self.create_operation()[0]
        ope2 = self.create_operation()[1]
        cr = self.create_context_record(
            data={"label": u"CR 1", "operation": ope1}
        )[0]
        cr2 = self.create_context_record(
            data={"label": u"CR 2", "operation": ope2}
        )[1]
        self.create_finds(data_base={"context_record": cr})[0]
        self.create_finds(data_base={"context_record": cr2})[1]
        # basket = models.FindBasket.objects.create(label="Hophop")
        # basket.items.add(self.finds[0])
        # basket.items.add(self.finds[1])

    def test_serialization(self):
        res = self.generic_serialization_test(serializers.find_serialization)
        find_json = json.loads(
            res[('finds', 'archaeological_finds__Find')]
        )
        self.assertEqual(len(find_json), 2)
        bfind_json = json.loads(
            res[('finds', 'archaeological_finds__BaseFind')]
        )
        self.assertEqual(len(bfind_json), 2)

        result_queryset = Operation.objects.filter(uuid=self.operations[0].uuid)
        res = self.generic_serialization_test(
            serializers.find_serialization, no_test=True,
            kwargs={"operation_queryset": result_queryset}
        )
        find_json = json.loads(
            res[('finds', 'archaeological_finds__Find')]
        )
        self.assertEqual(len(find_json), 1)
        bfind_json = json.loads(
            res[('finds', 'archaeological_finds__BaseFind')]
        )
        self.assertEqual(len(bfind_json), 1)

        result_queryset = ContextRecord.objects.filter(
            uuid=self.context_records[0].uuid)
        res = self.generic_serialization_test(
            serializers.find_serialization, no_test=True,
            kwargs={"cr_queryset": result_queryset}
        )
        find_json = json.loads(
            res[('finds', 'archaeological_finds__Find')]
        )
        self.assertEqual(len(find_json), 1)
        bfind_json = json.loads(
            res[('finds', 'archaeological_finds__BaseFind')]
        )
        self.assertEqual(len(bfind_json), 1)

        result_queryset = models.Find.objects.filter(
            uuid=self.finds[0].uuid)
        res = self.generic_serialization_test(
            serializers.find_serialization, no_test=True,
            kwargs={"find_queryset": result_queryset}
        )
        find_json = json.loads(
            res[('finds', 'archaeological_finds__Find')]
        )
        self.assertEqual(len(find_json), 1)
        bfind_json = json.loads(
            res[('finds', 'archaeological_finds__BaseFind')]
        )
        self.assertEqual(len(bfind_json), 1)

    def test_ope_serialization_with_find_filter(self):
        res = self.generic_serialization_test(
            operation_serialization, no_test=True,
        )
        ope_json = json.loads(
            res[('operations', 'archaeological_operations__Operation')]
        )
        self.assertEqual(len(ope_json), 2)

        result_queryset = models.Find.objects.filter(
            uuid=self.finds[0].uuid)
        res = self.generic_serialization_test(
            operation_serialization, no_test=True,
            kwargs={"find_queryset": result_queryset}
        )
        ope_json = json.loads(
            res[('operations', 'archaeological_operations__Operation')]
        )
        self.assertEqual(len(ope_json), 1)

    def test_cr_serialization_with_find_filter(self):
        res = self.generic_serialization_test(
            cr_serialization, no_test=True,
        )
        cr_json = json.loads(
            res[('context_records',
                 'archaeological_context_records__ContextRecord')]
        )
        self.assertEqual(len(cr_json), 2)

        result_queryset = models.Find.objects.filter(
            uuid=self.finds[0].uuid)
        res = self.generic_serialization_test(
            cr_serialization, no_test=True,
            kwargs={"find_queryset": result_queryset}
        )
        cr_json = json.loads(
            res[('context_records',
                 'archaeological_context_records__ContextRecord')]
        )
        self.assertEqual(len(cr_json), 1)

    def test_restore(self):
        current_number, zip_filename = self.generic_restore_test_genzip(
            serializers.FIND_MODEL_LIST,
            serializers.find_serialization)
        self.generic_restore_test(zip_filename, current_number,
                                  serializers.FIND_MODEL_LIST)


class FindWizardCreationTest(WizardTest, FindInit, TestCase):
    fixtures = WAREHOUSE_FIXTURES
    url_name = 'find_creation'
    wizard_name = 'find_wizard'
    steps = views.find_creation_steps
    redirect_url = "/find_modification/selec-find_modification?open_item="\
                   "{last_id}"
    model = models.Find
    form_datas = [
        FormData(
            'Find creation',
            form_datas={
                'selecrecord-find_creation': {'pk': 1},
                'find-find_creation': {
                    'label': 'hop',
                    'checked': 'NC',
                    'check_date': '2016-01-01'
                },
                'dating-find_creation': [
                    {
                        'period': None,
                        'start_date': '0',
                        'end_date': '200',
                    },
                    {
                        'period': None,
                        'start_date': '0',
                        'end_date': '200',
                    }
                ]
            },
            ignored=['preservation-find_creation']
        )
    ]

    def pre_wizard(self):
        cr = self.create_context_record(
            data={'parcel': self.create_parcel()[-1]}, force=True)[-1]

        self.form_datas[0].form_datas['selecrecord-find_creation']['pk'] = \
            cr.pk
        period = Period.objects.all()[0].pk
        self.form_datas[0].form_datas['dating-find_creation'][0]['period'] = \
            period
        self.form_datas[0].form_datas['dating-find_creation'][1]['period'] = \
            period
        self.find_number = models.Find.objects.count()
        self.basefind_number = models.BaseFind.objects.count()
        super(FindWizardCreationTest, self).pre_wizard()

    def post_wizard(self):
        self.assertEqual(models.BaseFind.objects.count(),
                         self.basefind_number + 1)
        self.assertEqual(models.Find.objects.count(),
                         self.find_number + 1)
        # identical datings, only one should be finaly save
        f = models.Find.objects.order_by("-pk").all()[0]
        self.assertEqual(f.datings.count(), 1)


class FindWizardDeletionWithWarehouseModTest(WizardTest, FindInit, TestCase):
    fixtures = WAREHOUSE_FIXTURES
    url_name = 'find_deletion'
    wizard_name = 'find_deletion_wizard'
    steps = views.find_deletion_steps
    redirect_url = "/{}/selecw-{}".format(url_name, url_name)
    form_datas = [
        FormData(
            'Find deletion',
            form_datas={
                'selecw': {},
            },
            ignored=['selec-find_deletion']
        )
    ]

    def pre_wizard(self):
        profile, created = IshtarSiteProfile.objects.get_or_create(
            slug='default', active=True)
        profile.warehouse = True
        profile.save()

        find, bf = self.get_default_find(force=True)
        self.form_datas[0].set('selecw', 'pks', find.pk)
        self.find_number = models.Find.objects.count()
        super(FindWizardDeletionWithWarehouseModTest, self).pre_wizard()

    def post_wizard(self):
        self.assertEqual(models.Find.objects.count(),
                         self.find_number - 1)


class TreatmentWizardCreationTest(WizardTest, FindInit, TestCase):
    fixtures = WAREHOUSE_FIXTURES
    url_name = 'treatment_creation'
    wizard_name = 'treatment_wizard'
    steps = views.treatment_wizard_steps
    redirect_url = "/treatment_search/general-treatment_search?" \
                   "open_item={last_id}"
    model = models.Treatment
    form_datas = [
        FormData(
            'Move treatment (planned)',
            form_datas={
                'file': {},
                'basetreatment': {
                    'treatment_type': None,
                    'person': 1,  # doer
                    'location': 1,  # associated warehouse
                    'year': 2016,
                    'container': None,
                },
                'selecfind': {
                    'pk': 1,
                    'resulting_pk': 1
                }
            },
            ignored=('resultfind-treatment_creation',
                     'selecbasket-treatment_creation',
                     'resultfinds-treatment_creation')),
        FormData(
            'Move treatment (done)',
            form_datas={
                'file': {},
                'basetreatment': {
                    'treatment_type': None,
                    'person': 1,  # doer
                    'location': 1,  # associated warehouse
                    'year': 2016,
                    'container': None,
                },
                'selecfind': {
                    'pk': 1,
                    'resulting_pk': 1
                }
            },
            ignored=('resultfind-treatment_creation',
                     'selecbasket-treatment_creation',
                     'resultfinds-treatment_creation')),
        FormData(
            'Loan treatment (done)',
            form_datas={
                'file': {},
                'basetreatment': {
                    'treatment_type': None,
                    'person': 1,  # doer
                    'location': 1,  # associated warehouse
                    'year': 2016,
                    'container': None,
                },
                'selecfind': {
                    'pk': 1,
                    'resulting_pk': 1
                }
            },
            ignored=('resultfind-treatment_creation',
                     'selecbasket-treatment_creation',
                     'resultfinds-treatment_creation')),
    ]

    def pre_wizard(self):
        q = Warehouse.objects.filter(pk=1)
        if not q.count():
            warehouse = Warehouse.objects.create(
                name="default", warehouse_type=WarehouseType.objects.all()[0])
            warehouse.id = 1
            warehouse.save()
        else:
            warehouse = q.all()[0]
        q = Person.objects.filter(pk=1)
        if not q.count():
            person = Person.objects.create(name="default")
            person.id = 1
            person.save()

        self.container_ref = Container.objects.create(
            location=warehouse,
            responsible=warehouse,
            container_type=ContainerType.objects.all()[0]
        )

        self.container1 = Container.objects.create(
            location=warehouse,
            responsible=warehouse,
            container_type=ContainerType.objects.all()[0]
        )

        self.container2 = Container.objects.create(
            location=warehouse,
            responsible=warehouse,
            container_type=ContainerType.objects.all()[0]
        )

        self.find, base_find = self.get_default_find(force=True)
        self.find.container_ref = self.container_ref
        self.find.container = self.container_ref
        self.find.save()
        for idx in (0, 1):
            self.form_datas[idx].form_datas['selecfind']["pk"] = self.find.pk
            self.form_datas[idx].form_datas['selecfind']["resulting_pk"] = \
                self.find.pk
            self.form_datas[idx].set('basetreatment', 'container',
                                     self.container1.pk)

        self.find_2, base_find = self.get_default_find(force=True)
        self.find_2.container_ref = self.container_ref
        self.find_2.container = self.container_ref
        self.find_2.save()
        self.form_datas[2].form_datas['selecfind']["pk"] = self.find_2.pk
        self.form_datas[2].form_datas['selecfind']["resulting_pk"] = \
            self.find_2.pk
        self.form_datas[2].set('basetreatment', 'container', self.container2.pk)

        moving = models.TreatmentType.objects.get(txt_idx='moving')
        moving.change_reference_location = True
        moving.change_current_location = True
        moving.save()
        self.form_datas[0].set('basetreatment', 'treatment_type',
                               moving.pk)
        self.form_datas[1].set('basetreatment', 'treatment_type',
                               moving.pk)
        loan = models.TreatmentType.objects.get(txt_idx='loan')
        loan.change_reference_location = False
        loan.change_current_location = True
        loan.save()
        self.form_datas[2].set('basetreatment', 'treatment_type',
                               loan.pk)

        planned, __ = models.TreatmentState.objects.get_or_create(
            txt_idx='planned', defaults={"executed": False, "label": u"Planned"}
        )
        planned.executed = False
        planned.save()
        self.form_datas[0].set('basetreatment', 'treatment_state', planned.pk)

        completed, created = models.TreatmentState.objects.get_or_create(
            txt_idx='completed', defaults={"executed": True, "label": u"Done"}
        )
        completed.executed = True
        completed.save()
        self.form_datas[1].set('basetreatment', 'treatment_state', completed.pk)
        self.form_datas[2].set('basetreatment', 'treatment_state', completed.pk)

        self.treatment_number = models.Treatment.objects.count()

        def post_first_wizard(test_object, final_step_response):
            # treatment planned - no changes
            find = models.Find.objects.get(pk=test_object.find.pk)
            test_object.assertEqual(find.container, test_object.container_ref)
            test_object.assertEqual(find.container_ref,
                                    test_object.container_ref)
        self.form_datas[0].extra_tests = [post_first_wizard]

        def post_second_wizard(test_object, final_step_response):
            # moving done
            find = models.Find.objects.get(pk=test_object.find.pk)
            test_object.assertEqual(find.container, test_object.container1)
            test_object.assertEqual(find.container_ref, test_object.container1)
        self.form_datas[1].extra_tests = [post_second_wizard]

        def post_third_wizard(test_object, final_step_response):
            # loan done
            find = models.Find.objects.get(pk=test_object.find_2.pk)
            test_object.assertEqual(find.container_ref,
                                    test_object.container_ref)
            test_object.assertEqual(find.container, test_object.container2)
        self.form_datas[2].extra_tests = [post_third_wizard]

        super(TreatmentWizardCreationTest, self).pre_wizard()

    def post_wizard(self):
        self.assertEqual(models.Treatment.objects.count(),
                         self.treatment_number + 3)
        self.find = models.Find.objects.get(pk=self.find.pk)
        self.assertEqual(self.find.treatments.count(), 2)
        self.find_2 = models.Find.objects.get(pk=self.find_2.pk)
        self.assertEqual(self.find_2.treatments.count(), 1)
        # TODO: test treatment with new find creation
        # treat = models.Treatment.objects.order_by('-pk').all()[0]
        # self.assertEqual(self.find.downstream_treatment,
        #                 treat)


class ImportFindTest(ImportTest, TestCase):
    fixtures = FIND_TOWNS_FIXTURES

    def test_mcc_import_finds(self):
        self.init_context_record()

        old_nb = models.BaseFind.objects.count()
        old_nb_find = models.Find.objects.count()
        MCC = ImporterType.objects.get(name=u"MCC - Mobilier")

        col = ImporterColumn.objects.create(col_number=25,
                                            importer_type_id=MCC.pk)
        formater = FormaterType.objects.filter(
            formater_type='FileFormater').all()[0]
        ImportTarget.objects.create(target='documents__image',
                                    formater_type_id=formater.pk,
                                    column_id=col.pk)
        mcc_file = open(
            settings.ROOT_PATH +
            '../archaeological_finds/tests/MCC-finds-example.csv', 'rb')
        mcc_images = open(
            settings.ROOT_PATH +
            '../archaeological_finds/tests/images.zip', 'rb')
        file_dict = {'imported_file': SimpleUploadedFile(mcc_file.name,
                                                         mcc_file.read()),
                     'imported_images': SimpleUploadedFile(mcc_images.name,
                                                           mcc_images.read())}
        post_dict = {'importer_type': MCC.pk, 'skip_lines': 1,
                     "encoding": 'utf-8', "name": 'init_find_import',
                     "csv_sep": u","}
        form = forms_common.NewImportForm(data=post_dict, files=file_dict,
                                          user=self.user)
        form.is_valid()
        self.assertTrue(form.is_valid())
        impt = form.save(self.ishtar_user)
        impt.initialize()

        # doing manual connections
        ceram = models.MaterialType.objects.get(txt_idx='ceramic').pk
        glass = models.MaterialType.objects.get(txt_idx='glass').pk
        self.set_target_key('material_types', 'terre-cuite', ceram)
        self.set_target_key('material_types', 'verre', glass)
        impt.importation()
        # new finds has now been imported
        current_nb = models.BaseFind.objects.count()
        self.assertEqual(current_nb, (old_nb + 4))
        current_nb = models.Find.objects.count()
        self.assertEqual(current_nb, (old_nb_find + 4))
        self.assertEqual(
            models.Find.objects.filter(material_types__pk=ceram).count(), 4)
        self.assertEqual(
            models.Find.objects.filter(material_types__pk=glass).count(), 1)
        images = []
        for find in models.Find.objects.all():
            images += [1 for im in find.images.all() if im.image.name]
        self.assertEqual(len(images), 1)

        # check index
        bfs = list(models.BaseFind.objects.order_by("-pk").all())
        for idx in range(4):
            bf = bfs[idx]
            expected_index = 4 - idx
            self.assertEqual(
                bf.index, expected_index,
                "Bad index for imported base find: {} where {} is "
                "expected".format(bf.index, expected_index))
            f = bf.find.all()[0]
            self.assertEqual(
                f.index, expected_index,
                "Bad index for imported find: {} where {} is expected".format(
                    f.index, expected_index
                ))


class FindTest(FindInit, TestCase):
    fixtures = FIND_FIXTURES
    model = models.Find

    def setUp(self):
        self.create_finds(force=True)
        self.password = 'mypassword'
        self.username = 'myuser'
        User.objects.create_superuser(self.username, 'myemail@test.com',
                                      self.password)
        self.client = Client()
        self.client.login(username=self.username, password=self.password)

    def test_external_id(self):
        find = self.finds[0]
        base_find = find.base_finds.all()[0]
        self.assertEqual(
            find.external_id,
            u"{}-{}".format(
                find.get_first_base_find().context_record.external_id,
                find.label))
        self.assertEqual(
            base_find.external_id,
            u"{}-{}".format(
                base_find.context_record.external_id,
                base_find.label))
        base_find.label = "New label"
        base_find.save()
        base_find = models.BaseFind.objects.get(pk=base_find.pk)
        self.assertEqual(
            base_find.external_id,
            u"{}-{}".format(
                base_find.context_record.external_id,
                "New label"))
        cr = ContextRecord.objects.get(pk=base_find.context_record.pk)
        cr.label = "new-label-too"
        cr.skip_history_when_saving = True
        cr.save()
        base_find = models.BaseFind.objects.get(pk=base_find.pk)
        find = models.Find.objects.get(pk=find.pk)
        cr = ContextRecord.objects.get(pk=cr.pk)
        self.assertIn("new-label-too", find.external_id)
        self.assertIn("new-label-too", base_find.external_id)

        cr.operation.code_patriarche = "PAT"
        cr.operation.skip_history_when_saving = True
        cr.operation.save()
        base_find = models.BaseFind.objects.get(pk=base_find.pk)
        find = models.Find.objects.get(pk=find.pk)
        cr = ContextRecord.objects.get(pk=cr.pk)
        self.assertIn("PAT", find.external_id)
        self.assertIn("PAT", base_find.external_id)

        find.label = "hop"
        find.save()
        find = models.Find.objects.get(pk=find.pk)
        # default: {get_first_base_find__context_record__external_id}-{label}
        self.assertEqual(find.external_id, u"PAT-12345-A1-new-label-too-hop")
        profile = get_current_profile(force=True)
        profile.find_external_id = \
            u"{get_first_base_find__context_record__external_id}-{label}-"\
            u"{label}"
        profile.save()
        find.save()
        find = models.Find.objects.get(pk=find.pk)
        self.assertEqual(find.external_id,
                         u"PAT-12345-A1-new-label-too-hop-hop")
        profile.find_external_id = \
            u"{get_first_base_find__context_record__external_id}-{label}-" \
            u"{label}||lower||deduplicate"
        profile.save()
        find.save()
        find = models.Find.objects.get(pk=find.pk)
        self.assertEqual(find.external_id,
                         u"pat-12345-a1-new-label-too-hop")

    def testIndex(self):
        profile = get_current_profile()
        profile.find_index = u"O"
        profile.save()
        get_current_profile(force=True)

        op1 = self.create_operation()[-1]
        op2 = self.create_operation()[-1]
        op1_cr1 = self.create_context_record(data={'label': "CR1",
                                                   'operation': op1})[-1]
        op1_cr2 = self.create_context_record(data={'label': "CR2",
                                                   'operation': op1})[-1]
        op2_cr1 = self.create_context_record(data={'label': "CR3",
                                                   'operation': op2})[-1]
        self.create_finds(data_base={'context_record': op1_cr1})
        find_1 = self.finds[-1]
        bf_1 = models.BaseFind.objects.get(pk=self.base_finds[-1].pk)
        self.assertEqual(find_1.index, 1)
        self.assertEqual(bf_1.index, 1)

        # index is based on operations
        self.create_finds(data_base={'context_record': op1_cr2})
        find_2 = self.finds[-1]
        bf_2 = models.BaseFind.objects.get(pk=self.base_finds[-1].pk)
        self.assertEqual(find_2.index, 2)
        self.assertEqual(bf_2.index, 2)

        self.create_finds(data_base={'context_record': op2_cr1})
        find_3 = self.finds[-1]
        bf_3 = models.BaseFind.objects.get(pk=self.base_finds[-1].pk)
        self.assertEqual(find_3.index, 1)
        self.assertEqual(bf_3.index, 1)

        profile = get_current_profile(force=True)
        profile.find_index = u"CR"
        profile.save()
        profile = get_current_profile(force=True)

        op3 = self.create_operation()[-1]
        op3_cr1 = self.create_context_record(data={'label': "CR1",
                                                   'operation': op3})[-1]
        op3_cr2 = self.create_context_record(data={'label': "CR2",
                                                   'operation': op3})[-1]
        self.create_finds(data_base={'context_record': op3_cr1})
        find_1b = self.finds[-1]
        bf_1b = models.BaseFind.objects.get(pk=self.base_finds[-1].pk)
        self.assertEqual(find_1b.index, 1)
        self.assertEqual(bf_1b.index, 1)

        # index now based on context records
        self.create_finds(data_base={'context_record': op3_cr2})
        find_2b = self.finds[-1]
        bf_2b = models.BaseFind.objects.get(pk=self.base_finds[-1].pk)
        self.assertEqual(find_2b.index, 1)
        self.assertEqual(bf_2b.index, 1)

        self.create_finds(data_base={'context_record': op3_cr2})
        find_3b = self.finds[-1]
        bf_3b = models.BaseFind.objects.get(pk=self.base_finds[-1].pk)
        self.assertEqual(find_3b.index, 2)
        self.assertEqual(bf_3b.index, 2)

    def test_show(self):
        obj = self.finds[0]
        response = self.client.get(reverse('display-find', args=[obj.pk]))
        self.assertEqual(response.status_code, 200)
        self.assertIn('load_window("/show-find/{}/");'.format(obj.pk),
                      response.content.decode())
        c = Client()
        response = c.get(reverse('show-find', kwargs={'pk': obj.pk}))
        # empty content when not allowed
        self.assertRedirects(response, "/")

        c.login(username=self.username, password=self.password)
        response = self.client.get(reverse('show-find', kwargs={'pk': obj.pk}))
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'class="card sheet"', response.content)

    def test_delete(self):
        self.create_finds(force=True)
        first_bf = self.base_finds[0]
        self.finds[1].base_finds.add(first_bf)

        self.finds[0].delete()
        # on delete the selected base find is not deleted if another find
        # is related to it
        self.assertEqual(models.BaseFind.objects.filter(
            pk=self.base_finds[0].pk).count(), 1)
        self.finds[1].delete()
        self.assertEqual(models.BaseFind.objects.filter(
            pk=self.base_finds[0].pk).count(), 0)


class FindSearchTest(FindInit, TestCase):
    fixtures = WAREHOUSE_FIXTURES
    model = models.Find

    def setUp(self):
        self.create_finds(force=True)
        self.create_finds(force=True)
        self.username = 'myuser'
        self.password = 'mypassword'
        User.objects.create_superuser(self.username, 'myemail@test.com',
                                      self.password)
        self.client = Client()

    def test_material_type_hierarchic_search(self):
        find = self.finds[0]
        c = Client()
        metal = models.MaterialType.objects.get(txt_idx='metal')
        iron_metal = models.MaterialType.objects.get(txt_idx='iron_metal')
        not_iron_metal = models.MaterialType.objects.get(
            txt_idx='not_iron_metal')
        find.material_types.add(iron_metal)
        find = models.Find.objects.get(pk=find.pk)
        find.save()

        search = {'material_types': iron_metal.pk}

        # no result when no authentication
        response = c.get(reverse('get-find'), search)
        self.assertRedirects(response, "/")

        c.login(username=self.username, password=self.password)

        # one result for exact search
        response = c.get(reverse('get-find'), search)
        self.assertEqual(response.status_code, 200)
        content = response.content.decode()
        res = json.loads(content)
        self.assertEqual(res['recordsTotal'], 1)
        self.assertEqual(res["rows"][0]["cached_materials"],
                         str(iron_metal))

        # no result for the brother
        search = {'material_types': not_iron_metal.pk}
        response = c.get(reverse('get-find'), search)
        self.assertEqual(response.status_code, 200)
        content = response.content.decode()
        self.assertEqual(json.loads(content)['recordsTotal'], 0)

        # one result for the father
        search = {'material_types': metal.pk}
        response = c.get(reverse('get-find'), search)
        self.assertEqual(response.status_code, 200)
        content = response.content.decode()
        self.assertEqual(json.loads(content)['recordsTotal'], 1)

    def test_pinned_search(self):
        c = Client()
        c.login(username=self.username, password=self.password)

        # operation with no associated find
        operation = create_operation(self.user, values={"year": 2017})
        c.get(reverse("pin", args=["operation", operation.pk]))
        response = c.get(reverse('get-find'), {})  # empty search -> check pined
        self.assertEqual(response.status_code, 200)
        content = response.content.decode()
        self.assertEqual(json.loads(content)['recordsTotal'], 0)

        # pinned operation with current find
        find = self.finds[0]
        c.get(reverse(
            "pin",
            args=["operation",
                  find.get_first_base_find().context_record.operation.pk]))
        response = c.get(reverse('get-find'), {})  # empty search -> check pined
        self.assertEqual(response.status_code, 200)
        content = response.content.decode()
        self.assertEqual(json.loads(content)['recordsTotal'], 1)

    def test_period_hierarchic_search(self):
        find = self.finds[0]
        c = Client()

        neo = Period.objects.get(txt_idx='neolithic')
        final_neo = Period.objects.get(txt_idx='final-neolithic')
        recent_neo = Period.objects.get(txt_idx='recent-neolithic')
        dating = Dating.objects.create(
            period=final_neo
        )
        find.datings.add(dating)
        find = models.Find.objects.get(pk=find.pk)
        find.save()

        search = {'datings__period': final_neo.pk}

        # no result when no authentication
        response = c.get(reverse('get-find'), search)
        self.assertRedirects(response, "/")

        # one result for exact search
        c.login(username=self.username, password=self.password)
        response = c.get(reverse('get-find'), search)
        self.assertEqual(response.status_code, 200)
        res = json.loads(response.content.decode())
        self.assertEqual(res['recordsTotal'], 1)
        self.assertEqual(res["rows"][0]["cached_periods"],
                         str(final_neo))

        # no result for the brother
        search = {'datings__period': recent_neo.pk}
        response = c.get(reverse('get-find'), search)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            json.loads(response.content.decode())['recordsTotal'], 0)

        # one result for the father
        search = {'datings__period': neo.pk}
        response = c.get(reverse('get-find'), search)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            json.loads(response.content.decode())['recordsTotal'], 1)

    def test_conservatory_state_hierarchic_search(self):
        find = self.finds[0]
        c = Client()
        cs1 = models.ConservatoryState.objects.all()[0]
        cs1.parent = None
        cs1.save()
        cs2 = models.ConservatoryState.objects.all()[1]
        cs2.parent = cs1
        cs2.save()
        cs3 = models.ConservatoryState.objects.all()[2]
        cs3.parent = cs1
        cs3.save()
        find.conservatory_state = cs2
        find.save()

        search = {'conservatory_state': cs2.pk}

        # no result when no authentication
        response = c.get(reverse('get-find'), search)
        self.assertRedirects(response, "/")

        c.login(username=self.username, password=self.password)

        # one result for exact search
        response = c.get(reverse('get-find'), search)
        self.assertEqual(response.status_code, 200)
        content = response.content.decode()
        self.assertEqual(json.loads(content)['recordsTotal'], 1)

        # no result for the brother
        search = {'conservatory_state': cs3.pk}
        response = c.get(reverse('get-find'), search)
        self.assertEqual(response.status_code, 200)
        content = response.content.decode()
        self.assertEqual(json.loads(content)['recordsTotal'], 0)

        # one result for the father
        search = {'conservatory_state': cs1.pk}
        response = c.get(reverse('get-find'), search)
        self.assertEqual(response.status_code, 200)
        content = response.content.decode()
        self.assertEqual(json.loads(content)['recordsTotal'], 1)

    def test_image_search(self):
        c = Client()
        c.login(username=self.username, password=self.password)
        search = {'documents__image__isnull': 2}  # 2 for nullboolfield is None
        response = c.get(reverse('get-find'), search)
        self.assertEqual(response.status_code, 200)
        res = json.loads(response.content.decode())
        self.assertEqual(res['recordsTotal'], 0)

        # add an image to the first find
        image = Document.objects.create(title="Image!")
        img = settings.ROOT_PATH + \
              '../ishtar_common/static/media/images/ishtar-bg.jpg'
        image.image.save('ishtar-bg.jpg', File(open(img)))
        self.finds[0].documents.add(image)
        self.finds[0].save()
        response = c.get(reverse('get-find'), search)
        self.assertEqual(response.status_code, 200)
        res = json.loads(response.content.decode())
        self.assertEqual(res['recordsTotal'], 1)

    def _test_search(self, client, result, context=""):
        for q, expected_result in result:
            search = {'search_vector': q}
            response = client.get(reverse('get-find'), search)
            self.assertEqual(response.status_code, 200)
            res = json.loads(response.content.decode())
            msg = "{} result(s) where expected for search: {} - found {}" \
                  "".format(expected_result, q, res['recordsTotal'])
            if context:
                msg = context + " - " + msg
            self.assertEqual(res['recordsTotal'], expected_result,
                             msg=msg)

    def test_search_with_callable(self):
        find = self.finds[0]
        find2 = self.finds[1]
        c = Client()
        c.login(username=self.username, password=self.password)

        loan_key = str(pgettext_lazy("key for text search", 'loan'))

        result = [
            (u'{}="{}"'.format(loan_key, str(_(u"Yes"))), 0),
            (u'{}="{}"'.format(loan_key, str(_(u"No"))), 0),
            ]
        self._test_search(c, result, context="No container defined")

        warehouse = Warehouse.objects.create(
            name="Lambda warehouse",
            warehouse_type=WarehouseType.objects.all()[0])
        container = Container.objects.create(
            location=warehouse,
            responsible=warehouse,
            container_type=ContainerType.objects.all()[0]
        )
        find.container_ref = container
        find.container = container
        find.save()
        container2 = Container.objects.create(
            location=warehouse,
            responsible=warehouse,
            container_type=ContainerType.objects.all()[0]
        )
        find2.container_ref = container2
        find2.container = container2
        find2.save()

        result = [
            (u'{}="{}"'.format(loan_key, str(_(u"Yes"))), 0),
            (u'{}="{}"'.format(loan_key, str(_(u"No"))), 2),
        ]
        self._test_search(c, result, context="All container in their "
                                             "reference location")
        find2.container = container
        find2.save()

        result = [
            (u'{}="{}"'.format(loan_key, str(_(u"Yes"))), 1),
            (u'{}="{}"'.format(loan_key, str(_(u"No"))), 1),
        ]
        self._test_search(c, result, context="One container in his "
                                             "reference location")

    def test_dynamic_field_search(self):
        find = self.finds[0]
        find2 = self.finds[1]
        c = Client()

        warehouse = Warehouse.objects.create(
            name="Lambda warehouse",
            warehouse_type=WarehouseType.objects.all()[0])
        warehouse_div = WarehouseDivision.objects.create(
            label="Salle", txt_idx='salle')
        warehouse_div2 = WarehouseDivision.objects.create(
            label=u"Étagère", txt_idx='etagere')
        warehouse_div_lnk = WarehouseDivisionLink.objects.create(
            warehouse=warehouse,
            division=warehouse_div
        )
        warehouse_div_lnk2 = WarehouseDivisionLink.objects.create(
            warehouse=warehouse,
            division=warehouse_div2
        )
        container = Container.objects.create(
            location=warehouse,
            responsible=warehouse,
            container_type=ContainerType.objects.all()[0]
        )
        ContainerLocalisation.objects.create(container=container,
                                             division=warehouse_div_lnk,
                                             reference="B2")
        ContainerLocalisation.objects.create(container=container,
                                             division=warehouse_div_lnk2,
                                             reference="A5")
        find.container_ref = container
        find.container = container
        find.save()
        container2 = Container.objects.create(
            location=warehouse,
            responsible=warehouse,
            container_type=ContainerType.objects.all()[0]
        )
        ContainerLocalisation.objects.create(container=container2,
                                             division=warehouse_div_lnk,
                                             reference="B5")
        ContainerLocalisation.objects.create(container=container2,
                                             division=warehouse_div_lnk2,
                                             reference="A5")
        find2.container_ref = container2
        find2.container = container2
        find2.save()

        ref_div_key = str(pgettext_lazy("key for text search",
                                        'reference-division')) + "-"

        result = [
            (u'{}="{}"'.format(ref_div_key + "salle", "B2"), 1),
            (u'{}="{}"'.format(ref_div_key + "etagere", "A5"), 2),
            (u'{}="{}" {}="{}"'.format(
                ref_div_key + "salle", "B2", ref_div_key + "etagere", "A5"), 1),
            (u'{}="{}" {}="{}"'.format(
                ref_div_key + "salle", "B*", ref_div_key + "etagere", "A5"), 2),
        ]
        c.login(username=self.username, password=self.password)

        self._test_search(c, result)


class FindAutocompleteTest(FindInit, TestCase):
    fixtures = WAREHOUSE_FIXTURES
    model = models.Find

    def setUp(self):
        self.create_finds(force=True)
        self.create_finds(force=True)
        self.create_finds(force=True)
        self.create_finds(force=True)
        self.username = 'myuser'
        self.password = 'mypassword'
        User.objects.create_superuser(self.username, 'myemail@test.com',
                                      self.password)
        self.client = Client()

    def test_autocomplete(self):
        find = self.finds[0]
        find.label = "test 012"
        find.save()
        find2 = self.finds[1]
        find2.label = "test 12"
        find2.save()
        find3 = self.finds[2]
        find3.label = "test 1"
        find3.save()
        find4 = self.finds[3]
        find4.label = "test 0120"
        find4.save()
        c = Client()
        c.login(username=self.username, password=self.password)
        response = c.get(reverse('autocomplete-find') + "?term=12")
        self.assertEqual(response.status_code, 200)
        content = response.content.decode()
        res = json.loads(content)
        self.assertEqual(len(res), 3)
        self.assertEqual(res[0]['id'], find2.pk)  # " 12" - startswith
        self.assertEqual(res[1]['id'], find.pk)  # 12 - endswith
        self.assertEqual(res[2]['id'], find4.pk)  # 12 - contains


class FindPermissionTest(FindInit, TestCase):
    fixtures = FIND_FIXTURES
    model = models.Find

    def setUp(self):
        self.username, self.password, self.user = create_superuser()
        self.alt_username, self.alt_password, self.alt_user = create_user()
        ct_find = ContentType.objects.get(app_label='archaeological_finds',
                                          model='find')
        self.alt_user.user_permissions.add(Permission.objects.get(
            codename='view_own_find', content_type=ct_find))
        self.alt_user.user_permissions.add(Permission.objects.get(
            codename='change_own_find', content_type=ct_find))
        self.alt_username2, self.alt_password2, self.alt_user2 = create_user(
            username='luke', password='iamyourfather'
        )
        profile = UserProfile.objects.create(
            profile_type=ProfileType.objects.get(txt_idx='collaborator'),
            person=self.alt_user2.ishtaruser.person,
            current=True
        )

        town = Town.objects.create(name='Tatouine', numero_insee='66000')
        area = Area.objects.create(label='Galaxie', txt_idx='galaxie')
        area.towns.add(town)
        profile.areas.add(area)

        self.orgas = self.create_orgas(self.user)
        self.create_operation(self.user, self.orgas[0])
        self.create_operation(self.alt_user, self.orgas[0])

        self.create_context_record(
            user=self.user,
            data={"label": u"CR 1", "operation": self.operations[0]})
        self.create_context_record(
            user=self.alt_user,
            data={"label": u"CR 2", "operation": self.operations[1]})
        self.cr_1 = self.context_records[-2]
        self.cr_2 = self.context_records[-1]

        self.create_finds(data_base={'context_record': self.cr_1},
                          user=self.user, force=True)
        self.create_finds(data_base={'context_record': self.cr_2},
                          user=self.alt_user, force=True)

        self.find_1 = self.finds[-2]
        self.find_2 = self.finds[-1]
        self.operations[-1].towns.add(town)

    def test_own_search(self):
        # no result when no authentification
        c = Client()
        response = c.get(reverse('get-find'))
        self.assertTrue(not response.content or
                        not json.loads(response.content))

        # possession
        c = Client()
        c.login(username=self.alt_username, password=self.alt_password)
        response = c.get(reverse('get-find'))
        # only one "own" context record available
        content = response.content.decode()
        self.assertTrue(json.loads(content))
        self.assertEqual(json.loads(content)['recordsTotal'], 1)

        # area filter
        c = Client()
        c.login(username=self.alt_username2, password=self.alt_password2)
        response = c.get(reverse('get-find'))
        # only one "own" operation available
        content = response.content.decode()
        self.assertTrue(json.loads(content))
        self.assertEqual(json.loads(content)['recordsTotal'], 1)


class FindQATest(FindInit, TestCase):
    fixtures = WAREHOUSE_FIXTURES
    model = models.Find

    def setUp(self):
        self.create_finds(data_base={"label": u"Find 1"}, force=True)
        self.create_finds(data_base={"label": u"Find 2"}, force=True)
        self.username, self.password, self.user = create_superuser()
        self.alt_username, self.alt_password, self.alt_user = create_user()
        self.alt_user.user_permissions.add(Permission.objects.get(
            codename='change_find'))

    def test_duplicate(self):
        find = self.finds[0]
        default_desc = "Description for duplicate"
        find.description = default_desc
        find.save()
        d = Document.objects.create()
        d.finds.add(find)

        c = Client()
        url = reverse('find-qa-duplicate', args=[find.pk])
        response = c.get(url)
        self.assertRedirects(response, '/')

        c = Client()
        c.login(username=self.username, password=self.password)
        response = c.get(url)
        self.assertEqual(response.status_code, 200)

        c = Client()
        c.login(username=self.alt_username, password=self.alt_password)
        response = c.get(url)
        self.assertEqual(response.status_code, 200)

        data = {
            "denomination": "clone",
            "label": "clone - free id"
        }
        nb_find = models.Find.objects.count()
        nb_bf = models.BaseFind.objects.count()
        response = c.post(url, data)
        self.assertEqual(response.status_code, 302)  # success redirect
        self.assertEqual(models.Find.objects.count(), nb_find + 1)
        self.assertEqual(models.BaseFind.objects.count(), nb_bf + 1)
        new = models.Find.objects.order_by('-pk').all()[0]
        self.assertEqual(new.description, default_desc)
        new_bf = models.BaseFind.objects.order_by('-pk').all()[0]
        base_bf = find.get_first_base_find()
        self.assertEqual(new_bf.context_record, base_bf.context_record)
        self.assertNotIn(d, list(new.documents.all()))

    def test_bulk_update(self):
        c = Client()
        pks = u"{}-{}".format(self.finds[0].pk, self.finds[1].pk)
        response = c.get(reverse('find-qa-bulk-update', args=[pks]))
        self.assertRedirects(response, '/')

        c = Client()
        c.login(username=self.username, password=self.password)
        response = c.get(reverse('find-qa-bulk-update', args=[pks]))
        self.assertEqual(response.status_code, 200)

        c = Client()
        c.login(username=self.alt_username, password=self.alt_password)
        response = c.get(reverse('find-qa-bulk-update', args=[pks]))
        self.assertEqual(response.status_code, 200)

        find_0 = self.finds[0]
        find_1 = self.finds[1]
        base_desc_0 = u"Base description 1"
        find_0.description = base_desc_0
        find_0.save()
        base_desc_1 = u"Base description 2"
        find_1.description = base_desc_1
        find_1.save()

        period = Period.objects.all()[0].pk
        self.assertNotIn(period,
                         [dating.period.pk for dating in find_0.datings.all()])
        self.assertNotIn(period,
                         [dating.period.pk for dating in find_1.datings.all()])
        extra_desc = u"Extra description"
        response = c.post(
            reverse('find-qa-bulk-update-confirm', args=[pks]),
            {'qa_period': period, 'qa_description': extra_desc}
        )
        if response.status_code != 200:
            self.assertRedirects(response, '/success/')
        self.assertIn(period,
                      [dating.period.pk for dating in find_0.datings.all()])
        self.assertIn(period,
                      [dating.period.pk for dating in find_1.datings.all()])
        self.assertEqual(models.Find.objects.get(pk=find_0.pk).description,
                         base_desc_0 + u"\n" + extra_desc)
        self.assertEqual(models.Find.objects.get(pk=find_1.pk).description,
                         base_desc_1 + u"\n" + extra_desc)

    def test_packaging(self):
        c = Client()
        find_0 = self.finds[0]
        find_1 = self.finds[1]
        pks = u"{}-{}".format(find_0.pk, find_1.pk)
        url = reverse('find-qa-packaging', args=[pks])
        response = c.get(url)
        self.assertRedirects(response, '/')
        profile, created = IshtarSiteProfile.objects.get_or_create(
            slug='default', active=True)
        profile.warehouse = False
        profile.save()

        c = Client()
        c.login(username=self.username, password=self.password)
        response = c.get(url)
        # warehouse profile must be activated
        self.assertEqual(response.status_code, 404)

        profile.warehouse = True
        profile.save()
        response = c.get(url)
        self.assertEqual(response.status_code, 200)

        c = Client()
        c.login(username=self.alt_username, password=self.alt_password)
        response = c.get(url)
        self.assertEqual(response.status_code, 200)

        main_warehouse = Warehouse.objects.create(
            name="Main",
            warehouse_type=WarehouseType.objects.all()[0]
        )

        container = Container.objects.create(
            reference="Test", responsible=main_warehouse,
            location=main_warehouse,
            container_type=ContainerType.objects.all()[0]
        )
        container2 = Container.objects.create(
            reference="Test2", responsible=main_warehouse,
            location=main_warehouse,
            container_type=ContainerType.objects.all()[0]
        )
        packaging = models.TreatmentType.objects.get(txt_idx='packaging')
        packaging.change_reference_location = True
        packaging.save()

        data_check_lst = [
            ({
                 'qa-packaging-container': container.pk,
                 'qa-packaging-container_to_change': 'reference',
             },
             {
                 'container_ref': container,
                 'container': None
             }, 0),
            ({
                 'qa-packaging-container': container2.pk,
                 'qa-packaging-container_to_change': 'current',
             },
             {
                 'container_ref': None,
                 'container': container2
             }, 0),
            ({
                 'qa-packaging-container': container.pk,
                 'qa-packaging-container_to_change': 'current-and-reference',
             },
             {
                 'container_ref': container,
                 'container': container
             }, 0),
            ({
                 'qa-packaging-container': container2.pk,
                 'qa-packaging-container_to_change': 'reference',
                 'qa-packaging-create_treatment': True,
                 'qa-packaging-year': 2019,
                 'qa-packaging-treatment_type': packaging.pk
             },
             {
                 'container_ref': container2,
                 'container': None
             }, 1),
        ]

        for data, check, nb_treat in data_check_lst:
            # reinit
            find_0.container, find_0.container_ref = None, None
            find_0.skip_history_when_saving = True
            find_0.save()
            find_1.container, find_1.container_ref = None, None
            find_1.skip_history_when_saving = True
            find_1.save()

            init_nb_treat = models.Treatment.objects.count()

            response = c.post(
                reverse('find-qa-packaging', args=[pks]),
                data)
            self.assertRedirects(response, '/success/')
            for k in check:
                find = models.Find.objects.get(pk=find_0.pk)
                self.assertEqual(getattr(find, k), check[k])
                find = models.Find.objects.get(pk=find_1.pk)
                self.assertEqual(getattr(find, k), check[k])

            final_nb_treat = models.Treatment.objects.count()
            self.assertEqual(init_nb_treat + nb_treat, final_nb_treat)


class FindHistoryTest(FindInit, TestCase):
    fixtures = FIND_FIXTURES
    model = models.Find

    def setUp(self):
        self.create_finds(data_base={"label": u"Find 1"}, force=True)
        self.username, self.password, self.user = create_superuser()
        self.client = Client()
        self.client.login(username=self.username, password=self.password)

    def _add_datings(self, find):
        d1_attrs = {
            "period": Period.objects.get(txt_idx='neolithic'),
            "start_date": 5000,
            'end_date': 5001,
            'dating_type': DatingType.objects.get(
                txt_idx='from_absolute_dating'),
            "quality": DatingQuality.objects.get(txt_idx='sure'),
            "precise_dating": u"Blah !!!"
        }
        d1 = Dating.objects.create(**d1_attrs)
        d2_attrs = {
            "period": Period.objects.get(txt_idx='paleolithic'),
        }
        d2 = Dating.objects.create(**d2_attrs)

        d1_dct, d2_dct = {}, {}
        for k in Dating.HISTORY_ATTR:
            for dct, attr in ((d1_dct, d1_attrs) ,
                              (d2_dct, d2_attrs)):
                if k in attr:
                    if hasattr(attr[k], 'txt_idx'):
                        dct[k] = attr[k].txt_idx
                    else:
                        dct[k] = str(attr[k])
                else:
                    dct[k] = ''

        find.datings.add(d1)
        find.datings.add(d2)
        return d1_dct, d2_dct

    def test_m2m_history_save(self):
        find = self.finds[0]
        user = self.get_default_user()

        ceram = models.MaterialType.objects.get(txt_idx='ceramic').pk
        glass = models.MaterialType.objects.get(txt_idx='glass').pk

        find = models.Find.objects.get(pk=find.pk)
        nb_hist = find.history.count()
        find.label = "hop hop hop1"
        find.history_modifier = user
        find._force_history = True
        find.save()
        find.material_types.add(ceram)
        find.material_types.add(glass)
        d1_txt, d2_txt = self._add_datings(find)

        find = models.Find.objects.get(pk=find.pk)
        self.assertIsNotNone(find.history_m2m)
        self.assertIn('material_types', find.history_m2m)
        self.assertIn(
            find.history_m2m['material_types'],
            [['ceramic', 'glass'],   # order do not
             ['glass', 'ceramic']])  # matter
        self.assertIn('datings', find.history_m2m)
        self.assertIn(
            find.history_m2m['datings'],
            [[d1_txt, d2_txt],   # order do not
             [d2_txt, d1_txt]])  # matter
        self.assertEqual(find.history.count(), nb_hist + 1)

        historical_material_types = find.history_m2m['material_types']

        find = models.Find.objects.get(pk=find.pk)
        find.label = "hop hop hop2"
        find.history_modifier = user
        if hasattr(find, 'skip_history_when_saving'):
            delattr(find, 'skip_history_when_saving')
        find._force_history = True
        find.save()
        find.material_types.remove(ceram)
        find.datings.clear()

        find = models.Find.objects.get(pk=find.pk)
        self.assertEqual(find.history_m2m['material_types'], ['glass'])
        self.assertEqual(find.history_m2m['datings'], [])
        self.assertEqual(find.history.count(), nb_hist + 2)
        self.assertEqual(find.history.all()[1].history_m2m['material_types'],
                         historical_material_types)
        self.assertEqual(find.history.all()[0].history_m2m['material_types'],
                         ["glass"])

    def _init_m2m(self, find, user):
        ceram = models.MaterialType.objects.get(txt_idx='ceramic').pk
        glass = models.MaterialType.objects.get(txt_idx='glass').pk

        find = models.Find.objects.get(pk=find.pk)
        find.history_modifier = user
        find.label = "hop hop hop1"
        find._force_history = True
        if hasattr(find, 'skip_history_when_saving'):
            delattr(find, 'skip_history_when_saving')
        find.save()
        find.material_types.add(ceram)
        find.material_types.add(glass)
        self.d1_dct, self.d2_dct = self._add_datings(find)

        find = models.Find.objects.get(pk=find.pk)
        find.history_modifier = user
        find.label = "hop hop hop2"
        find._force_history = True
        if hasattr(find, 'skip_history_when_saving'):
            delattr(find, 'skip_history_when_saving')
        find.save()
        find.datings.clear()
        find.material_types.remove(ceram)

    def test_m2m_history_display(self):
        c = Client()
        user = self.get_default_user()
        find = self.finds[0]
        self._init_m2m(find, user)

        find = models.Find.objects.get(pk=find.pk)
        history_date = find.history.order_by('-history_date').all()[
            1].history_date.strftime('%Y-%m-%dT%H:%M:%S.%f')

        c.login(username=self.username, password=self.password)
        response = c.get(reverse('show-find',
                                 kwargs={'pk': find.pk}))
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'class="card sheet"', response.content)
        content = response.content.decode('utf-8')
        self.assertNotIn(
            models.MaterialType.objects.get(txt_idx='ceramic').label,
            content)
        self.assertNotIn(
            Period.objects.get(txt_idx='neolithic').label, content)
        self.assertNotIn("5001", content)

        response = c.get(reverse('show-historized-find',
                                 kwargs={'pk': find.pk,
                                         'date': history_date}))
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('class="card sheet"', content)
        self.assertIn(
            models.MaterialType.objects.get(txt_idx='ceramic').label, content,
            msg=u"ceramic not found in historical sheet")
        self.assertIn("5001", content, msg=u"5001 not found in historical "
                                           u"sheet")
        self.assertIn(
            Period.objects.get(txt_idx='neolithic').label, content)

    def test_m2m_history_restore(self):
        user = self.get_default_user()
        find = self.finds[0]
        self._init_m2m(find, user)

        find = models.Find.objects.get(pk=find.pk)
        ceram = models.MaterialType.objects.get(txt_idx='ceramic')
        glass = models.MaterialType.objects.get(txt_idx='glass')

        materials = list(find.material_types.all())
        self.assertNotIn(ceram, materials)
        self.assertIn(glass, materials)
        self.assertEqual(list(find.datings.all()), [])

        history_date = find.history.order_by('-history_date').all()[
            1].history_date
        find.rollback(history_date)

        find = models.Find.objects.get(pk=find.pk)
        materials = list(find.material_types.all())
        self.assertIn(ceram, materials)
        self.assertIn(glass, materials)

        current_datings = []
        for dating in find.datings.all():
            dating_dct = {}
            for k in self.d1_dct.keys():
                if not getattr(dating, k):
                    dating_dct[k] = ''
                    continue
                dating_dct[k] = getattr(dating, k)
                if hasattr(dating_dct[k], 'txt_idx'):
                    dating_dct[k] = dating_dct[k].txt_idx
                dating_dct[k] = str(dating_dct[k])

            current_datings.append(dating_dct)

        self.assertIn(self.d1_dct, current_datings)
        self.assertIn(self.d2_dct, current_datings)


class TreatmentTest(FindInit, TestCase):
    fixtures = FIND_FIXTURES
    model = models.Find

    def setUp(self):
        img = settings.ROOT_PATH + \
            '../ishtar_common/static/media/images/ishtar-bg.jpg'

        self.create_finds(data_base={"label": u"Find 1"}, force=True)
        self.create_finds(data_base={"label": u"Find 2"}, force=True)
        image = Document.objects.create(title="Image!")
        image.image.save('ishtar-bg.jpg', File(open(img)))
        self.finds[0].documents.add(image)
        self.finds[0].save()

        self.basket = models.FindBasket.objects.create(
            label="My basket", user=IshtarUser.objects.get(
                pk=self.get_default_user().pk))
        self.other_basket = models.FindBasket.objects.create(
            label="My other basket", user=IshtarUser.objects.get(
                pk=self.get_default_user().pk))
        for find in self.finds:
            self.basket.items.add(find)
            self.other_basket.items.add(find)

    def test_packaging_with_new_find_creation(self):
        treatment_type = models.TreatmentType.objects.get(txt_idx='packaging')
        # make packaging a treatment with a new version of the find created
        treatment_type.create_new_find = True
        treatment_type.save()

        treatment = models.Treatment()
        items_nb = models.Find.objects.count()

        first_find = self.finds[0]

        completed, created = models.TreatmentState.objects.get_or_create(
            txt_idx='completed', defaults={"executed": True, "label": u"Done"}
        )
        completed.executed = True
        completed.save()

        treatment.treatment_state = completed
        treatment.save(
            user=self.get_default_user(), items=self.basket,
            treatment_type_list=[treatment_type],
        )
        treatment.treatment_types.add(treatment_type)

        self.assertEqual(items_nb + self.basket.items.count(),
                         models.Find.objects.count(),
                         msg="Packaging doesn't generate enough new finds")

        resulting_find = models.Find.objects.get(
            upstream_treatment__upstream=first_find,
            base_finds__pk=first_find.base_finds.all()[0].pk
        )

        # image names used to be altered on save: check for this bug
        self.assertEqual(
            resulting_find.documents.all()[0].title,
            models.Find.objects.get(pk=first_find.pk).documents.all()[0].title
        )

        # new version of the find is in the basket
        for item in self.basket.items.all():
            self.assertNotIn(
                item, self.finds,
                msg="Original basket have not been upgraded after packaging")
        for item in self.other_basket.items.all():
            self.assertNotIn(
                item, self.finds,
                msg="Other basket have not been upgraded after packaging")

    def test_simple_delete(self):
        treatment_type = models.TreatmentType.objects.get(txt_idx='packaging')
        treatment_type.create_new_find = False
        treatment_type.save()

        nb_find = models.Find.objects.count()

        treatment = models.Treatment()

        initial_find = self.finds[0]
        completed, created = models.TreatmentState.objects.get_or_create(
            txt_idx='completed', defaults={"executed": True, "label": u"Done"}
        )
        completed.executed = True
        completed.save()

        treatment.treatment_state = completed
        treatment.save(
            user=self.get_default_user(), items=self.basket,
            treatment_type_list=[treatment_type],
        )
        treatment.treatment_types.add(treatment_type)

        self.assertEqual(nb_find, models.Find.objects.count())

        treatment.delete()
        self.assertEqual(
            models.Treatment.objects.filter(pk=treatment.pk).count(), 0)

        q = models.Find.objects.filter(pk=initial_find.pk)
        # initial find not deleted
        self.assertEqual(q.count(), 1)
        initial_find = q.all()[0]
        self.assertEqual(initial_find.upstream_treatment, None)

    def test_upstream_find_delete(self):
        treatment_type = models.TreatmentType.objects.get(txt_idx='packaging')
        # make packaging a treatment with a new version of the find created
        treatment_type.create_new_find = True
        treatment_type.save()

        nb_find = models.Find.objects.count()

        treatment = models.Treatment()

        initial_find = self.finds[0]
        completed, created = models.TreatmentState.objects.get_or_create(
            txt_idx='completed', defaults={"executed": True, "label": u"Done"}
        )
        completed.executed = True
        completed.save()

        treatment.treatment_state = completed
        treatment.save(
            user=self.get_default_user(), items=self.basket,
            treatment_type_list=[treatment_type],
        )
        treatment.treatment_types.add(treatment_type)

        nb_b = self.basket.items.count()
        self.assertEqual(
            nb_find + nb_b, models.Find.objects.count())

        resulting_find = models.Find.objects.get(
            upstream_treatment__upstream=initial_find,
            base_finds__pk=initial_find.base_finds.all()[0].pk
        )
        resulting_find.delete()

        self.assertEqual(
            models.Treatment.objects.filter(pk=treatment.pk).count(), 0)
        q = models.Find.objects.filter(pk=initial_find.pk)
        # initial find not deleted
        self.assertEqual(q.count(), 1)
        initial_find = q.all()[0]
        self.assertEqual(initial_find.upstream_treatment, None)

    def test_treatment_delete(self):
        treatment_type = models.TreatmentType.objects.get(txt_idx='packaging')
        treatment_type.create_new_find = True
        treatment_type.save()

        nb_find = models.Find.objects.count()

        treatment = models.Treatment()

        initial_find = self.finds[0]
        completed, created = models.TreatmentState.objects.get_or_create(
            txt_idx='completed', defaults={"executed": True, "label": u"Done"}
        )
        completed.executed = True
        completed.save()

        treatment.treatment_state = completed
        treatment.save(
            user=self.get_default_user(), items=self.basket,
            treatment_type_list=[treatment_type],
        )
        treatment.treatment_types.add(treatment_type)

        nb_b = self.basket.items.count()
        self.assertEqual(
            nb_find + nb_b, models.Find.objects.count())

        treatment.delete()

        self.assertEqual(nb_find, models.Find.objects.count())

        self.assertEqual(
            models.Treatment.objects.filter(pk=treatment.pk).count(), 0)
        q = models.Find.objects.filter(pk=initial_find.pk)
        # initial find not deleted
        self.assertEqual(q.count(), 1)
        initial_find = q.all()[0]
        self.assertEqual(initial_find.upstream_treatment, None)


class GeomaticTest(FindInit, TestCase):
    fixtures = FIND_FIXTURES
    model = models.Find

    def setUp(self):
        self.create_finds(data_base={"label": u"Find 1"}, force=True)

    def test_point_precision(self):
        self.username = 'myuser'
        self.password = 'mypassword'
        User.objects.create_superuser(self.username, 'myemail@test.com',
                                      self.password)
        profile, created = IshtarSiteProfile.objects.get_or_create(
            slug='default', active=True)
        profile.mapping = True
        profile.save()
        wgs84 = SpatialReferenceSystem.objects.get(srid=4326)
        find = self.finds[0]
        base_find = find.base_finds.all()[0]
        self.assertEqual(base_find.x, None)

        base_find.x, base_find.y = 33.12999, 42.78945
        base_find.spatial_reference_system = wgs84
        base_find.save()

        find = base_find.find.all()[0]
        c = Client()
        c.login(username=self.username, password=self.password)
        response = c.get(
            reverse('show-find', kwargs={'pk': find.pk}))
        self.assertIn(b'33.12999', response.content)
        profile.point_precision = 2
        profile.save()

        response = c.get(
            reverse('show-find', kwargs={'pk': find.pk}))
        self.assertIn(b'33.13', response.content)

    def test_update_container_localisation_on_warehouse_update(self):
        profile, created = IshtarSiteProfile.objects.get_or_create(
            slug='default', active=True)
        profile.mapping = True
        profile.save()
        wgs84 = SpatialReferenceSystem.objects.get(srid=4326)

        find = self.finds[0]
        base_find = find.base_finds.all()[0]
        self.assertEqual(base_find.x, None)

        operation = Operation.objects.get(
            pk=base_find.context_record.operation.pk
        )
        operation.x, operation.y = 33, 42
        operation.spatial_reference_system = wgs84
        operation.save()

        # an update is pending for the linked context record
        q = ContextRecord.objects.filter(
            pk=base_find.context_record.pk,
            need_update=True)
        self.assertEqual(q.count(), 1)

        # process pending update
        context_record = q.all()[0]
        self.assertEqual(context_record.x, None)  # update has to be done
        context_record.skip_history_when_saving = True
        context_record._no_move = True
        context_record.save()
        context_record = ContextRecord.objects.get(pk=context_record.pk)
        self.assertEqual(context_record.x, 33)

        # then an update is pending for the linked base find
        q = models.BaseFind.objects.filter(pk=base_find.pk, need_update=True)
        self.assertEqual(q.count(), 1)

        # process pending update
        bf = q.all()[0]
        self.assertEqual(bf.x, None)  # update has to be done
        bf.skip_history_when_saving = True
        bf._no_move = True
        bf.save()
        bf = models.BaseFind.objects.get(pk=bf.pk)
        self.assertEqual(bf.x, 33)

    def test_post_save_point(self):
        find = self.finds[0]
        base_find = find.base_finds.all()[0]

        srs, __ = SpatialReferenceSystem.objects.get_or_create(
            txt_idx='wgs84', defaults={"srid": 4326, "label": 'WGS84',
                                       'auth_name': 'EPSG'}
        )
        profile = get_current_profile()
        profile.mapping = True
        profile.save()

        # db source
        geom = GEOSGeometry('POINT({} {} {})'.format(2, 43, 1), srid=4326)
        base_find.point = geom
        base_find.save()
        base_find = models.BaseFind.objects.get(pk=base_find.pk)
        self.assertIsNotNone(base_find.point)
        self.assertIsNotNone(base_find.point_2d)
        self.assertIsNotNone(base_find.x)
        self.assertIsNotNone(base_find.y)
        self.assertIsNotNone(base_find.z)
        self.assertEqual(base_find.point_source, 'P')  # precise
        self.assertEqual(base_find.point_source_item,
                         str(models.BaseFind._meta.verbose_name))

        # re-save do not change sources
        base_find.save()
        base_find = models.BaseFind.objects.get(pk=base_find.pk)
        self.assertIsNotNone(base_find.point)
        self.assertIsNotNone(base_find.point_2d)
        self.assertIsNotNone(base_find.x)
        self.assertIsNotNone(base_find.y)
        self.assertIsNotNone(base_find.z)
        self.assertEqual(base_find.point_source, 'P')  # precise
        self.assertEqual(base_find.point_source_item,
                         str(models.BaseFind._meta.verbose_name))

        # form input
        base_find.x = 2
        base_find.y = 3
        base_find.z = 4
        base_find.spatial_reference_system = srs
        base_find.save()
        base_find = models.BaseFind.objects.get(pk=base_find.pk)
        self.assertIsNotNone(base_find.point_2d)
        self.assertIsNotNone(base_find.point)
        self.assertEqual(base_find.point_source, 'P')  # precise
        self.assertEqual(base_find.point_source_item,
                         str(models.BaseFind._meta.verbose_name))
        # re-save do not change sources
        base_find.save()
        base_find = models.BaseFind.objects.get(pk=base_find.pk)
        self.assertIsNotNone(base_find.point_2d)
        self.assertIsNotNone(base_find.point)
        self.assertEqual(base_find.point_source, 'P')  # precise
        self.assertEqual(base_find.point_source_item,
                         str(models.BaseFind._meta.verbose_name))

        # reinit
        base_find.x = None
        base_find.y = None
        base_find.z = None
        base_find.save()
        base_find = models.BaseFind.objects.get(pk=base_find.pk)
        self.assertIsNone(base_find.point_2d)
        self.assertIsNone(base_find.point)
        self.assertEqual(base_find.point_source, None)
        self.assertEqual(base_find.point_source_item, None)

        profile, created = IshtarSiteProfile.objects.get_or_create(
            slug='default', active=True)
        profile.use_town_for_geo = True
        profile.save()

        # geom from context record town
        center = 'POINT(6 10)'
        limit = 'MULTIPOLYGON(((1 1,5 1,5 5,1 5,1 1),(2 2,2 3,3 3,3 2,2 2)),'\
                '((6 3,9 2,9 4,6 3)))'
        cr = ContextRecord.objects.get(pk=base_find.context_record.pk)
        t = cr.town
        t.center = 'SRID=4326;' + center
        t.limit = 'SRID=4326;' + limit
        t.save()
        cr = ContextRecord.objects.get(pk=base_find.context_record.pk)
        base_find = models.BaseFind.objects.get(pk=base_find.pk)
        base_find.save()
        base_find = models.BaseFind.objects.get(pk=base_find.pk)
        bfp = GEOSGeometry(base_find.point_2d.ewkt)
        bfp.transform(4326)
        tp = GEOSGeometry(cr.town.center.ewkt)
        tp.transform(4326)
        self.assertTrue(bfp.distance(tp) < 0.0001)
        self.assertEqual(base_find.multi_polygon, cr.town.limit)
        self.assertEqual(base_find.point_source, 'T')  # town
        self.assertEqual(base_find.point_source_item,
                         str(ContextRecord._meta.verbose_name))
        # re-save do not change sources
        base_find.save()
        base_find = models.BaseFind.objects.get(pk=base_find.pk)
        bfp = GEOSGeometry(base_find.point_2d.ewkt)
        bfp.transform(4326)
        tp = GEOSGeometry(cr.town.center.ewkt)
        tp.transform(4326)
        self.assertTrue(bfp.distance(tp) < 0.0001)
        self.assertEqual(base_find.multi_polygon, cr.town.limit)
        self.assertEqual(base_find.point_source, 'T')  # town
        self.assertEqual(base_find.point_source_item,
                         str(ContextRecord._meta.verbose_name))

        # geom from context record
        center = 'POINT(8 6)'
        cr = ContextRecord.objects.get(pk=base_find.context_record.pk)
        cr.point_2d = 'SRID=4326;' + center
        cr.point_source = 'P'
        cr.point_source_item = str(ContextRecord._meta.verbose_name)
        cr.save()
        cr = ContextRecord.objects.get(pk=base_find.context_record.pk)
        cr.x = cr.point_2d.x
        cr.y = cr.point_2d.y
        cr.save()
        cr = ContextRecord.objects.get(pk=base_find.context_record.pk)
        base_find = models.BaseFind.objects.get(pk=base_find.pk)
        base_find.save()
        base_find = models.BaseFind.objects.get(pk=base_find.pk)
        bfp = GEOSGeometry(base_find.point_2d.ewkt)
        bfp.transform(4326)
        tp = GEOSGeometry(cr.point_2d.ewkt)
        tp.transform(4326)
        self.assertTrue(bfp.distance(tp) < 0.0001)
        self.assertEqual(base_find.multi_polygon, cr.multi_polygon)
        self.assertEqual(base_find.point_source, 'P')  # precise
        self.assertEqual(base_find.point_source_item,
                         str(ContextRecord._meta.verbose_name))

        # overload of coordinates by form
        base_find = models.BaseFind.objects.get(pk=base_find.pk)
        base_find.x = 5
        base_find.y = 6
        base_find.z = 1
        base_find.spatial_reference_system = srs
        base_find.save()

        base_find = models.BaseFind.objects.get(pk=base_find.pk)
        self.assertIsNotNone(base_find.point_2d)
        self.assertIsNotNone(base_find.point)
        self.assertEqual(base_find.point_2d.wkt, 'POINT (5 6)')
        self.assertEqual(base_find.point_source, 'P')  # precise
        self.assertEqual(base_find.point_source_item,
                         str(models.BaseFind._meta.verbose_name))
        # re-save do not change sources
        base_find.save()
        base_find = models.BaseFind.objects.get(pk=base_find.pk)
        self.assertIsNotNone(base_find.point_2d)
        self.assertIsNotNone(base_find.point)
        self.assertEqual(base_find.point_2d.wkt, 'POINT (5 6)')
        self.assertEqual(base_find.point_source, 'P')  # precise
        self.assertEqual(base_find.point_source_item,
                         str(models.BaseFind._meta.verbose_name))

    def test_post_save_polygon(self):
        profile = get_current_profile()
        profile.mapping = True
        profile.save()

        find = self.finds[0]
        base_find = find.base_finds.all()[0]
        srs, __ = SpatialReferenceSystem.objects.get_or_create(
            txt_idx='wgs84', defaults={"srid": 4326, "label": 'WGS84',
                                       'auth_name': 'EPSG'}
        )

        base_find = models.BaseFind.objects.get(pk=base_find.pk)

        # get centroid geom from poly
        limit = 'MULTIPOLYGON(((1 1,5 1,5 5,1 5,1 1),(2 2,2 3,3 3,3 2,2 2)),' \
                '((6 3,9 2,9 4,6 3)))'
        base_find.multi_polygon = 'SRID=4326;' + limit
        base_find.point_source = None
        base_find.point_2d = None
        base_find.point = None
        base_find.save()

        base_find = models.BaseFind.objects.get(pk=base_find.pk)
        self.assertIsNotNone(base_find.point_2d)
        self.assertEqual(base_find.point_source, 'M')  # from multi polygon
        # resaving do not change source
        base_find.save()
        base_find = models.BaseFind.objects.get(pk=base_find.pk)
        self.assertIsNotNone(base_find.point_2d)
        self.assertEqual(base_find.point_source, 'M')  # from multi polygon

        # geom from context record
        limit = 'MULTIPOLYGON(((1 1,5 1,5 5,1 5,1 1),(2 2,2 3,3 3,3 2,2 2)),' \
                '((6 3,9 2,9 4,6 3)))'
        cr = ContextRecord.objects.get(pk=base_find.context_record.pk)
        cr.multi_polygon = 'SRID=4326;' + limit
        cr.multi_polygon_source = 'P'
        cr.multi_polygon_source_item = str(ContextRecord._meta.verbose_name)
        cr.save()
        cr = ContextRecord.objects.get(pk=base_find.context_record.pk)
        base_find = models.BaseFind.objects.get(pk=base_find.pk)
        base_find.multi_polygon_source_item = "hop"
        base_find.save()
        base_find = models.BaseFind.objects.get(pk=base_find.pk)
        self.assertEqual(base_find.multi_polygon, cr.multi_polygon)
        self.assertEqual(base_find.multi_polygon_source, 'P')  # precise
        self.assertEqual(base_find.multi_polygon_source_item,
                         str(ContextRecord._meta.verbose_name))

        base_find = models.BaseFind.objects.get(pk=base_find.pk)
        base_find.point_source = None
        base_find.spatial_reference_system = srs
        base_find.x = "42"
        base_find.y = "3"
        base_find.save()
        base_find = models.BaseFind.objects.get(pk=base_find.pk)
        self.assertEqual(base_find.point_2d.ewkt, "SRID=4326;POINT (42 3)")
        self.assertEqual(base_find.point_source, 'P')
        # resaving do not change source
        base_find.save()
        base_find = models.BaseFind.objects.get(pk=base_find.pk)
        self.assertEqual(base_find.point_2d.ewkt, "SRID=4326;POINT (42 3)")
        self.assertEqual(base_find.point_source, 'P')


class AutocompleteTest(AutocompleteTestBase, TestCase):
    fixtures = FIND_FIXTURES
    models = [
        AcItem(models.ObjectType, 'autocomplete-objecttype'),
        AcItem(models.MaterialType, 'autocomplete-materialtype'),
        AcItem(models.TreatmentType, 'autocomplete-treatmenttype',
               default_values={"virtual": False}),
        AcItem(models.IntegrityType, 'autocomplete-integritytype'),
        AcItem(models.TreatmentFile, 'autocomplete-treatmentfile',
               prepare_func="create_treatmentfile"),
        AcItem(models.FindBasket, 'autocomplete-findbasket',
               prepare_func="create_findbasket"),
        AcItem(models.Find, 'autocomplete-find', prepare_func="create_find"),
        AcItem(models.Treatment, 'autocomplete-treatment',
               prepare_func='create_treatment'),
    ]

    def create_treatmentfile(self, base_name):
        item, __ = models.TreatmentFile.objects.get_or_create(
            name=base_name,
            type=models.TreatmentFileType.objects.all()[0]
        )
        return item, None

    def create_treatment(self, base_name):
        item, __ = models.Treatment.objects.get_or_create(
            label=base_name,
            treatment_state=models.TreatmentState.objects.all()[0]
        )
        return item, None

    def create_findbasket(self, base_name):
        item, __ = models.FindBasket.objects.get_or_create(
            label=base_name,
        )
        item.user = self.user.ishtaruser
        item.save()
        return item, None

    def create_find(self, base_name):
        ope, __ = Operation.objects.get_or_create(
            common_name="Test",
            operation_type=OperationType.objects.all()[0]
        )
        cr, __ = ContextRecord.objects.get_or_create(
            operation=ope,
            label=base_name
        )
        base_find = models.BaseFind.objects.create(context_record=cr)
        find = models.Find.objects.create(
            label=base_name,
        )
        find.base_finds.add(base_find)
        return find, None


class PublicAPITest(FindInit, APITestCase):
    fixtures = FIND_FIXTURES
    model = models.Find

    def setUp(self):
        self.create_finds(data_base={"label": u"Find 1"}, force=True)
        self.basket = models.FindBasket.objects.create(
            slug="my-basket", label="My basket",
            user=IshtarUser.objects.get(pk=self.get_default_user().pk))
        for find in self.finds:
            self.basket.items.add(find)
        self.username = 'myuser'
        self.password = 'mypassword'
        user = User.objects.create_user(self.username, 'myemail@test.com',
                                        self.password)
        self.auth_token = 'Token '+ Token.objects.create(user=user).key

    def test_authentication(self):
        url = reverse('api-public-find') + "?basket=my-basket"
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, 401)
        response = self.client.get(url, format='json',
                                   HTTP_AUTHORIZATION=self.auth_token)
        self.assertEqual(response.status_code, 200)

    def test_basket(self):
        # unknown basket
        url = reverse('api-public-find') + "?basket=non-exist-basket"
        response = self.client.get(url, format='json',
                                   HTTP_AUTHORIZATION=self.auth_token)
        self.assertEqual(response.status_code, 200)
        content = response.content.decode()
        res = json.loads(content)
        self.assertEqual(res, [])

        # basket is not public
        url = reverse('api-public-find') + "?basket=my-basket"
        response = self.client.get(url, format='json',
                                   HTTP_AUTHORIZATION=self.auth_token)
        self.assertEqual(response.status_code, 200)
        content = response.content.decode()
        res = json.loads(content)
        self.assertEqual(res, [])

        # ok
        self.basket.public = True
        self.basket.save()
        response = self.client.get(url, format='json',
                                   HTTP_AUTHORIZATION=self.auth_token)
        self.assertEqual(response.status_code, 200)
        content = response.content.decode()
        res = json.loads(content)
        self.assertEqual(len(res), 1)

    def test_basket_content(self):
        find = self.finds[0]
        find.denomination = "denomination"
        ceram = models.MaterialType.objects.get(txt_idx='ceramic')
        find.material_types.add(ceram)
        find.save()

        url = reverse('api-public-find') + "?basket=my-basket"
        self.basket.public = True
        self.basket.save()
        response = self.client.get(url, format='json',
                                   HTTP_AUTHORIZATION=self.auth_token)
        self.assertEqual(response.status_code, 200)
        content = response.content.decode()
        res = json.loads(content)
        expected_results = [
            ((0, 'denomination',), 'denomination'),
            ((0, 'materials', 0), 'Céramique'),
            ((0, 'base-finds', 0, 'context-record', 'label'),
             'Context record'),
            ((0, 'base-finds', 0, 'context-record', 'town'),
             'default_town (12345)'),
            ((0, 'base-finds', 0, 'context-record', 'operation', 'year'), 2010),
        ]
        for path, result in expected_results:
            path = list(reversed(path))
            value = deepcopy(res)
            while path:
                key = path.pop()
                if isinstance(key, int):
                    self.assertTrue(len(value) > key)
                else:
                    self.assertIn(key, value)
                value = value[key]
            self.assertEqual(value, result)

