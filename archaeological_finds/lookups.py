from ajax_select import register

from ishtar_common.lookups import LookupChannel, TypeLookupChannel

from django.db.models import Q
from django.utils.encoding import force_text
from django.utils.html import escape

from archaeological_finds import models


@register('base_find')
class BaseFindLookup(LookupChannel):
    model = models.BaseFind

    def get_query(self, q, request):
        query = Q()
        for term in q.strip().split(' '):
            subquery = (
                Q(cache_complete_id__icontains=term) |
                Q(label__icontains=term)
            )
            query &= subquery
        return self.model.objects.filter(query).order_by(
            'cache_complete_id')[:20]

    def format_item_display(self, item):
        return u"<span class='ajax-label'>%s</span>" % item.cache_complete_id


@register('find')
class FindLookup(LookupChannel):
    model = models.Find

    def get_query(self, q, request):
        query = Q()
        for term in q.strip().split(' '):
            subquery = (
                Q(cached_label__icontains=term) |
                Q(base_finds__cache_complete_id__icontains=term)
            )
            query &= subquery
        return self.model.objects.filter(query).order_by(
            'cached_label')[:20]

    def format_item_display(self, item):
        return u"<span class='ajax-label'>%s</span>" % item.full_label

    def format_match(self, obj):
        return escape(force_text(obj.full_label))


@register('treatment')
class TreatmentLookup(LookupChannel):
    model = models.Treatment

    def get_query(self, q, request):
        query = Q()
        for term in q.strip().split(' '):
            subquery = (
                Q(cached_label__icontains=term)
            )
            query &= subquery
        return self.model.objects.filter(query).order_by(
            'cached_label')[:20]

    def format_item_display(self, item):
        return u"<span class='ajax-label'>%s</span>" % item.cached_label


@register('treatment_file')
class TreatmentFileLookup(LookupChannel):
    model = models.TreatmentFile

    def get_query(self, q, request):
        query = Q()
        for term in q.strip().split(' '):
            subquery = (
                Q(cached_label__icontains=term)
            )
            query &= subquery
        return self.model.objects.filter(query).order_by(
            'cached_label')[:20]

    def format_item_display(self, item):
        return u"<span class='ajax-label'>%s</span>" % item.cached_label


@register('material_type')
class MaterialTypeLookup(TypeLookupChannel):
    model = models.MaterialType


@register('object_type')
class MaterialTypeLookup(TypeLookupChannel):
    model = models.ObjectType

