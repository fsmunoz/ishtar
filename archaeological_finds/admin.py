#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2012-2016 Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from ajax_select import make_ajax_form
from ajax_select.fields import AutoCompleteSelectField

from django import forms
from django.contrib import admin
from django.contrib.gis.forms import PointField, MultiPolygonField, \
    LineStringField, OSMWidget
from ishtar_common.utils import ugettext_lazy as _

from ishtar_common.apps import admin_site
from ishtar_common.admin import HistorizedObjectAdmin, GeneralTypeAdmin, \
    HierarchicalTypeAdmin

from . import models


class AdminBaseFindForm(forms.ModelForm):
    class Meta:
        model = models.BaseFind
        exclude = []
    point_2d = PointField(label=_(u"Point (2D)"), required=False,
                          widget=OSMWidget)
    line = LineStringField(label=_(u"Line"), required=False,
                           widget=OSMWidget)
    multi_polygon = MultiPolygonField(label=_(u"Multi polygon"), required=False,
                                      widget=OSMWidget)
    context_record = AutoCompleteSelectField('context_record')


class BaseFindAdmin(HistorizedObjectAdmin):
    list_display = ('label', 'context_record', 'index')
    search_fields = ('label', 'cache_complete_id',)
    model = models.BaseFind
    form = AdminBaseFindForm
    readonly_fields = HistorizedObjectAdmin.readonly_fields + [
        'cache_short_id', 'cache_complete_id',
    ]


admin_site.register(models.BaseFind, BaseFindAdmin)


class FindBasketAdmin(admin.ModelAdmin):
    class Meta:
        model = models.FindBasket
        exclude = []
    list_display = ["label", "user"]
    search_fields = ('label', 'user__person__raw_name',)
    readonly_fields = ["search_vector"]
    form = make_ajax_form(models.FindBasket, {
        'user': 'ishtaruser',
        'shared_with': 'ishtaruser',
        'shared_write_with': 'ishtaruser',
        'items': 'find',
    })


admin_site.register(models.FindBasket, FindBasketAdmin)


class FindAdmin(HistorizedObjectAdmin):
    list_display = ('label', 'operations_lbl', 'context_records_lbl', 'index',
                    'dating', 'materials')
    list_filter = ('datings__period', 'material_types')
    search_fields = ('label', "base_finds__cache_complete_id",
                     "base_finds__context_record__operation__cached_label")
    model = models.Find
    form = make_ajax_form(model, {
        'base_finds': 'base_find',
        'container': 'container',
        'container_ref': 'container',
        'material_types': 'material_type',
        'upstream_treatment': 'treatment',
        'downstream_treatment': 'treatment',
        'treatments': 'treatment',
        'main_image': 'document',
        'documents': 'document',
    })
    readonly_fields = HistorizedObjectAdmin.readonly_fields + [
        'datings', 'cached_label'
    ]


admin_site.register(models.Find, FindAdmin)


class PropertyAdmin(HistorizedObjectAdmin):
    list_display = ['find', 'person', 'start_date', 'end_date']
    search_fields = ('find__label', 'person__name')
    model = models.Property
    form = make_ajax_form(model, {
        'find': 'find',
        'person': 'person',
    })
    readonly_fields = HistorizedObjectAdmin.readonly_fields + [
        'administrative_act']

    def has_add_permission(self, request):
        return False


admin_site.register(models.Property, PropertyAdmin)


class TreatmentAdmin(HistorizedObjectAdmin):
    list_display = ('year', 'index', 'label','treatment_types_lbl', 'location',
                    'downstream_lbl', 'upstream_lbl', 'container', 'person')
    list_filter = ('treatment_types', 'treatment_state', 'year')
    model = models.Treatment
    form = make_ajax_form(model, {
        'person': 'person',
        'organization': 'organization',
        'file': 'treatment_file',
        'location': 'warehouse',
        'container': 'container',
    })
    readonly_fields = HistorizedObjectAdmin.readonly_fields + [
        'cached_label', 'downstream_lbl', 'upstream_lbl'
    ]

    def has_add_permission(self, request):
        return False


admin_site.register(models.Treatment, TreatmentAdmin)


class TreatmentFileAdmin(HistorizedObjectAdmin):
    list_display = ('type', 'year', 'index', 'name',
                    'applicant', 'in_charge', 'internal_reference')
    list_filter = ('type', 'year')
    search_fields = ('name', 'applicant__name', 'applicant__surname',
                     'applicant__raw_name', 'applicant_organisation__name',
                     'cached_label')
    model = models.TreatmentFile
    form = make_ajax_form(model, {
        'in_charge': 'person',
        'applicant': 'person',
        'applicant_organisation': 'organization',
    })
    readonly_fields = HistorizedObjectAdmin.readonly_fields + [
        'cached_label',
    ]


admin_site.register(models.TreatmentFile, TreatmentFileAdmin)


class ObjectTypeAdmin(HierarchicalTypeAdmin):
    model = models.ObjectType
    form = make_ajax_form(model, {
        'parent': 'object_type',
    })


admin_site.register(models.ObjectType, ObjectTypeAdmin)


class MaterialTypeAdmin(HierarchicalTypeAdmin):
    list_display = HierarchicalTypeAdmin.list_display + ['recommendation']
    search_fields = ('label', 'parent__label', 'comment',)
    model = models.MaterialType
    form = make_ajax_form(model, {
        'parent': 'material_type',
    })


admin_site.register(models.MaterialType, MaterialTypeAdmin)


admin_site.register(models.CommunicabilityType, HierarchicalTypeAdmin)


class TreatmentTypeAdmin(GeneralTypeAdmin):
    list_display = HierarchicalTypeAdmin.list_display[:-1] + ['order'] + \
                   [HierarchicalTypeAdmin.list_display[-1]]
    list_filter = [
        'virtual', 'destructive', 'create_new_find', 'upstream_is_many',
        'downstream_is_many', 'destructive', 'change_reference_location',
        'change_current_location', 'restore_reference_location'
    ]
    model = models.TreatmentType


admin_site.register(models.TreatmentType, TreatmentTypeAdmin)


class CheckedTypeAdmin(GeneralTypeAdmin):
    list_display = GeneralTypeAdmin.list_display + ['order']
    model = models.CheckedType


admin_site.register(models.CheckedType, CheckedTypeAdmin)


@admin.register(models.ConservatoryState, site=admin_site)
class ConservatoryStateAdmin(GeneralTypeAdmin):
    list_display = GeneralTypeAdmin.list_display + ['order']


@admin.register(models.TreatmentFileType, site=admin_site)
class TreatmentFileType(GeneralTypeAdmin):
    list_display = GeneralTypeAdmin.list_display + ["treatment_type"]


@admin.register(models.TreatmentState, site=admin_site)
class TreatmentState(GeneralTypeAdmin):
    list_display = GeneralTypeAdmin.list_display[:-1] + ["order", "executed"]


general_models = [
    models.RemarkabilityType,
    models.IntegrityType,
    models.BatchType, models.AlterationCauseType, models.AlterationType,
    models.TreatmentEmergencyType, models.ObjectTypeQualityType,
    models.MaterialTypeQualityType
]

for model in general_models:
    admin_site.register(model, GeneralTypeAdmin)
