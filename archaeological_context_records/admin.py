#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2012-2015 Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from ajax_select import make_ajax_form
from ajax_select.fields import AutoCompleteSelectField

from django import forms
from django.contrib import admin
from django.contrib.gis.forms import PointField, MultiPolygonField, OSMWidget
from ishtar_common.utils import ugettext_lazy as _

from ishtar_common.apps import admin_site
from ishtar_common.admin import HistorizedObjectAdmin, GeneralTypeAdmin

from . import models


class DatingAdmin(admin.ModelAdmin):
    list_display = ('period', 'context_records_lbl', 'finds_lbl')
    list_filter = ("period", 'dating_type', 'quality')
    model = models.Dating
    search_fields = ['context_records__cached_label', 'period__label',
                     'find__cached_label']
    readonly_fields = ['context_records_lbl', 'finds_lbl']


admin_site.register(models.Dating, DatingAdmin)


class AdminContextRecordForm(forms.ModelForm):
    class Meta:
        model = models.ContextRecord
        exclude = []
    point_2d = PointField(label=_(u"Point"), required=False,
                          widget=OSMWidget)
    multi_polygon = MultiPolygonField(label=_(u"Multi polygon"), required=False,
                                      widget=OSMWidget)
    operation = AutoCompleteSelectField('operation')
    parcel = AutoCompleteSelectField('parcel')
    town = AutoCompleteSelectField('town', required=False)
    archaeological_site = AutoCompleteSelectField('archaeological_site',
                                                  required=False)


class ContextRecordAdmin(HistorizedObjectAdmin):
    list_display = ('label', 'operation', 'parcel')
    list_filter = ('unit',)
    search_fields = ('label', 'parcel__operation__cached_label',
                     'parcel__section', 'parcel__parcel_number')
    model = models.ContextRecord
    form = AdminContextRecordForm
    readonly_fields = HistorizedObjectAdmin.readonly_fields + [
        'cached_label', 'datings'
    ]


admin_site.register(models.ContextRecord, ContextRecordAdmin)


class RecordRelationsAdmin(admin.ModelAdmin):
    list_display = ('left_record', 'relation_type', 'right_record')
    list_filter = ('relation_type',)
    model = models.RecordRelations
    form = make_ajax_form(model, {
        'left_record': 'context_record',
        'right_record': 'context_record',
    })


admin_site.register(models.RecordRelations, RecordRelationsAdmin)


class RelationTypeAdmin(GeneralTypeAdmin):
    list_display = ('label', 'txt_idx', 'tiny_label', 'available',
                    'symmetrical', 'logical_relation', 'inverse_relation',
                    'order', 'comment')


admin_site.register(models.RelationType, RelationTypeAdmin)


class UnitAdmin(GeneralTypeAdmin):
    list_display = ['label', 'txt_idx', 'parent', 'available', 'order',
                    'comment']

admin_site.register(models.Unit, UnitAdmin)

 
class IdentificationTypeAdmin(GeneralTypeAdmin):
    list_display = ['label', 'txt_idx', 'available', 'order', 'comment']

admin_site.register(models.IdentificationType, IdentificationTypeAdmin)


general_models = [
    models.DatingType, models.DatingQuality, models.DocumentationType,
    models.ActivityType, models.ExcavationTechnicType]
for model in general_models:
    admin_site.register(model, GeneralTypeAdmin)


