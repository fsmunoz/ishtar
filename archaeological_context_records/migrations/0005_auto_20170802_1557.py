# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('archaeological_context_records', '0004_views'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='contextrecord',
            options={'ordering': ('cached_label',), 'verbose_name': 'Context Record', 'verbose_name_plural': 'Context Record', 'permissions': (('view_contextrecord', 'Can view all Context Records'), ('view_own_contextrecord', 'Can view own Context Record'), ('add_own_contextrecord', 'Can add own Context Record'), ('change_own_contextrecord', 'Can change own Context Record'), ('delete_own_contextrecord', 'Can delete own Context Record'))},
        ),
        migrations.AlterField(
            model_name='contextrecord',
            name='closing_date',
            field=models.DateField(null=True, verbose_name='Closing date', blank=True),
        ),
        migrations.AlterField(
            model_name='contextrecord',
            name='opening_date',
            field=models.DateField(null=True, verbose_name='Opening date', blank=True),
        ),
        migrations.AlterField(
            model_name='historicalcontextrecord',
            name='closing_date',
            field=models.DateField(null=True, verbose_name='Closing date', blank=True),
        ),
        migrations.AlterField(
            model_name='historicalcontextrecord',
            name='opening_date',
            field=models.DateField(null=True, verbose_name='Opening date', blank=True),
        ),
    ]
