# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2019-05-27 16:45
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('archaeological_context_records', '0044_auto_20190225_1637'),
    ]

    operations = [
        migrations.AddField(
            model_name='contextrecord',
            name='cached_periods',
            field=models.TextField(blank=True, help_text='Generated automatically - do not edit', null=True, verbose_name='Cached periods label'),
        ),
        migrations.AddField(
            model_name='historicalcontextrecord',
            name='cached_periods',
            field=models.TextField(blank=True, help_text='Generated automatically - do not edit', null=True, verbose_name='Cached periods label'),
        ),
        migrations.AlterField(
            model_name='contextrecord',
            name='multi_polygon_source',
            field=models.CharField(blank=True, choices=[('T', 'Commune'), ('P', 'Précis'), ('M', 'Polygone')], max_length=1, null=True, verbose_name='Source du multi-polygone'),
        ),
        migrations.AlterField(
            model_name='contextrecord',
            name='multi_polygon_source_item',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='Élément source du multi-polygone'),
        ),
        migrations.AlterField(
            model_name='contextrecord',
            name='point_source',
            field=models.CharField(blank=True, choices=[('T', 'Commune'), ('P', 'Précis'), ('M', 'Polygone')], max_length=1, null=True, verbose_name='Source du point'),
        ),
        migrations.AlterField(
            model_name='contextrecord',
            name='point_source_item',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='Élément source du point'),
        ),
        migrations.AlterField(
            model_name='historicalcontextrecord',
            name='multi_polygon_source',
            field=models.CharField(blank=True, choices=[('T', 'Commune'), ('P', 'Précis'), ('M', 'Polygone')], max_length=1, null=True, verbose_name='Source du multi-polygone'),
        ),
        migrations.AlterField(
            model_name='historicalcontextrecord',
            name='multi_polygon_source_item',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='Élément source du multi-polygone'),
        ),
        migrations.AlterField(
            model_name='historicalcontextrecord',
            name='point_source',
            field=models.CharField(blank=True, choices=[('T', 'Commune'), ('P', 'Précis'), ('M', 'Polygone')], max_length=1, null=True, verbose_name='Source du point'),
        ),
        migrations.AlterField(
            model_name='historicalcontextrecord',
            name='point_source_item',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='Élément source du point'),
        ),
        migrations.AlterField(
            model_name='relationtype',
            name='logical_relation',
            field=models.CharField(blank=True, choices=[('above', 'Au-dessus'), ('bellow', 'En dessous'), ('equal', 'Égal')], max_length=10, null=True, verbose_name='Relation logique'),
        ),
    ]
