# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2019-12-16 10:13
from __future__ import unicode_literals

import django.contrib.postgres.fields.jsonb
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('archaeological_context_records', '0054_auto_20190910_1324'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contextrecord',
            name='data',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default={}),
        ),
        migrations.AlterField(
            model_name='historicalcontextrecord',
            name='data',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, default={}),
        ),
    ]
