# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('archaeological_operations', '0001_initial'),
        ('archaeological_context_records', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicalcontextrecord',
            name='operation',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='archaeological_operations.Operation', null=True),
        ),
        migrations.AddField(
            model_name='historicalcontextrecord',
            name='parcel',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='archaeological_operations.Parcel', null=True),
        ),
        migrations.AddField(
            model_name='historicalcontextrecord',
            name='unit',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='archaeological_context_records.Unit', null=True),
        ),
        migrations.AddField(
            model_name='dating',
            name='dating_type',
            field=models.ForeignKey(verbose_name='Dating type', blank=True, to='archaeological_context_records.DatingType', null=True),
        ),
        migrations.AddField(
            model_name='dating',
            name='period',
            field=models.ForeignKey(verbose_name='Period', to='archaeological_operations.Period'),
        ),
        migrations.AddField(
            model_name='dating',
            name='quality',
            field=models.ForeignKey(verbose_name='Quality', blank=True, to='archaeological_context_records.DatingQuality', null=True),
        ),
    ]
