# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations
from archaeological_context_records.models import RecordRelationView, CRBulkView


class Migration(migrations.Migration):

    dependencies = [
        ('archaeological_context_records', '0003_auto_20170414_2123'),
    ]

    operations = [
        migrations.RunSQL(RecordRelationView.CREATE_SQL +
                          CRBulkView.CREATE_SQL),
    ]
