# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import re
import django.contrib.gis.db.models.fields
import django.db.models.deletion
from django.conf import settings
import ishtar_common.models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='RecordRelationView',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'db_table': 'record_relations',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='ActivityType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label', models.CharField(max_length=100, verbose_name='Label')),
                ('txt_idx', models.CharField(unique=True, max_length=100, verbose_name='Textual ID', validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+$'), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')])),
                ('comment', models.TextField(null=True, verbose_name='Comment', blank=True)),
                ('available', models.BooleanField(default=True, verbose_name='Available')),
                ('order', models.IntegerField(verbose_name='Order')),
            ],
            options={
                'ordering': ('order',),
                'verbose_name': 'Activity Type',
                'verbose_name_plural': 'Activity Types',
            },
            bases=(ishtar_common.models.Cached, models.Model),
        ),
        migrations.CreateModel(
            name='ContextRecord',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(max_length=255, null=True, upload_to=b'upload/', blank=True)),
                ('thumbnail', models.ImageField(max_length=255, null=True, upload_to=b'upload/thumbs/', blank=True)),
                ('external_id', models.TextField(null=True, verbose_name='External ID', blank=True)),
                ('auto_external_id', models.BooleanField(default=False, verbose_name='External ID is set automatically')),
                ('label', models.CharField(max_length=200, verbose_name='ID')),
                ('description', models.TextField(null=True, verbose_name='Description', blank=True)),
                ('comment', models.TextField(null=True, verbose_name='General comment', blank=True)),
                ('opening_date', models.DateField(null=True, verbose_name="Date d'ouverture", blank=True)),
                ('closing_date', models.DateField(null=True, verbose_name='End date', blank=True)),
                ('length', models.FloatField(null=True, verbose_name='Length (m)', blank=True)),
                ('width', models.FloatField(null=True, verbose_name='Width (m)', blank=True)),
                ('thickness', models.FloatField(null=True, verbose_name='Thickness (m)', blank=True)),
                ('diameter', models.FloatField(null=True, verbose_name='Diameter (m)', blank=True)),
                ('depth', models.FloatField(null=True, verbose_name='Depth (m)', blank=True)),
                ('depth_of_appearance', models.FloatField(null=True, verbose_name='Depth of appearance (m)', blank=True)),
                ('location', models.TextField(help_text='A short description of the location of the context record', null=True, verbose_name='Location', blank=True)),
                ('datings_comment', models.TextField(null=True, verbose_name='Comment on datings', blank=True)),
                ('filling', models.TextField(null=True, verbose_name='Filling', blank=True)),
                ('interpretation', models.TextField(null=True, verbose_name='Interpretation', blank=True)),
                ('taq', models.IntegerField(help_text='"Terminus Ante Quem" the context record can\'t have been created after this date', null=True, verbose_name='TAQ', blank=True)),
                ('taq_estimated', models.IntegerField(help_text='Estimation of a "Terminus Ante Quem"', null=True, verbose_name='Estimated TAQ', blank=True)),
                ('tpq', models.IntegerField(help_text='"Terminus Post Quem" the context record can\'t have been created before this date', null=True, verbose_name='TPQ', blank=True)),
                ('tpq_estimated', models.IntegerField(help_text='Estimation of a "Terminus Post Quem"', null=True, verbose_name='Estimated TPQ', blank=True)),
                ('point', django.contrib.gis.db.models.fields.PointField(srid=4326, dim=3, null=True, verbose_name='Point', blank=True)),
                ('polygon', django.contrib.gis.db.models.fields.PolygonField(srid=4326, null=True, verbose_name='Polygon', blank=True)),
                ('cached_label', models.TextField(null=True, verbose_name='Cached name', blank=True)),
            ],
            options={
                'ordering': ('cached_label',),
                'verbose_name': 'Context Record',
                'verbose_name_plural': 'Context Record',
                'permissions': (('view_contextrecord', "Peut voir toutes les Unit\xe9s d'Enregistrement"), ('view_own_contextrecord', "Peut voir sa propre Unit\xe9 d'Enregistrement"), ('add_own_contextrecord', "Peut ajouter sa propre Unit\xe9 d'Enregistrement"), ('change_own_contextrecord', "Peut modifier sa propre Unit\xe9 d'Enregistrement"), ('delete_own_contextrecord', "Peut supprimer sa propre Unit\xe9 d'Enregistrement")),
            },
            bases=(models.Model, ishtar_common.models.OwnPerms, ishtar_common.models.ValueGetter, ishtar_common.models.ShortMenuItem),
        ),
        migrations.CreateModel(
            name='ContextRecordSource',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(max_length=255, null=True, upload_to=b'upload/', blank=True)),
                ('thumbnail', models.ImageField(max_length=255, null=True, upload_to=b'upload/thumbs/', blank=True)),
                ('title', models.CharField(max_length=300, verbose_name='Title')),
                ('external_id', models.TextField(max_length=300, null=True, verbose_name='External ID', blank=True)),
                ('scale', models.CharField(max_length=30, null=True, verbose_name='Scale', blank=True)),
                ('associated_url', models.URLField(null=True, verbose_name='Numerical ressource (web address)', blank=True)),
                ('receipt_date', models.DateField(null=True, verbose_name='Receipt date', blank=True)),
                ('creation_date', models.DateField(null=True, verbose_name='Creation date', blank=True)),
                ('receipt_date_in_documentation', models.DateField(null=True, verbose_name='Receipt date in documentation', blank=True)),
                ('item_number', models.IntegerField(default=1, verbose_name='Item number')),
                ('reference', models.CharField(max_length=100, null=True, verbose_name='Ref.', blank=True)),
                ('internal_reference', models.CharField(max_length=100, null=True, verbose_name='Internal ref.', blank=True)),
                ('description', models.TextField(null=True, verbose_name='Description', blank=True)),
                ('comment', models.TextField(null=True, verbose_name='Comment', blank=True)),
                ('additional_information', models.TextField(null=True, verbose_name='Additional information', blank=True)),
                ('duplicate', models.BooleanField(default=False, verbose_name='Has a duplicate')),
            ],
            options={
                'verbose_name': 'Context record documentation',
                'verbose_name_plural': 'Context record documentations',
                'permissions': (('view_contextrecordsource', 'Can view all Context record sources'), ('view_own_contextrecordsource', 'Can view own Context record source'), ('add_own_contextrecordsource', 'Can add own Context record source'), ('change_own_contextrecordsource', 'Can change own Context record source'), ('delete_own_contextrecordsource', 'Can delete own Context record source')),
            },
            bases=(ishtar_common.models.OwnPerms, models.Model),
        ),
        migrations.CreateModel(
            name='Dating',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('start_date', models.IntegerField(null=True, verbose_name='Start date', blank=True)),
                ('end_date', models.IntegerField(null=True, verbose_name='End date', blank=True)),
                ('precise_dating', models.TextField(null=True, verbose_name='Precise dating', blank=True)),
            ],
            options={
                'verbose_name': 'Dating',
                'verbose_name_plural': 'Datings',
            },
        ),
        migrations.CreateModel(
            name='DatingQuality',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label', models.CharField(max_length=100, verbose_name='Label')),
                ('txt_idx', models.CharField(unique=True, max_length=100, verbose_name='Textual ID', validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+$'), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')])),
                ('comment', models.TextField(null=True, verbose_name='Comment', blank=True)),
                ('available', models.BooleanField(default=True, verbose_name='Available')),
            ],
            options={
                'ordering': ('label',),
                'verbose_name': 'Dating quality type',
                'verbose_name_plural': 'Dating quality types',
            },
            bases=(ishtar_common.models.Cached, models.Model),
        ),
        migrations.CreateModel(
            name='DatingType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label', models.CharField(max_length=100, verbose_name='Label')),
                ('txt_idx', models.CharField(unique=True, max_length=100, verbose_name='Textual ID', validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+$'), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')])),
                ('comment', models.TextField(null=True, verbose_name='Comment', blank=True)),
                ('available', models.BooleanField(default=True, verbose_name='Available')),
            ],
            options={
                'ordering': ('label',),
                'verbose_name': 'Dating type',
                'verbose_name_plural': 'Dating types',
            },
            bases=(ishtar_common.models.Cached, models.Model),
        ),
        migrations.CreateModel(
            name='DocumentationType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label', models.CharField(max_length=100, verbose_name='Label')),
                ('txt_idx', models.CharField(unique=True, max_length=100, verbose_name='Textual ID', validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+$'), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')])),
                ('comment', models.TextField(null=True, verbose_name='Comment', blank=True)),
                ('available', models.BooleanField(default=True, verbose_name='Available')),
            ],
            options={
                'ordering': ('label',),
                'verbose_name': 'Documentation type',
                'verbose_name_plural': 'Documentation types',
            },
            bases=(ishtar_common.models.Cached, models.Model),
        ),
        migrations.CreateModel(
            name='ExcavationTechnicType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label', models.CharField(max_length=100, verbose_name='Label')),
                ('txt_idx', models.CharField(unique=True, max_length=100, verbose_name='Textual ID', validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+$'), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')])),
                ('comment', models.TextField(null=True, verbose_name='Comment', blank=True)),
                ('available', models.BooleanField(default=True, verbose_name='Available')),
            ],
            options={
                'ordering': ('label',),
                'verbose_name': 'Excavation technique type',
                'verbose_name_plural': 'Excavation technique types',
            },
            bases=(ishtar_common.models.Cached, models.Model),
        ),
        migrations.CreateModel(
            name='HistoricalContextRecord',
            fields=[
                ('id', models.IntegerField(verbose_name='ID', db_index=True, auto_created=True, blank=True)),
                ('image', models.TextField(max_length=255, null=True, blank=True)),
                ('thumbnail', models.TextField(max_length=255, null=True, blank=True)),
                ('external_id', models.TextField(null=True, verbose_name='External ID', blank=True)),
                ('auto_external_id', models.BooleanField(default=False, verbose_name='External ID is set automatically')),
                ('label', models.CharField(max_length=200, verbose_name='ID')),
                ('description', models.TextField(null=True, verbose_name='Description', blank=True)),
                ('comment', models.TextField(null=True, verbose_name='General comment', blank=True)),
                ('opening_date', models.DateField(null=True, verbose_name="Date d'ouverture", blank=True)),
                ('closing_date', models.DateField(null=True, verbose_name='End date', blank=True)),
                ('length', models.FloatField(null=True, verbose_name='Length (m)', blank=True)),
                ('width', models.FloatField(null=True, verbose_name='Width (m)', blank=True)),
                ('thickness', models.FloatField(null=True, verbose_name='Thickness (m)', blank=True)),
                ('diameter', models.FloatField(null=True, verbose_name='Diameter (m)', blank=True)),
                ('depth', models.FloatField(null=True, verbose_name='Depth (m)', blank=True)),
                ('depth_of_appearance', models.FloatField(null=True, verbose_name='Depth of appearance (m)', blank=True)),
                ('location', models.TextField(help_text='A short description of the location of the context record', null=True, verbose_name='Location', blank=True)),
                ('datings_comment', models.TextField(null=True, verbose_name='Comment on datings', blank=True)),
                ('filling', models.TextField(null=True, verbose_name='Filling', blank=True)),
                ('interpretation', models.TextField(null=True, verbose_name='Interpretation', blank=True)),
                ('taq', models.IntegerField(help_text='"Terminus Ante Quem" the context record can\'t have been created after this date', null=True, verbose_name='TAQ', blank=True)),
                ('taq_estimated', models.IntegerField(help_text='Estimation of a "Terminus Ante Quem"', null=True, verbose_name='Estimated TAQ', blank=True)),
                ('tpq', models.IntegerField(help_text='"Terminus Post Quem" the context record can\'t have been created before this date', null=True, verbose_name='TPQ', blank=True)),
                ('tpq_estimated', models.IntegerField(help_text='Estimation of a "Terminus Post Quem"', null=True, verbose_name='Estimated TPQ', blank=True)),
                ('point', django.contrib.gis.db.models.fields.PointField(srid=4326, dim=3, null=True, verbose_name='Point', blank=True)),
                ('polygon', django.contrib.gis.db.models.fields.PolygonField(srid=4326, null=True, verbose_name='Polygon', blank=True)),
                ('cached_label', models.TextField(null=True, verbose_name='Cached name', blank=True)),
                ('history_id', models.AutoField(serialize=False, primary_key=True)),
                ('history_date', models.DateTimeField()),
                ('history_type', models.CharField(max_length=1, choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')])),
                ('activity', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='archaeological_context_records.ActivityType', null=True)),
                ('excavation_technic', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='archaeological_context_records.ExcavationTechnicType', null=True)),
                ('history_creator', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('history_modifier', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('history_user', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
                'verbose_name': 'historical Context Record',
            },
        ),
        migrations.CreateModel(
            name='IdentificationType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label', models.CharField(max_length=100, verbose_name='Label')),
                ('txt_idx', models.CharField(unique=True, max_length=100, verbose_name='Textual ID', validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+$'), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')])),
                ('comment', models.TextField(null=True, verbose_name='Comment', blank=True)),
                ('available', models.BooleanField(default=True, verbose_name='Available')),
                ('order', models.IntegerField(verbose_name='Order')),
            ],
            options={
                'ordering': ('order', 'label'),
                'verbose_name': 'Identification Type',
                'verbose_name_plural': 'Identification Types',
            },
            bases=(ishtar_common.models.Cached, models.Model),
        ),
        migrations.CreateModel(
            name='RecordRelations',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('left_record', models.ForeignKey(related_name='right_relations', to='archaeological_context_records.ContextRecord')),
            ],
            options={
                'verbose_name': 'Record relation',
                'verbose_name_plural': 'Record relations',
            },
            bases=(ishtar_common.models.GeneralRecordRelations, models.Model),
        ),
        migrations.CreateModel(
            name='RelationType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label', models.CharField(max_length=100, verbose_name='Label')),
                ('txt_idx', models.CharField(unique=True, max_length=100, verbose_name='Textual ID', validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+$'), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')])),
                ('comment', models.TextField(null=True, verbose_name='Comment', blank=True)),
                ('available', models.BooleanField(default=True, verbose_name='Available')),
                ('order', models.IntegerField(default=1, verbose_name='Order')),
                ('symmetrical', models.BooleanField(verbose_name='Symmetrical')),
                ('tiny_label', models.CharField(max_length=50, null=True, verbose_name='Tiny label', blank=True)),
                ('inverse_relation', models.ForeignKey(verbose_name='Inverse relation', blank=True, to='archaeological_context_records.RelationType', null=True)),
            ],
            options={
                'ordering': ('order', 'label'),
                'verbose_name': 'Relation type',
                'verbose_name_plural': 'Relation types',
            },
            bases=(ishtar_common.models.Cached, models.Model),
        ),
        migrations.CreateModel(
            name='Unit',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label', models.CharField(max_length=100, verbose_name='Label')),
                ('txt_idx', models.CharField(unique=True, max_length=100, verbose_name='Textual ID', validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+$'), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')])),
                ('comment', models.TextField(null=True, verbose_name='Comment', blank=True)),
                ('available', models.BooleanField(default=True, verbose_name='Available')),
                ('order', models.IntegerField(verbose_name='Order')),
                ('parent', models.ForeignKey(verbose_name='Parent context record type', blank=True, to='archaeological_context_records.Unit', null=True)),
            ],
            options={
                'ordering': ('order', 'label'),
                'verbose_name': 'Context record Type',
                'verbose_name_plural': 'Context record Types',
            },
            bases=(ishtar_common.models.Cached, models.Model),
        ),
        migrations.AddField(
            model_name='recordrelations',
            name='relation_type',
            field=models.ForeignKey(to='archaeological_context_records.RelationType'),
        ),
        migrations.AddField(
            model_name='recordrelations',
            name='right_record',
            field=models.ForeignKey(related_name='left_relations', to='archaeological_context_records.ContextRecord'),
        ),
        migrations.AddField(
            model_name='historicalcontextrecord',
            name='identification',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='archaeological_context_records.IdentificationType', null=True),
        ),
    ]
