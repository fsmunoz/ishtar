# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('archaeological_context_records', '0005_auto_20170802_1557'),
    ]

    operations = [
        migrations.AddField(
            model_name='contextrecord',
            name='point_2d',
            field=django.contrib.gis.db.models.fields.PointField(srid=4326, null=True, verbose_name='Point (2D)', blank=True),
        ),
        migrations.AddField(
            model_name='historicalcontextrecord',
            name='point_2d',
            field=django.contrib.gis.db.models.fields.PointField(srid=4326, null=True, verbose_name='Point (2D)', blank=True),
        ),
    ]
