# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-06-04 15:32
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ishtar_common', '0054_auto_20180525_1249'),
        ('archaeological_context_records', '0023_auto_20180511_1232'),
    ]

    operations = [
        migrations.AddField(
            model_name='contextrecord',
            name='town',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='context_record', to='ishtar_common.Town', verbose_name='Town'),
        ),
        migrations.AddField(
            model_name='historicalcontextrecord',
            name='town',
            field=models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to='ishtar_common.Town'),
        ),
    ]
