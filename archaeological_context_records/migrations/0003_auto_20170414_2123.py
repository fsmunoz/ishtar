# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('archaeological_operations', '0001_initial'),
        ('archaeological_context_records', '0002_auto_20170414_2123'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('ishtar_common', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='contextrecordsource',
            name='authors',
            field=models.ManyToManyField(related_name='contextrecordsource_related', verbose_name='Authors', to='ishtar_common.Author'),
        ),
        migrations.AddField(
            model_name='contextrecordsource',
            name='context_record',
            field=models.ForeignKey(related_name='source', verbose_name='Context record', to='archaeological_context_records.ContextRecord'),
        ),
        migrations.AddField(
            model_name='contextrecordsource',
            name='format_type',
            field=models.ForeignKey(verbose_name='Format', blank=True, to='ishtar_common.Format', null=True),
        ),
        migrations.AddField(
            model_name='contextrecordsource',
            name='source_type',
            field=models.ForeignKey(verbose_name='Type', to='ishtar_common.SourceType'),
        ),
        migrations.AddField(
            model_name='contextrecordsource',
            name='support_type',
            field=models.ForeignKey(verbose_name='Support', blank=True, to='ishtar_common.SupportType', null=True),
        ),
        migrations.AddField(
            model_name='contextrecord',
            name='activity',
            field=models.ForeignKey(verbose_name='Activity', blank=True, to='archaeological_context_records.ActivityType', null=True),
        ),
        migrations.AddField(
            model_name='contextrecord',
            name='datings',
            field=models.ManyToManyField(related_name='context_records', to='archaeological_context_records.Dating'),
        ),
        migrations.AddField(
            model_name='contextrecord',
            name='documentations',
            field=models.ManyToManyField(to='archaeological_context_records.DocumentationType', blank=True),
        ),
        migrations.AddField(
            model_name='contextrecord',
            name='excavation_technic',
            field=models.ForeignKey(verbose_name='Excavation technique', blank=True, to='archaeological_context_records.ExcavationTechnicType', null=True),
        ),
        migrations.AddField(
            model_name='contextrecord',
            name='history_creator',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Creator', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='contextrecord',
            name='history_modifier',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Last editor', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='contextrecord',
            name='identification',
            field=models.ForeignKey(verbose_name='Identification', blank=True, to='archaeological_context_records.IdentificationType', null=True),
        ),
        migrations.AddField(
            model_name='contextrecord',
            name='imports',
            field=models.ManyToManyField(related_name='imported_archaeological_context_records_contextrecord', to='ishtar_common.Import', blank=True),
        ),
        migrations.AddField(
            model_name='contextrecord',
            name='operation',
            field=models.ForeignKey(related_name='context_record', verbose_name='Operation', to='archaeological_operations.Operation'),
        ),
        migrations.AddField(
            model_name='contextrecord',
            name='parcel',
            field=models.ForeignKey(related_name='context_record', verbose_name='Parcel', to='archaeological_operations.Parcel'),
        ),
        migrations.AddField(
            model_name='contextrecord',
            name='related_context_records',
            field=models.ManyToManyField(to='archaeological_context_records.ContextRecord', through='archaeological_context_records.RecordRelations', blank=True),
        ),
        migrations.AddField(
            model_name='contextrecord',
            name='unit',
            field=models.ForeignKey(related_name='+', verbose_name='Context record type', blank=True, to='archaeological_context_records.Unit', null=True),
        ),
    ]
