Install
=======

Install from Git
----------------

This procedure only works for Debian Stretch for now.
If you know bash and your distribution the script should be easy to adapt (contributions are welcomed!).

```

VERSION=latest  # could also be 'develop'
# get the last install script (only work for Debian)
wget https://ishtar-archeo.net/install/install-ishtar-$VERSION.tar.bz2
tar xvjf install-ishtar-$VERSION.tar.bz2
rm install-ishtar-$VERSION.tar.bz2

# install Ishtar script
cd install
bash ./ishtar-install
```

```
• Default PostgreSQL host? [localhost]
• Which webserver do you want to use? ([nginx]/none)
• Which version would you like to use? ([develop]/master)
• Which base install path for Ishtar? [/srv/]
```
```
# create a new instance
bash ./ishtar-prepare-instance
```

```
• Which instance code name? [my_ishtar_instance]
• Which url? my_ishtar_instance.net
```

```
# you would probably like to load a fixture with towns (french dataset here)
cd /srv
mkdir data
cd data
wget "https://ishtar-archeo.net/fixtures/initial_towns-fr-latest.tar.bz2"
tar xvjf initial_towns-fr-latest.tar.bz2
rm initial_towns-fr-latest.tar.bz2

cd /srv/ishtar/my_ishtar_instance
./manage.py loaddata /srv/data/towns_norel-fr.json
./manage.py loaddata /srv/data/towns-fr.json
```
