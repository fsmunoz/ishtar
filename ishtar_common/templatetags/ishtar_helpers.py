#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.template import Library
from django.utils.text import mark_safe

register = Library()


@register.filter
def or_(value1, value2):
    return value1 or value2


@register.filter
def and_(value1, value2):
    return value1 and value2


@register.filter
def safe_or(item, args):
    if not item:
        return False
    for arg in args.split("|"):
        result = True
        current_item = item
        for sub in arg.split("."):
            if not hasattr(current_item, sub) or not getattr(current_item, sub):
                result = False
                break
            current_item = getattr(current_item, sub)
        if result:
            return True


@register.filter
def file_content(value):
    if value:
        return mark_safe(value.read())
    return ""


@register.filter
def is_locked(item, user):
    if not hasattr(item, "is_locked"):
        return False
    return item.is_locked(user)
