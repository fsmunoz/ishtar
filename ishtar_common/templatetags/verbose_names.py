from django import template
register = template.Library()


@register.simple_tag
def get_verbose_field_name(instance, field_name):
    """
    Returns verbose_name for a field.
    """
    return instance._meta.get_field(field_name).verbose_name.title()


@register.simple_tag
def get_verbose_model_name(instance):
    """
    Returns verbose_name for a model.
    """
    return instance._meta.verbose_name.title()


@register.filter
def verbose_model_name(instance):
    return instance._meta.verbose_name.title()
