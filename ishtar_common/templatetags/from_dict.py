#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.template import Library

register = Library()


@register.filter
def from_dict(value, dct):
    if not dct or value not in dct:
        return ''
    return dct[value]


@register.filter
def call(value, call):
    args = call.split(",")
    return getattr(value, args[0])(*args[1:])


