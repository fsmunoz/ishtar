# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('ishtar_common', '0002_change_ishtaruser_management'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ishtaruser',
            name='user_ptr',
            field=models.OneToOneField(related_name='ishtaruser', primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL),
        ),
    ]
