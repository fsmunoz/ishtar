# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-09-26 10:23
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ishtar_common', '0070_profiletypesummary'),
    ]

    operations = [
        migrations.AddField(
            model_name='area',
            name='reference',
            field=models.CharField(blank=True, max_length=200, null=True, verbose_name='Reference'),
        ),
        migrations.AlterField(
            model_name='jsondatafield',
            name='value_type',
            field=models.CharField(choices=[(b'T', 'Text'), (b'LT', 'Long text'), (b'I', 'Integer'), (b'B', 'Boolean'), (b'F', 'Float'), (b'D', 'Date'), (b'C', 'Choices')], default=b'T', max_length=10, verbose_name='Type'),
        ),
    ]
