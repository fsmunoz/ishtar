# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-12-13 15:13
from __future__ import unicode_literals

from django.db import migrations


def migrate_importer(apps, schema):
    ImporterDuplicateField = apps.get_model('ishtar_common',
                                            'ImporterDuplicateField')
    ImportTarget = apps.get_model('ishtar_common', 'ImportTarget')

    idx = 0
    for k, model in (('field_name', ImporterDuplicateField),
                     ('target', ImportTarget),):
        q = model.objects.filter(
            **{k + "__icontains": 'container'}
        ).exclude(
            **{k + "__icontains": 'container_ref'}
        )
        for item in q.all():
            value = getattr(item, k).replace(
                'container', 'container_ref').replace(
                'container_ref_type', 'container_type')

            dup_dct = {"column": item.column,
                       "field_name": value}
            q2 = ImporterDuplicateField.objects.filter(
                **dup_dct
            )
            if q2.count():
                continue
            idx += 1
            if item.concat_str:
                dup_dct['concat_str'] = item.concat_str
            if item.concat:
                dup_dct['concat'] = item.concat
            ImporterDuplicateField.objects.create(**dup_dct)
        q = model.objects.filter(
            **{k + "__icontains": 'set_localisation'}
        )
        for item in q.all():
            value = getattr(item, k).replace(
                'set_localisation', 'set_reference_localisation')
            dup_dct = {"column": item.column,
                       "field_name": value}
            q2 = ImporterDuplicateField.objects.filter(
                **dup_dct
            )
            if q2.count():
                continue
            idx += 1
            if item.concat_str:
                dup_dct['concat_str'] = item.concat_str
            if item.concat:
                dup_dct['concat'] = item.concat
            ImporterDuplicateField.objects.create(**dup_dct)
    if idx:
        print("{} dup field created".format(idx))


class Migration(migrations.Migration):

    dependencies = [
        ('ishtar_common', '0078_auto_20181203_1442'),
    ]

    operations = [
        migrations.RunPython(migrate_importer)
    ]
