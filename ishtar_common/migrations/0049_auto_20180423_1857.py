# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-04-23 18:57
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ishtar_common', '0048_auto_20180423_1838'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ishtarimage',
            name='name',
            field=models.CharField(blank=True, max_length=250, null=True, verbose_name='Name'),
        ),
    ]
