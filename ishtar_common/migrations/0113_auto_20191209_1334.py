# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2019-12-09 13:34
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ishtar_common', '0112_document_qrcode'),
    ]

    operations = [
        migrations.AddField(
            model_name='ishtarsiteprofile',
            name='warning_message',
            field=models.TextField(blank=True, verbose_name='Warning message'),
        ),
        migrations.AddField(
            model_name='ishtarsiteprofile',
            name='warning_name',
            field=models.TextField(blank=True, verbose_name='Warning name'),
        ),
    ]
