# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-09-03 10:15
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ishtar_common', '0069_userprofile_show_field_number'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProfileTypeSummary',
            fields=[
            ],
            options={
                'verbose_name': 'Profile type summary',
                'proxy': True,
                'verbose_name_plural': 'Profile types summary',
                'indexes': [],
            },
            bases=('ishtar_common.profiletype',),
        ),
    ]
