# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2019-09-09 13:42
from __future__ import unicode_literals

from django.conf import settings
import django.contrib.gis.db.models.fields
import django.contrib.postgres.search
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import re


class Migration(migrations.Migration):

    dependencies = [
        ('ishtar_common', '0102_auto_20190909_1300'),
    ]

    operations = [
        migrations.AddField(
            model_name='exporttask',
            name='result_info',
            field=models.TextField(blank=True, null=True, verbose_name='Result information'),
        ),
        migrations.AddField(
            model_name='exporttask',
            name='export_conf',
            field=models.BooleanField(default=True, verbose_name='Export configuration'),
        ),
        migrations.AddField(
            model_name='exporttask',
            name='export_dir',
            field=models.BooleanField(default=True, verbose_name='Export directory'),
        ),
        migrations.AddField(
            model_name='exporttask',
            name='export_docs',
            field=models.BooleanField(default=True, verbose_name='Export documents'),
        ),
        migrations.AddField(
            model_name='exporttask',
            name='export_geo',
            field=models.BooleanField(default=True, verbose_name='Export towns, areas...'),
        ),
        migrations.AddField(
            model_name='exporttask',
            name='export_importers',
            field=models.BooleanField(default=True, verbose_name='Export importers'),
        ),
        migrations.AddField(
            model_name='exporttask',
            name='export_items',
            field=models.BooleanField(default=True, verbose_name='Export main items'),
        ),
        migrations.AddField(
            model_name='exporttask',
            name='export_types',
            field=models.BooleanField(default=True, verbose_name='Export types'),
        ),
    ]
