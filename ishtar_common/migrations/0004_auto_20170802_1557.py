# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ishtar_common', '0003_auto_20170421_1613'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='organization',
            options={'verbose_name': 'Organization', 'verbose_name_plural': 'Organizations', 'permissions': (('view_organization', 'Can view all Organizations'), ('view_own_organization', 'Can view own Organization'), ('add_own_organization', 'Can add own Organization'), ('change_own_organization', 'Can change own Organization'), ('delete_own_organization', 'Can delete own Organization'))},
        ),
        migrations.AlterModelOptions(
            name='person',
            options={'verbose_name': 'Person', 'verbose_name_plural': 'Persons', 'permissions': (('view_person', 'Can view all Persons'), ('view_own_person', 'Can view own Person'), ('add_own_person', 'Can add own Person'), ('change_own_person', 'Can change own Person'), ('delete_own_person', 'Can delete own Person'))},
        ),
    ]
