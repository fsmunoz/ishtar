# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from ishtar_common.utils import create_slug


def dt_create_slug(apps, schema):
    DocumentTemplate = apps.get_model('ishtar_common', 'documenttemplate')
    for dt in DocumentTemplate.objects.all():
        dt.slug = create_slug(DocumentTemplate, dt.name)
        dt.save()


class Migration(migrations.Migration):

    dependencies = [
        ('ishtar_common', '0006_auto_20170811_2129'),
    ]

    operations = [
        migrations.AddField(
            model_name='documenttemplate',
            name='slug',
            field=models.SlugField(null=True, max_length=100, blank=True, unique=True, verbose_name='Slug'),
        ),
        migrations.RunPython(dt_create_slug),
    ]
