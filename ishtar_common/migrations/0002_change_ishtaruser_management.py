# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('ishtar_common', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelManagers(
            name='ishtaruser',
            managers=[
            ],
        ),
        migrations.AlterField(
            model_name='ishtaruser',
            name='user_ptr',
            field=models.OneToOneField(primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL),
        ),
    ]
