# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2019-12-16 10:18
from __future__ import unicode_literals

from django.db import migrations
from django.contrib.postgres.operations import BtreeGinExtension


class Migration(migrations.Migration):

    dependencies = [
        ('ishtar_common', '0115_auto_20191216_1013'),
    ]

    operations = [
        BtreeGinExtension()
    ]
