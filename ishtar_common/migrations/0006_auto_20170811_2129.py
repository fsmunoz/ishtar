# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ishtar_common', '0005_auto_20170804_2023'),
    ]

    operations = [
        migrations.AlterField(
            model_name='importermodel',
            name='klass',
            field=models.CharField(unique=True, max_length=200, verbose_name='Class name'),
        ),
        migrations.AlterField(
            model_name='regexp',
            name='name',
            field=models.CharField(unique=True, max_length=100, verbose_name='Name'),
        ),
        migrations.AlterUniqueTogether(
            name='importerdefault',
            unique_together=set([('importer_type', 'target')]),
        ),
        migrations.AlterUniqueTogether(
            name='importtarget',
            unique_together=set([('column', 'target')]),
        ),
    ]
