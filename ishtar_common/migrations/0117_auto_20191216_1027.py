# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2019-12-16 10:27
from __future__ import unicode_literals

import django.contrib.postgres.indexes
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ishtar_common', '0116_create_gist_extension'),
    ]

    operations = [
        migrations.AddIndex(
            model_name='person',
            index=django.contrib.postgres.indexes.GinIndex(fields=['data'], name='ishtar_comm_data_a563fb_gin'),
        ),
        migrations.AddIndex(
            model_name='document',
            index=django.contrib.postgres.indexes.GinIndex(fields=['data'], name='ishtar_comm_data_c6da6f_gin'),
        ),
        migrations.AddIndex(
            model_name='organization',
            index=django.contrib.postgres.indexes.GinIndex(fields=['data'], name='ishtar_comm_data_36ecc0_gin'),
        ),
    ]
