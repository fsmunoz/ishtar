# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-02-18 17:08
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ishtar_common', '0028_auto_20180214_1144'),
    ]

    operations = [
        migrations.AlterField(
            model_name='formatertype',
            name='formater_type',
            field=models.CharField(choices=[(b'IntegerFormater', 'Integer'), (b'FloatFormater', 'Float'), (b'UnicodeFormater', 'String'), (b'DateFormater', 'Date'), (b'TypeFormater', 'Type'), (b'YearFormater', 'Year'), (b'InseeFormater', 'INSEE code'), (b'StrToBoolean', 'String to boolean'), (b'FileFormater', 'File'), (b'UnknowType', 'Unknow type')], max_length=20, verbose_name='Formater type'),
        ),
    ]
