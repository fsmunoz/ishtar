# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-08-26 11:53
from __future__ import unicode_literals

import django.core.validators
from django.db import migrations, models
import re


class Migration(migrations.Migration):

    dependencies = [
        ('ishtar_common', '0007_documenttemplate_slug'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='importerduplicatefield',
            options={'ordering': ('column', 'field_name'), 'verbose_name': 'Importer - Duplicate field', 'verbose_name_plural': 'Importer - Duplicate fields'},
        ),
        migrations.AlterField(
            model_name='authortype',
            name='txt_idx',
            field=models.CharField(help_text='The slug is the standardized version of the name. It contains only lowercase letters, numbers and hyphens. Each slug must be unique.', max_length=100, unique=True, validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+\\Z'), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')], verbose_name='Textual ID'),
        ),
        migrations.AlterField(
            model_name='format',
            name='txt_idx',
            field=models.CharField(help_text='The slug is the standardized version of the name. It contains only lowercase letters, numbers and hyphens. Each slug must be unique.', max_length=100, unique=True, validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+\\Z'), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')], verbose_name='Textual ID'),
        ),
        migrations.AlterField(
            model_name='ishtarsiteprofile',
            name='currency',
            field=models.CharField(choices=[('\u20ac', 'Euro'), ('$', 'US dollar')], default='\u20ac', max_length=5, verbose_name='Currency'),
        ),
        migrations.AlterField(
            model_name='operationtype',
            name='txt_idx',
            field=models.CharField(help_text='The slug is the standardized version of the name. It contains only lowercase letters, numbers and hyphens. Each slug must be unique.', max_length=100, unique=True, validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+\\Z'), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')], verbose_name='Textual ID'),
        ),
        migrations.AlterField(
            model_name='organizationtype',
            name='txt_idx',
            field=models.CharField(help_text='The slug is the standardized version of the name. It contains only lowercase letters, numbers and hyphens. Each slug must be unique.', max_length=100, unique=True, validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+\\Z'), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')], verbose_name='Textual ID'),
        ),
        migrations.AlterField(
            model_name='persontype',
            name='txt_idx',
            field=models.CharField(help_text='The slug is the standardized version of the name. It contains only lowercase letters, numbers and hyphens. Each slug must be unique.', max_length=100, unique=True, validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+\\Z'), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')], verbose_name='Textual ID'),
        ),
        migrations.AlterField(
            model_name='sourcetype',
            name='txt_idx',
            field=models.CharField(help_text='The slug is the standardized version of the name. It contains only lowercase letters, numbers and hyphens. Each slug must be unique.', max_length=100, unique=True, validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+\\Z'), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')], verbose_name='Textual ID'),
        ),
        migrations.AlterField(
            model_name='spatialreferencesystem',
            name='txt_idx',
            field=models.CharField(help_text='The slug is the standardized version of the name. It contains only lowercase letters, numbers and hyphens. Each slug must be unique.', max_length=100, unique=True, validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+\\Z'), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')], verbose_name='Textual ID'),
        ),
        migrations.AlterField(
            model_name='supporttype',
            name='txt_idx',
            field=models.CharField(help_text='The slug is the standardized version of the name. It contains only lowercase letters, numbers and hyphens. Each slug must be unique.', max_length=100, unique=True, validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+\\Z'), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')], verbose_name='Textual ID'),
        ),
        migrations.AlterField(
            model_name='titletype',
            name='txt_idx',
            field=models.CharField(help_text='The slug is the standardized version of the name. It contains only lowercase letters, numbers and hyphens. Each slug must be unique.', max_length=100, unique=True, validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+\\Z'), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')], verbose_name='Textual ID'),
        ),
    ]
