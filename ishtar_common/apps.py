from __future__ import unicode_literals

from django.apps import AppConfig
from django.contrib.admin import AdminSite
from django.utils.translation import ugettext_lazy as _

import virtualtime


class IshtarAdminSite(AdminSite):
    site_header = _('Ishtar administration')
    site_title = _("Ishtar administration")


admin_site = IshtarAdminSite(name='ishtaradmin')


class TranslationOverloadConfig(AppConfig):
    name = "overload_translation"
    verbose_name = _("Translation - Overload")


class ArchaeologicalContextRecordConfig(AppConfig):
    name = 'archaeological_context_records'
    verbose_name = _("Ishtar - Context record")


class ArchaeologicalFilesConfig(AppConfig):
    name = 'archaeological_files'
    verbose_name = _("Ishtar - File")


class ArchaeologicalFindsConfig(AppConfig):
    name = 'archaeological_finds'
    verbose_name = _("Ishtar - Find")


class ArchaeologicalOperationsConfig(AppConfig):
    name = 'archaeological_operations'
    verbose_name = _("Ishtar - Operation")


class ArchaeologicalWarehouseConfig(AppConfig):
    name = 'archaeological_warehouse'
    verbose_name = _("Ishtar - Warehouse")


class IshtarCommonConfig(AppConfig):
    name = 'ishtar_common'
    verbose_name = _("Ishtar - Common")

    def ready(self):
        virtualtime.enable()
