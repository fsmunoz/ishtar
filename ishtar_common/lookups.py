from ajax_select import register, LookupChannel as BaseLookupChannel

from django.conf import settings
from django.contrib.auth.models import User
from django.db.models import Q
from ishtar_common import models


class LookupChannel(BaseLookupChannel):
    def get_objects(self, items):
        # TODO: why IDs are not given here? M2M issue
        ids = []
        for item in items:
            if hasattr(item, 'pk'):
                ids.append(item.pk)
            else:
                ids.append(item)
        return super(LookupChannel, self).get_objects(ids)

    def format_item_display(self, item):
        return u"<span class='ajax-label'>%s</span>" % str(item)


class TypeLookupChannel(LookupChannel):
    def get_query(self, q, request):
        query = Q(label__icontains=q) | Q(label__icontains=q)
        return self.model.objects.filter(query).order_by('label')[:20]


@register('town')
class TownLookup(LookupChannel):
    model = models.Town

    def get_query(self, q, request):
        if settings.COUNTRY == 'fr':
            query = Q()
            for term in q.strip().split(' '):
                query &= (
                    Q(name__icontains=term) | Q(numero_insee__icontains=term)
                )
        else:
            query = Q(name__icontains=q)
        return self.model.objects.filter(query).order_by('name')[:20]


@register('user')
class UserLookup(LookupChannel):
    model = User

    def get_query(self, q, request):
        query = Q()
        for term in q.strip().split(' '):
            query &= (
                    Q(username__icontains=term) |
                    Q(first_name__icontains=term) |
                    Q(last_name__icontains=term) |
                    Q(email__icontains=term)
            )
        return self.model.objects.filter(query).order_by('username')[:20]


@register('organization')
class OrganizationLookup(LookupChannel):
    model = models.Organization

    def get_query(self, q, request):
        return self.model.objects.filter(
            name__icontains=q).order_by('name')[:20]


@register('person')
class PersonLookup(LookupChannel):
    model = models.Person

    def get_query(self, q, request):
        query = Q()
        for term in q.strip().split(' '):
            query &= (
                Q(name__icontains=term) | Q(surname__icontains=term) |
                Q(email__icontains=term) | Q(attached_to__name__icontains=term)|
                Q(raw_name__icontains=term)
            )
        return self.model.objects.filter(query).order_by('name')[:20]


@register('author')
class AuthorLookup(LookupChannel):
    model = models.Author

    def get_query(self, q, request):
        query = Q()
        for term in q.strip().split(' '):
            query &= (
                Q(person__name__icontains=term) |
                Q(person__surname__icontains=term) |
                Q(person__raw_name__icontains=term) |
                Q(person__attached_to__name__icontains=term) |
                Q(author_type__label__icontains=term)
            )
        return self.model.objects.filter(query).order_by('person__name')[:20]


@register('ishtaruser')
class UserLookup(LookupChannel):
    model = models.IshtarUser

    def get_query(self, q, request):
        query = Q()
        for term in q.strip().split(' '):
            query &= (
                Q(user_ptr__username__icontains=term) |
                Q(person__name__icontains=term) |
                Q(person__surname__icontains=term) |
                Q(person__email__icontains=term) |
                Q(person__attached_to__name__icontains=term)|
                Q(person__raw_name__icontains=term)
            )
        return self.model.objects.filter(query).order_by('person__name')[:20]

    def format_item_display(self, item):
        return u"<span class='ajax-label'>%s</span>" % str(item.person)


@register('area')
class AreaLookup(LookupChannel):
    model = models.Area

    def get_query(self, q, request):
        query = Q(label__icontains=q)
        return self.model.objects.filter(query).order_by('label')[:20]


@register('document')
class DocumentLookup(LookupChannel):
    model = models.Document

    def get_query(self, q, request):
        query = Q(title__icontains=q) | Q(reference__icontains=q) | Q(
            internal_reference__icontains=q
        )
        return self.model.objects.filter(query).order_by('title')[:20]
