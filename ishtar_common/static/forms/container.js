$(document).ready(function(){
    var location_changed = false;

    $("#id_select_responsible").on("autocompleteselect", function(event, ui) {
        if (location_changed) return;
        $("#id_select_location").val(ui.item.label);
        $("#id_location").val(ui.item.id);
    });

    $("#id_select_location").on("autocompleteselect", function(event, ui){
        location_changed = true;
    });
});
