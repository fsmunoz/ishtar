#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2019  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

import os.path
import sys

from ishtar_common.utils import get_used_media, try_fix_file

from django.core.management.base import BaseCommand


class Command(BaseCommand):
    args = ''
    help = 'Find missing files in media.'

    def add_arguments(self, parser):
        parser.add_argument(
            '--exclude', nargs='?', dest='exclude', default=None,
            help="Field to exclude separated with \",\". Example: "
            "ishtar_common.Import.imported_file,"
            "ishtar_common.Import.imported_images"
        )
        parser.add_argument(
            '--limit', nargs='?', dest='limit', default=None,
            help="Field to limit to separated with \",\"."
        )
        parser.add_argument(
            '--try-fix', dest='try-fix', action='store_true',
            default=False,
            help='Try to find file with similar names and copy the file.')
        parser.add_argument(
            '--find-fix', dest='find-fix', action='store_true',
            default=False,
            help='Try to find file with similar names and print them.')

    def handle(self, *args, **options):
        exclude = options['exclude'].split(',') if options['exclude'] else []
        limit = options['limit'].split(',') if options['limit'] else []
        try_fix = options['try-fix']
        find_fix = options['find-fix']
        if try_fix and find_fix:
            self.stdout.write("try-fix and find-fix options are not "
                              "compatible.\n")
            return

        missing = []
        for media in get_used_media(exclude=exclude, limit=limit):
            if not os.path.isfile(media):
                missing.append(media)

        if try_fix or find_fix:
            if find_fix:
                self.stdout.write("* potential similar file:\n")
            else:
                self.stdout.write("* fixes files:\n")
            for item in missing[:]:
                source_file = try_fix_file(item, make_copy=try_fix)
                if source_file:
                    missing.pop(missing.index(item))
                    sys.stdout.write(
                        "{} <- {}\n".format(item.encode('utf-8'),
                                            source_file.encode('utf-8')))

        if missing:
            if find_fix or try_fix:
                self.stdout.write("* missing file with no similar file "
                                  "found:\n")
            for item in missing:
                sys.stdout.write(item.encode('utf-8') + "\n")
        else:
            self.stdout.write("No missing files.\n")
