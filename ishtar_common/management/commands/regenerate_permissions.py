#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2013-2017 Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.


from django.core.management.base import BaseCommand

from django.contrib.auth.management import create_permissions
from django.apps import apps


class Command(BaseCommand):
    args = ''
    help = 'Regenerate permissions'

    def handle(self, *args, **options):
        for app in ['ishtar_common', 'archaeological_operations',
                    'archaeological_context_records',
                    'archaeological_finds', 'archaeological_warehouse']:
            create_permissions(apps.get_app_config(app))
