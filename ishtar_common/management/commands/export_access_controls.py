#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2017  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

import json
import sys

from django.core.management.base import BaseCommand

from django.contrib.auth.models import Permission, Group
from django.contrib.contenttypes.models import ContentType
from ishtar_common.models import PersonType


class Command(BaseCommand):
    args = ''
    help = 'Export access controls'

    def handle(self, *args, **options):
        result = {
            'content_types': [],
            'permissions': [],
            'groups': [],
            'group_perms': [],
            'person_types': [],
            'person_type_groups': [],
        }

        dct_ct = {}
        for content_type in ContentType.objects.all():
            value = {'app_label': content_type.app_label,
                     'model': content_type.model,
                     'name': content_type.name
                     }
            result['content_types'].append(value)
            dct_ct[content_type.pk] = (content_type.app_label,
                                       content_type.model)

        dct_perm = {}
        for perm in Permission.objects.all():
            value = {'content_type': dct_ct[perm.content_type_id],
                     'codename': perm.codename,
                     'name': perm.name}
            result['permissions'].append(value)
            dct_perm[perm.pk] = perm.codename

        dct_groups = {}
        for grp in Group.objects.all():
            value = {
                "name": grp.name
            }
            result['groups'].append(value)
            dct_groups[grp.pk] = grp
            for perm in grp.permissions.all():
                result['group_perms'].append(
                    {'group': grp.name, 'permission': dct_perm[perm.pk]})

        dct_pts = {}
        for pt in PersonType.objects.all():
            value = {
                "label": pt.label,
                "txt_idx": pt.txt_idx,
                "comment": pt.comment,
                "available": pt.available
            }
            result["person_types"].append(value)
            dct_pts[pt.pk] = pt.txt_idx
            for ptgp in pt.groups.all():
                result['person_type_groups'].append(
                    {"person_type": pt.txt_idx, "group": ptgp.name}
                )

        data = json.dumps(result)
        sys.stdout.write(data)