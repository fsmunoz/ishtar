#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

from django.core.management.base import BaseCommand
import django.apps


class Command(BaseCommand):
    help = "./manage.py update_search_vectors\n\n"\
           "Update full texte search vectors."

    def add_arguments(self, parser):
        parser.add_argument(
            '--model', type=str, default='', dest='model',
            help='Specific model to update')
        parser.add_argument(
            '--quiet', dest='quiet', action='store_true',
            help='Quiet output')

    def handle(self, *args, **options):
        quiet = options['quiet']
        nb_total = 0
        for model in django.apps.apps.get_models():
            if options['model'] and model.__name__ != options['model']:
                continue
            if hasattr(model, "update_search_vector") and \
                    (getattr(model, "BASE_SEARCH_VECTORS", None) or
                     getattr(model, "INT_SEARCH_VECTORS", None) or
                     getattr(model, "M2M_SEARCH_VECTORS", None) or
                     getattr(model, "PARENT_SEARCH_VECTORS", None)):
                if not quiet:
                    self.stdout.write("\n* update {}".format(model))
                total = model.objects.count()
                idx = 0
                for idx, item in enumerate(model.objects.all()):
                    if not quiet:
                        sys.stdout.write("\r{}/{} ".format(idx + 1, total))
                        sys.stdout.flush()
                    try:
                        item.update_search_vector()
                    except:
                        pass
                nb_total += idx
        if not quiet:
            self.stdout.write("\n")
        self.stdout.write("{} items updated\n".format(nb_total))
