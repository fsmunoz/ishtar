#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2017  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

import json

from django.core.management.base import BaseCommand

from django.contrib.auth.models import Permission, Group
from django.contrib.contenttypes.models import ContentType

from ishtar_common.models import PersonType


class Command(BaseCommand):
    args = '<access control file>'
    help = 'Import access controls'

    def handle(self, *args, **options):
        f = open(args[0])
        data = json.loads(f.read())

        res = {'content_types': {},
               'permissions': {},
               'groups': {},
               'person_types': {}
        }
        for content_type in data['content_types']:
            q = ContentType.objects.filter(
                app_label=content_type['app_label'],
                model=content_type['model'])
            if not q.count():
                ct = ContentType.objects.create(
                    app_label=content_type['app_label'],
                    model=content_type['model'],
                    name=content_type['name']
                )
                print("created", ct)
            else:
                ct = q.all()[0]
            res['content_types'][(ct.app_label,
                                  ct.model)] = ct

        for perm in data['permissions']:
            app_label, model = perm['content_type']
            q = Permission.objects.filter(
                codename=perm['codename'],
                content_type__app_label=app_label,
                content_type__model= model
            )
            if q.count():
                p = q.all()[0]
            else:
                p = Permission.objects.create(
                    name=perm['name'], codename=perm['codename'],
                    content_type=res['content_types'][(app_label, model)])
                print("created", p)
            res['permissions'][perm['codename']] = p

        for gp in data['groups']:
            q = Group.objects.filter(name=gp['name'])
            if q.count():
                g = q.all()[0]
            else:
                g = Group.objects.create(name=gp['name'])
                print("created", g)
            res['groups'][gp['name']] = g
            g.permissions.clear()

        for gperm in data['group_perms']:
            g = res['groups'][gperm['group']]
            g.permissions.add(res['permissions'][gperm['permission']])

        for pt in data['person_types']:
            q = PersonType.objects.filter(txt_idx=pt['txt_idx'])
            if q.count():
                pt = q.all()[0]
            else:
                pt = PersonType.objects.create(
                    label=pt['label'],
                    txt_idx=pt['txt_idx'],
                    comment=pt['comment'],
                    available=pt['available']
                )
                print("created", pt)
            res['person_types'][pt.txt_idx] = pt
            pt.groups.clear()

        for ptgp in data['person_type_groups']:
            pt = res['person_types'][ptgp['person_type']]
            gp = res['groups'][ptgp['group']]
            pt.groups.add(gp)
