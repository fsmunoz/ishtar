#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

from django.core.management.base import BaseCommand

from archaeological_operations.models import Parcel
from archaeological_context_records.models import ContextRecord
from archaeological_finds.models import BaseFind, Find


class Command(BaseCommand):
    help = "./manage.py ishtar_execute_admin_tasks\n\n"\
           "Launch pending administration tasks."

    def handle(self, *args, **options):
        for model in [Parcel, ContextRecord, BaseFind, Find]:
            updated = 0
            print("* {}".format(model))
            total = model.objects.count()
            for idx, item in enumerate(model.objects.all()):
                sys.stdout.write("\r{}/{} ".format(idx, total))
                sys.stdout.flush()
                updated += 1 if item.update_external_id(save=True) else 0
            print("\rupdated: {}     ".format(updated))
