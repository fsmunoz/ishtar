from datetime import datetime

from django.core.management.commands.makemigrations import Command
from django.db import models


original_deconstruct = models.Field.deconstruct


def new_deconstruct(self):
    """
    virtualtime can induce bad signature for "now" function replace it
    explicitly
    """
    name, path, args, kwargs = original_deconstruct(self)
    if 'default' in kwargs and callable(kwargs['default']) and \
            kwargs['default'].__name__ == 'now':
        kwargs['default'] = datetime.now
    return name, path, args, kwargs


models.Field.deconstruct = new_deconstruct

