#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError

from ishtar_common import models


class Command(BaseCommand):
    help = "./manage.py ishtar_execute_admin_tasks\n\n"\
           "Launch pending administration tasks."

    def handle(self, *args, **options):
        for task in models.AdministrationTask.objects.filter(state='S').all():
            task.execute()
