#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2017  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

import sys

from django.core.management.base import BaseCommand

from archaeological_files.models import File
from archaeological_operations.models import Parcel
from archaeological_context_records.models import ContextRecord
from archaeological_finds.models import BaseFind, Find
from archaeological_warehouse.models import Warehouse, Container


class Command(BaseCommand):
    args = ''
    help = 'Regenerate external ids'

    def handle(self, *args, **options):
        for model in (File, Parcel, ContextRecord, BaseFind, Find, Warehouse,
                      Container):
            self.stdout.write('{} regenerate.\n'.format(model))
            for obj in model.objects.all():
                obj.skip_history_when_saving = True
                obj.save()
