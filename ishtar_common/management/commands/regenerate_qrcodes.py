#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2019 Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

import sys
import tempfile
import shutil

from django.core.management.base import BaseCommand
from django.core.exceptions import FieldDoesNotExist

from django.apps import apps


APPS = ['ishtar_common', 'archaeological_operations',
        'archaeological_context_records', 'archaeological_finds',
        'archaeological_warehouse']


class Command(BaseCommand):
    args = ''
    help = 'Regenerate QR codes'

    def add_arguments(self, parser):
        parser.add_argument('app_name', nargs='?', default=None,
                            choices=APPS)
        parser.add_argument('model_name', nargs='?', default=None)
        parser.add_argument(
            '--no-https', dest='no-https', action='store_true',
            default=False,
            help='Use this parameter if https is not available.')

    def handle(self, *args, **options):
        limit = options['app_name']
        model_name = options['model_name']
        secure = not options['no-https']
        if model_name:
            model_name = model_name.lower()
        for app in APPS:
            if limit and app != limit:
                continue
            print(u"* app: {}".format(app))
            for model in apps.get_app_config(app).get_models():
                if model_name and model.__name__.lower() != model_name:
                    continue
                if model.__name__.startswith('Historical'):
                    continue
                try:
                    model._meta.get_field('qrcode')
                except FieldDoesNotExist:
                    continue
                msg = u"-> processing {}: ".format(model._meta.verbose_name)
                ln = model.objects.count()
                tmpdir = tempfile.mkdtemp("-qrcode")
                for idx, object in enumerate(model.objects.all()):
                    object.skip_history_when_saving = True
                    object._no_move = True
                    cmsg = u"\r{} {}/{}".format(msg, idx + 1, ln)
                    sys.stdout.write(cmsg)
                    sys.stdout.flush()
                    object.generate_qrcode(secure=secure, tmpdir=tmpdir)
                shutil.rmtree(tmpdir)
                sys.stdout.write("\n")
