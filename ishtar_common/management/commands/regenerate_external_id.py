#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2013-2018 Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

import sys

from django.core.management.base import BaseCommand, CommandError

from django.apps import apps


class Command(BaseCommand):
    help = 'Regenerate external id.'

    def add_arguments(self, parser):
        parser.add_argument('model', nargs='+')

    def handle(self, *args, **options):
        for app_model_name in options['model']:
            try:
                capp, model_name = app_model_name.split(".")
            except ValueError:
                raise CommandError(
                    "Model argument must be \"app_name.model_name\". Example: "
                    "archaeological_operations.Operation"
                )
            ok = False
            for app in ['ishtar_common', 'archaeological_operations',
                        'archaeological_context_records',
                        'archaeological_finds', 'archaeological_warehouse']:
                if capp != app:
                    continue
                for model in apps.get_app_config(app).get_models():
                    if model.__name__ != model_name:
                        continue
                    if not bool(
                            [k for k in dir(model) if k == "external_id"]):
                        continue
                    msg = u"* processing {} - {}:".format(
                        app, model._meta.verbose_name)
                    ln = model.objects.count()
                    for idx, object in enumerate(model.objects.all()):
                        object.skip_history_when_saving = True
                        object.external_id = None
                        object._no_move = True
                        cmsg = u"\r{} {}/{}".format(msg, idx + 1, ln)
                        sys.stdout.write(cmsg)
                        sys.stdout.flush()
                        object.save()
                    sys.stdout.write("\n")
                    ok = True
                if not ok:
                    raise CommandError(
                        "{} not found or have no external ID".format(
                            app_model_name))
