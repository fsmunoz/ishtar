#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2013-2018 Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

import sys

from django.core.management.base import BaseCommand

from django.apps import apps


APPS = ['ishtar_common', 'archaeological_operations',
        'archaeological_context_records', 'archaeological_finds',
        'archaeological_warehouse']


class Command(BaseCommand):
    args = ''
    help = 'Regenerate cached labels and search vectors'

    def add_arguments(self, parser):
        parser.add_argument('app_name', nargs='?', default=None,
                            choices=APPS)
        parser.add_argument('model_name', nargs='?', default=None)
        parser.add_argument(
            '--quiet', dest='quiet', action='store_true',
            help='Quiet output')

    def handle(self, *args, **options):
        quiet = options['quiet']
        limit = options['app_name']
        model_name = options['model_name']
        if model_name:
            model_name = model_name.lower()
        for app in APPS:
            if limit and app != limit:
                continue
            if not quiet:
                print(u"* app: {}".format(app))
            for model in apps.get_app_config(app).get_models():
                if model_name and model.__name__.lower() != model_name:
                    continue
                if model.__name__.startswith('Historical'):
                    continue
                if not bool(
                        [k for k in dir(model)
                         if k.startswith('_generate_') or
                            k == "search_vector"]):
                    continue
                msg = u"-> processing {}: ".format(model._meta.verbose_name)
                ln = model.objects.count()
                for idx, obj_id in enumerate(model.objects.values('pk').all()):
                    obj = model.objects.get(pk=obj_id['pk'])
                    obj.skip_history_when_saving = True
                    obj._no_move = True
                    cmsg = u"\r{} {}/{}".format(msg, idx + 1, ln)
                    if not quiet:
                        sys.stdout.write(cmsg)
                        sys.stdout.flush()
                    obj.save()
                if not quiet:
                    sys.stdout.write("\n")
