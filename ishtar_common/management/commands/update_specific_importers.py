#!/usr/bin/env python
# -*- coding: utf-8 -*-

from optparse import make_option

from django.core.management.base import BaseCommand

IMPORTERS = []

from archaeological_files.data_importer import FileImporterSraPdL
IMPORTERS.append(FileImporterSraPdL)


class Command(BaseCommand):
    help = "Update each specific importer"
    option_list = list(BaseCommand.option_list) + [
        make_option(
            '--force',
            action='store_true',
            dest='force',
            default=False,
            help='Force the deletion of existing importers. '
                 'ATTENTION: all associated imports and all associated items '
                 'will be deleted.')]

    def handle(self, *args, **options):
        for importer in IMPORTERS:
            response = importer()._create_models(force=options['force'])
            if response:
                self.stdout.write("%s configured\n" % importer.__name__)
                self.stdout.flush()
