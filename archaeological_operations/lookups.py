from ajax_select import register

from ishtar_common.lookups import LookupChannel, TypeLookupChannel

from django.db.models import Q
from django.utils.encoding import force_text
from django.utils.html import escape

from archaeological_operations.models import Operation, ArchaeologicalSite, \
    Parcel, CulturalAttributionType


@register('operation')
class OperationLookup(LookupChannel):
    model = Operation

    def get_query(self, q, request):
        query = Q()
        for term in q.strip().split(' '):
            subquery = (
                Q(cached_label__icontains=term)
            )
            query &= subquery
        return self.model.objects.filter(query).order_by('cached_label')[:20]

    def format_item_display(self, item):
        return u"<span class='ajax-label'>%s</span>" % item.cached_label


@register('archaeological_site')
class ArchaeologicalSiteLookup(LookupChannel):
    model = ArchaeologicalSite

    def get_query(self, q, request):
        query = Q()
        for term in q.strip().split(' '):
            subquery = (
                Q(reference__icontains=term) |
                Q(name__icontains=term)
            )
            query &= subquery
        return self.model.objects.filter(query).order_by('reference',
                                                         'name')[:20]

    def format_item_display(self, item):
        return "<span class='ajax-label'>%s</span>".format(item)


@register('parcel')
class ParcelLookup(LookupChannel):
    model = Parcel

    def get_query(self, q, request):
        query = Q()
        for term in q.strip().split(' '):
            subquery = (
                Q(associated_file__cached_label__icontains=term) |
                Q(operation__cached_label__icontains=term) |
                Q(section__icontains=term) |
                Q(parcel_number__icontains=term) |
                Q(town__name__icontains=term)
            )
            try:
                subquery |= Q(year=int(term))
            except ValueError:
                pass
            query &= subquery
        return self.model.objects.filter(
            query).order_by('-associated_file__cached_label',
                            '-operation__cached_label',
                            'section',
                            'parcel_number')[:20]

    def format_match(self, obj):
        return escape(force_text(obj.long_label()))

    def format_item_display(self, item):
        return u"<span class='ajax-label'>%s</span>" % item.long_label()


@register("cultural_attribution_type")
class CulturalAttributionTypeLookup(TypeLookupChannel):
    model = CulturalAttributionType
