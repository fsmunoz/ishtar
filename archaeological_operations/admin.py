#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2012-2014 Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from ajax_select import make_ajax_form
from ajax_select.fields import AutoCompleteSelectField, \
    AutoCompleteSelectMultipleField

from django import forms
from django.conf import settings
from django.contrib import admin
from django.contrib.gis.forms import PointField, MultiPolygonField, OSMWidget
from ishtar_common.utils import ugettext_lazy as _

from ishtar_common.apps import admin_site
from ishtar_common.admin import HistorizedObjectAdmin, GeneralTypeAdmin, \
    HierarchicalTypeAdmin

from . import models


class AdministrativeActAdmin(HistorizedObjectAdmin):
    list_display = ('year', 'index', 'operation', 'associated_file',
                    'act_type')
    list_filter = ('act_type',)
    search_fields = ('year', 'index')
    readonly_fields = HistorizedObjectAdmin.readonly_fields + [
        'in_charge', 'operator', 'scientist', 'signatory', 'associated_file',
        'departments_label', 'towns_label']
    model = models.AdministrativeAct
    form = make_ajax_form(
        models.AdministrativeAct, {'operation': 'operation'}
    )


admin_site.register(models.AdministrativeAct, AdministrativeActAdmin)


class PeriodAdmin(GeneralTypeAdmin):
    list_display = ('label', 'start_date', 'end_date', 'parent', 'available',
                    'order')
    list_filter = ('parent',)
    model = models.Period


admin_site.register(models.Period, PeriodAdmin)


class OperationInline(admin.TabularInline):
    model = models.Operation.archaeological_sites.through
    form = make_ajax_form(
        model, {'operation': 'operation'})
    extra = 1


class ArchaeologicalSiteAdmin(HistorizedObjectAdmin):
    list_display = ('name', 'reference')
    search_fields = ('name', 'reference')
    model = models.ArchaeologicalSite
    inlines = [OperationInline]


admin_site.register(models.ArchaeologicalSite, ArchaeologicalSiteAdmin)


class ArchaeologicalSiteInline(admin.TabularInline):
    model = models.Operation.archaeological_sites.through
    form = make_ajax_form(
        model, {'archaeologicalsite': 'archaeological_site'})
    extra = 1


class AdminOperationForm(forms.ModelForm):
    class Meta:
        model = models.Operation
        exclude = []
    point = PointField(label=_(u"Point"), required=False,
                       widget=OSMWidget)
    multi_polygon = MultiPolygonField(label=_(u"Multi polygon"), required=False,
                                      widget=OSMWidget)
    in_charge = AutoCompleteSelectField('person', required=False)
    scientist = AutoCompleteSelectField('person', required=False)
    associated_file = AutoCompleteSelectField('file', required=False)
    operator = AutoCompleteSelectField('organization', required=False)
    collaborators = AutoCompleteSelectMultipleField('person', required=False)
    towns = AutoCompleteSelectMultipleField('town', required=False)
    archaeological_sites = AutoCompleteSelectMultipleField(
        'archaeological_site', required=False)
    if settings.COUNTRY == 'fr':
        cira_rapporteur = AutoCompleteSelectField('person', required=False)


class OperationAdmin(HistorizedObjectAdmin):
    list_display = ['year', 'operation_code',
                    'operation_type', 'common_name']
    list_filter = ("operation_type", "year",)
    search_fields = ['towns__name', 'cached_label']
    if settings.COUNTRY == 'fr':
        list_display.insert(2, 'code_patriarche')
        search_fields += ['code_patriarche']
    model = models.Operation
    readonly_fields = HistorizedObjectAdmin.readonly_fields + [
        'cached_label']
    form = AdminOperationForm
    inlines = [ArchaeologicalSiteInline]


admin_site.register(models.Operation, OperationAdmin)


class ParcelAdmin(HistorizedObjectAdmin):
    list_display = ['section', 'parcel_number', 'operation', 'associated_file']
    search_fields = ('operation__cached_label',
                     'associated_file__cached_label',
                     'year', 'section', 'parcel_number')
    model = models.Parcel
    form = make_ajax_form(
        model, {'associated_file': 'file',
                'operation': 'operation',
                'town': 'town'}
    )
    readonly_fields = HistorizedObjectAdmin.readonly_fields + [
        'history_date'
    ]


admin_site.register(models.Parcel, ParcelAdmin)


class RecordRelationsAdmin(admin.ModelAdmin):
    list_display = ('left_record', 'relation_type', 'right_record')
    list_filter = ('relation_type',)
    model = models.RecordRelations
    search_fields = ['left_record__cached_label',
                     'right_record__cached_label']
    form = make_ajax_form(
        model, {'left_record': 'operation',
                'right_record': 'operation'})


admin_site.register(models.RecordRelations, RecordRelationsAdmin)


class RelationTypeAdmin(GeneralTypeAdmin):
    list_display = ('label', 'txt_idx', 'tiny_label', 'available',
                    'symmetrical', 'logical_relation', 'inverse_relation',
                    'order', 'comment')


admin_site.register(models.RelationType, RelationTypeAdmin)


class RecordQualityTypeAdmin(GeneralTypeAdmin):
    list_display = ['label', 'txt_idx', 'available', 'order', 'comment']


admin_site.register(models.RecordQualityType, RecordQualityTypeAdmin)


class ActTypeAdmin(GeneralTypeAdmin):
    list_filter = ('intented_to',)
    list_display = ['label', 'txt_idx', 'available', 'intented_to']


admin_site.register(models.ActType, ActTypeAdmin)


class ReportStateAdmin(GeneralTypeAdmin):
    list_display = ['label', 'txt_idx', 'available', 'order', 'comment']


admin_site.register(models.ReportState, ReportStateAdmin)


class ParcelOwnerAdmin(HistorizedObjectAdmin):
    list_display = ['parcel', 'operation', 'associated_file',
                    'owner']
    search_fields = ('parcel__operation__cached_label',
                     'parcel__associated_file__cached_label',
                     'owner__name', 'owner__surname', 'parcel__section',
                     'parcel__parcel_number')
    model = models.ParcelOwner
    form = make_ajax_form(
        model, {'owner': 'person',
                'parcel': 'parcel'}
    )
    readonly_fields = HistorizedObjectAdmin.readonly_fields + [
        'history_date'
    ]


admin_site.register(models.ParcelOwner, ParcelOwnerAdmin)


class CulturalAttributionTypeAdmin(HierarchicalTypeAdmin):
    list_display = HierarchicalTypeAdmin.list_display
    search_fields = ('label', 'parent__label', )
    model = models.CulturalAttributionType
    form = make_ajax_form(model, {
        'parent': 'cultural_attribution_type',
    })


admin_site.register(models.CulturalAttributionType,
                    CulturalAttributionTypeAdmin)


general_models = [models.RemainType]
for model in general_models:
    admin_site.register(model, GeneralTypeAdmin)
