# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2019-02-06 15:22
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('archaeological_operations', '0047_auto_20190206_1442'),
    ]

    operations = [
        migrations.AlterField(
            model_name='operation',
            name='associated_file',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='operations', to='archaeological_files.File', verbose_name='Dossier'),
        ),
        migrations.AlterField(
            model_name='operation',
            name='record_quality_type',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='archaeological_operations.RecordQualityType', verbose_name="Qualit\xe9 d'enregistrement"),
        ),
        migrations.AlterField(
            model_name='operation',
            name='report_processing',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='archaeological_operations.ReportState', verbose_name='Traitement du rapport'),
        ),
        migrations.AlterField(
            model_name='parcel',
            name='associated_file',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='parcels', to='archaeological_files.File', verbose_name='Dossier'),
        ),
        migrations.AlterField(
            model_name='parcel',
            name='operation',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='parcels', to='archaeological_operations.Operation', verbose_name='Op\xe9ration'),
        ),
        migrations.AlterField(
            model_name='period',
            name='parent',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='archaeological_operations.Period', verbose_name='P\xe9riode parente'),
        ),
    ]
