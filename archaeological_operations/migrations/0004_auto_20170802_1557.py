# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('archaeological_operations', '0003_views'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='administrativeact',
            options={'ordering': ('year', 'signature_date', 'index', 'act_type'), 'verbose_name': 'Administrative act', 'verbose_name_plural': 'Administrative acts', 'permissions': (('view_administrativeact', 'Can view all Administrative acts'), ('view_own_administrativeact', 'Can view own Administrative act'), ('add_own_administrativeact', 'Can add own Administrative act'), ('change_own_administrativeact', 'Can change own Administrative act'), ('delete_own_administrativeact', 'Can delete own Administrative act'))},
        ),
        migrations.AlterModelOptions(
            name='archaeologicalsite',
            options={'verbose_name': 'Archaeological site', 'verbose_name_plural': 'Archaeological sites', 'permissions': (('view_archaeologicalsite', 'Can view all Archaeological sites'), ('view_own_archaeologicalsite', 'Can view own Archaeological site'), ('add_own_archaeologicalsite', 'Can add own Archaeological site'), ('change_own_archaeologicalsite', 'Can change own Archaeological site'), ('delete_own_archaeologicalsite', 'Can delete own Archaeological site'))},
        ),
        migrations.AlterModelOptions(
            name='operation',
            options={'ordering': ('cached_label',), 'verbose_name': 'Operation', 'verbose_name_plural': 'Operations', 'permissions': (('view_operation', 'Can view all Operations'), ('view_own_operation', 'Can view own Operation'), ('add_own_operation', 'Can add own Operation'), ('change_own_operation', 'Can change own Operation'), ('delete_own_operation', 'Can delete own Operation'), ('close_operation', 'Can close Operation'))},
        ),
        migrations.AlterField(
            model_name='operation',
            name='code_patriarche',
            field=models.TextField(unique=True, null=True, verbose_name='Code PATRIARCHE', blank=True),
        ),
    ]
