# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2019-02-06 14:42
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('archaeological_operations', '0046_migrate_main_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='archaeologicalsite',
            name='main_image',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='main_image_sites', to='ishtar_common.Document', verbose_name='Main image'),
        ),
        migrations.AlterField(
            model_name='operation',
            name='main_image',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='main_image_operations', to='ishtar_common.Document', verbose_name='Main image'),
        ),
    ]
