# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('archaeological_finds', '0002_auto_20170414_2123'),
        ('archaeological_operations', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('ishtar_common', '0001_initial'),
        ('archaeological_files', '0002_auto_20170414_2123'),
    ]

    operations = [
        migrations.AddField(
            model_name='parcelowner',
            name='imports',
            field=models.ManyToManyField(related_name='imported_archaeological_operations_parcelowner', to='ishtar_common.Import', blank=True),
        ),
        migrations.AddField(
            model_name='parcelowner',
            name='owner',
            field=models.ForeignKey(related_name='parcel_owner', verbose_name='Owner', to='ishtar_common.Person'),
        ),
        migrations.AddField(
            model_name='parcelowner',
            name='parcel',
            field=models.ForeignKey(related_name='owners', verbose_name='Parcel', to='archaeological_operations.Parcel'),
        ),
        migrations.AddField(
            model_name='parcel',
            name='associated_file',
            field=models.ForeignKey(related_name='parcels', verbose_name='File', blank=True, to='archaeological_files.File', null=True),
        ),
        migrations.AddField(
            model_name='parcel',
            name='history_creator',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Creator', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='parcel',
            name='history_modifier',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Last editor', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='parcel',
            name='imports',
            field=models.ManyToManyField(related_name='imported_archaeological_operations_parcel', to='ishtar_common.Import', blank=True),
        ),
        migrations.AddField(
            model_name='parcel',
            name='operation',
            field=models.ForeignKey(related_name='parcels', verbose_name='Operation', blank=True, to='archaeological_operations.Operation', null=True),
        ),
        migrations.AddField(
            model_name='parcel',
            name='town',
            field=models.ForeignKey(related_name='parcels', verbose_name='Town', to='ishtar_common.Town'),
        ),
        migrations.AddField(
            model_name='operationsource',
            name='authors',
            field=models.ManyToManyField(related_name='operationsource_related', verbose_name='Authors', to='ishtar_common.Author'),
        ),
        migrations.AddField(
            model_name='operationsource',
            name='format_type',
            field=models.ForeignKey(verbose_name='Format', blank=True, to='ishtar_common.Format', null=True),
        ),
        migrations.AddField(
            model_name='operationsource',
            name='operation',
            field=models.ForeignKey(related_name='source', verbose_name='Operation', to='archaeological_operations.Operation'),
        ),
        migrations.AddField(
            model_name='operationsource',
            name='source_type',
            field=models.ForeignKey(verbose_name='Type', to='ishtar_common.SourceType'),
        ),
        migrations.AddField(
            model_name='operationsource',
            name='support_type',
            field=models.ForeignKey(verbose_name='Support', blank=True, to='ishtar_common.SupportType', null=True),
        ),
        migrations.AddField(
            model_name='operation',
            name='archaeological_sites',
            field=models.ManyToManyField(to='archaeological_operations.ArchaeologicalSite', verbose_name='Archaeological sites', blank=True),
        ),
        migrations.AddField(
            model_name='operation',
            name='associated_file',
            field=models.ForeignKey(related_name='operations', verbose_name='File', blank=True, to='archaeological_files.File', null=True),
        ),
        migrations.AddField(
            model_name='operation',
            name='cira_rapporteur',
            field=models.ForeignKey(related_name='cira_rapporteur', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Rapporteur CIRA', blank=True, to='ishtar_common.Person', null=True),
        ),
        migrations.AddField(
            model_name='operation',
            name='collaborators',
            field=models.ManyToManyField(related_name='operation_collaborator', verbose_name='Collaborators', to='ishtar_common.Person', blank=True),
        ),
        migrations.AddField(
            model_name='operation',
            name='history_creator',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Creator', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='operation',
            name='history_modifier',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Last editor', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='operation',
            name='imports',
            field=models.ManyToManyField(related_name='imported_archaeological_operations_operation', to='ishtar_common.Import', blank=True),
        ),
        migrations.AddField(
            model_name='operation',
            name='in_charge',
            field=models.ForeignKey(related_name='operation_responsability', on_delete=django.db.models.deletion.SET_NULL, verbose_name='In charge', blank=True, to='ishtar_common.Person', null=True),
        ),
        migrations.AddField(
            model_name='operation',
            name='operation_type',
            field=models.ForeignKey(related_name='+', verbose_name='Operation type', to='ishtar_common.OperationType'),
        ),
        migrations.AddField(
            model_name='operation',
            name='operator',
            field=models.ForeignKey(related_name='operator', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Operator', blank=True, to='ishtar_common.Organization', null=True),
        ),
        migrations.AddField(
            model_name='operation',
            name='periods',
            field=models.ManyToManyField(to='archaeological_operations.Period', verbose_name='Periods', blank=True),
        ),
        migrations.AddField(
            model_name='operation',
            name='remains',
            field=models.ManyToManyField(to='archaeological_operations.RemainType', verbose_name='Remains', blank=True),
        ),
        migrations.AddField(
            model_name='operation',
            name='report_processing',
            field=models.ForeignKey(verbose_name='Report processing', blank=True, to='archaeological_operations.ReportState', null=True),
        ),
        migrations.AddField(
            model_name='operation',
            name='scientist',
            field=models.ForeignKey(related_name='operation_scientist_responsability', on_delete=django.db.models.deletion.SET_NULL, verbose_name='In charge scientist', blank=True, to='ishtar_common.Person', null=True),
        ),
        migrations.AddField(
            model_name='operation',
            name='towns',
            field=models.ManyToManyField(related_name='operations', verbose_name='Towns', to='ishtar_common.Town'),
        ),
        migrations.AddField(
            model_name='historicaloperation',
            name='associated_file',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='archaeological_files.File', null=True),
        ),
        migrations.AddField(
            model_name='historicaloperation',
            name='cira_rapporteur',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='ishtar_common.Person', null=True),
        ),
        migrations.AddField(
            model_name='historicaloperation',
            name='history_creator',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='historicaloperation',
            name='history_modifier',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='historicaloperation',
            name='history_user',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='historicaloperation',
            name='in_charge',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='ishtar_common.Person', null=True),
        ),
        migrations.AddField(
            model_name='historicaloperation',
            name='operation_type',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='ishtar_common.OperationType', null=True),
        ),
        migrations.AddField(
            model_name='historicaloperation',
            name='operator',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='ishtar_common.Organization', null=True),
        ),
        migrations.AddField(
            model_name='historicaloperation',
            name='report_processing',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='archaeological_operations.ReportState', null=True),
        ),
        migrations.AddField(
            model_name='historicaloperation',
            name='scientist',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='ishtar_common.Person', null=True),
        ),
        migrations.AddField(
            model_name='historicaladministrativeact',
            name='act_type',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='archaeological_operations.ActType', null=True),
        ),
        migrations.AddField(
            model_name='historicaladministrativeact',
            name='associated_file',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='archaeological_files.File', null=True),
        ),
        migrations.AddField(
            model_name='historicaladministrativeact',
            name='history_creator',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='historicaladministrativeact',
            name='history_modifier',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='historicaladministrativeact',
            name='history_user',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='historicaladministrativeact',
            name='in_charge',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='ishtar_common.Person', null=True),
        ),
        migrations.AddField(
            model_name='historicaladministrativeact',
            name='operation',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='archaeological_operations.Operation', null=True),
        ),
        migrations.AddField(
            model_name='historicaladministrativeact',
            name='operator',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='ishtar_common.Organization', null=True),
        ),
        migrations.AddField(
            model_name='historicaladministrativeact',
            name='scientist',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='ishtar_common.Person', null=True),
        ),
        migrations.AddField(
            model_name='historicaladministrativeact',
            name='signatory',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='ishtar_common.Person', null=True),
        ),
        migrations.AddField(
            model_name='historicaladministrativeact',
            name='treatment',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='archaeological_finds.Treatment', null=True),
        ),
        migrations.AddField(
            model_name='historicaladministrativeact',
            name='treatment_file',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='archaeological_finds.TreatmentFile', null=True),
        ),
        migrations.AddField(
            model_name='archaeologicalsite',
            name='history_creator',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Creator', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='archaeologicalsite',
            name='history_modifier',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Last editor', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='archaeologicalsite',
            name='imports',
            field=models.ManyToManyField(related_name='imported_archaeological_operations_archaeologicalsite', to='ishtar_common.Import', blank=True),
        ),
        migrations.AddField(
            model_name='archaeologicalsite',
            name='periods',
            field=models.ManyToManyField(to='archaeological_operations.Period', verbose_name='Periods', blank=True),
        ),
        migrations.AddField(
            model_name='archaeologicalsite',
            name='remains',
            field=models.ManyToManyField(to='archaeological_operations.RemainType', verbose_name='Remains', blank=True),
        ),
        migrations.AddField(
            model_name='administrativeact',
            name='act_type',
            field=models.ForeignKey(verbose_name='Act type', to='archaeological_operations.ActType'),
        ),
        migrations.AddField(
            model_name='administrativeact',
            name='associated_file',
            field=models.ForeignKey(related_name='administrative_act', verbose_name='Archaeological file', blank=True, to='archaeological_files.File', null=True),
        ),
        migrations.AddField(
            model_name='administrativeact',
            name='history_creator',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Creator', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='administrativeact',
            name='history_modifier',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Last editor', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='administrativeact',
            name='imports',
            field=models.ManyToManyField(related_name='imported_archaeological_operations_administrativeact', to='ishtar_common.Import', blank=True),
        ),
        migrations.AddField(
            model_name='administrativeact',
            name='in_charge',
            field=models.ForeignKey(related_name='adminact_operation_in_charge', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Person in charge of the operation', blank=True, to='ishtar_common.Person', null=True),
        ),
        migrations.AddField(
            model_name='administrativeact',
            name='operation',
            field=models.ForeignKey(related_name='administrative_act', verbose_name='Operation', blank=True, to='archaeological_operations.Operation', null=True),
        ),
        migrations.AddField(
            model_name='administrativeact',
            name='operator',
            field=models.ForeignKey(related_name='adminact_operator', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Archaeological preventive operator', blank=True, to='ishtar_common.Organization', null=True),
        ),
        migrations.AddField(
            model_name='administrativeact',
            name='scientist',
            field=models.ForeignKey(related_name='adminact_scientist', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Scientist in charge', blank=True, to='ishtar_common.Person', null=True),
        ),
        migrations.AddField(
            model_name='administrativeact',
            name='signatory',
            field=models.ForeignKey(related_name='signatory', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Signatory', blank=True, to='ishtar_common.Person', null=True),
        ),
        migrations.AddField(
            model_name='administrativeact',
            name='treatment',
            field=models.ForeignKey(related_name='administrative_act', verbose_name='Treatment', blank=True, to='archaeological_finds.Treatment', null=True),
        ),
        migrations.AddField(
            model_name='administrativeact',
            name='treatment_file',
            field=models.ForeignKey(related_name='administrative_act', verbose_name='Treatment request', blank=True, to='archaeological_finds.TreatmentFile', null=True),
        ),
        migrations.AddField(
            model_name='acttype',
            name='associated_template',
            field=models.ManyToManyField(related_name='acttypes', verbose_name='Associated template', to='ishtar_common.DocumentTemplate', blank=True),
        ),
    ]
