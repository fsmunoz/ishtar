# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import re
import django.contrib.gis.db.models.fields
import archaeological_operations.models
import django.db.models.deletion
from django.conf import settings
import ishtar_common.models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='OperationByDepartment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'db_table': 'operation_department',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='ActType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label', models.CharField(max_length=100, verbose_name='Label')),
                ('txt_idx', models.CharField(unique=True, max_length=100, verbose_name='Textual ID', validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+$'), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')])),
                ('comment', models.TextField(null=True, verbose_name='Comment', blank=True)),
                ('available', models.BooleanField(default=True, verbose_name='Available')),
                ('intented_to', models.CharField(max_length=2, verbose_name='Intended to', choices=[(b'F', 'Archaeological file'), (b'O', 'Operation'), (b'TF', 'Treatment request'), (b'T', 'Treatment')])),
                ('code', models.CharField(max_length=10, null=True, verbose_name='Code', blank=True)),
                ('indexed', models.BooleanField(default=False, verbose_name='Indexed')),
            ],
            options={
                'ordering': ('label',),
                'verbose_name': 'Act type',
                'verbose_name_plural': 'Act types',
            },
            bases=(ishtar_common.models.Cached, models.Model),
        ),
        migrations.CreateModel(
            name='AdministrativeAct',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('index', models.IntegerField(null=True, verbose_name='Index', blank=True)),
                ('signature_date', models.DateField(null=True, verbose_name='Signature date', blank=True)),
                ('year', models.IntegerField(null=True, verbose_name='Year', blank=True)),
                ('act_object', models.TextField(max_length=300, null=True, verbose_name='Object', blank=True)),
                ('ref_sra', models.CharField(max_length=15, null=True, verbose_name='R\xe9f\xe9rence SRA', blank=True)),
                ('departments_label', models.TextField(help_text='Cached values get from associated departments', null=True, verbose_name='Departments', blank=True)),
                ('towns_label', models.TextField(help_text='Cached values get from associated towns', null=True, verbose_name='Towns', blank=True)),
            ],
            options={
                'ordering': ('year', 'signature_date', 'index', 'act_type'),
                'verbose_name': 'Administrative act',
                'verbose_name_plural': 'Administrative acts',
                'permissions': (('view_administrativeact', 'Peut voir tous les Actes administratifs'), ('view_own_administrativeact', 'Peut voir son propre Acte administratif'), ('add_own_administrativeact', 'Peut ajouter son propre Acte administratif'), ('change_own_administrativeact', 'Peut modifier son propre Acte administratif'), ('delete_own_administrativeact', 'Peut supprimer son propre Acte administratif')),
            },
            bases=(models.Model, ishtar_common.models.OwnPerms, ishtar_common.models.ValueGetter),
        ),
        migrations.CreateModel(
            name='ArchaeologicalSite',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('reference', models.CharField(unique=True, max_length=20, verbose_name='Reference')),
                ('name', models.CharField(max_length=200, null=True, verbose_name='Name', blank=True)),
            ],
            options={
                'verbose_name': 'Archaeological site',
                'verbose_name_plural': 'Archaeological sites',
                'permissions': (('view_archaeologicalsite', 'Peut voir toutes les Entit\xe9s arch\xe9ologiques'), ('view_own_archaeologicalsite', 'Peut voir ses propres Entit\xe9s arch\xe9ologiques'), ('add_own_archaeologicalsite', 'Peut ajouter ses propres Entit\xe9s arch\xe9ologique'), ('change_own_archaeologicalsite', 'Peut modifier sa propre Entit\xe9 arch\xe9ologique'), ('delete_own_archaeologicalsite', 'Peut supprimer ses propres Entit\xe9s arch\xe9ologiques')),
            },
        ),
        migrations.CreateModel(
            name='HistoricalAdministrativeAct',
            fields=[
                ('id', models.IntegerField(verbose_name='ID', db_index=True, auto_created=True, blank=True)),
                ('index', models.IntegerField(null=True, verbose_name='Index', blank=True)),
                ('signature_date', models.DateField(null=True, verbose_name='Signature date', blank=True)),
                ('year', models.IntegerField(null=True, verbose_name='Year', blank=True)),
                ('act_object', models.TextField(max_length=300, null=True, verbose_name='Object', blank=True)),
                ('ref_sra', models.CharField(max_length=15, null=True, verbose_name='R\xe9f\xe9rence SRA', blank=True)),
                ('departments_label', models.TextField(help_text='Cached values get from associated departments', null=True, verbose_name='Departments', blank=True)),
                ('towns_label', models.TextField(help_text='Cached values get from associated towns', null=True, verbose_name='Towns', blank=True)),
                ('history_id', models.AutoField(serialize=False, primary_key=True)),
                ('history_date', models.DateTimeField()),
                ('history_type', models.CharField(max_length=1, choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')])),
            ],
            options={
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
                'verbose_name': 'historical Administrative act',
            },
        ),
        migrations.CreateModel(
            name='HistoricalOperation',
            fields=[
                ('id', models.IntegerField(verbose_name='ID', db_index=True, auto_created=True, blank=True)),
                ('image', models.TextField(max_length=255, null=True, blank=True)),
                ('thumbnail', models.TextField(max_length=255, null=True, blank=True)),
                ('creation_date', models.DateField(default=datetime.date.today, verbose_name='Creation date')),
                ('end_date', models.DateField(null=True, verbose_name='Closing date', blank=True)),
                ('start_date', models.DateField(null=True, verbose_name='Start date', blank=True)),
                ('excavation_end_date', models.DateField(null=True, verbose_name='Excavation end date', blank=True)),
                ('report_delivery_date', models.DateField(null=True, verbose_name='Report delivery date', blank=True)),
                ('year', models.IntegerField(null=True, verbose_name='Year', blank=True)),
                ('operation_code', models.IntegerField(null=True, verbose_name='Numeric reference', blank=True)),
                ('surface', models.IntegerField(null=True, verbose_name='Surface (m2)', blank=True)),
                ('cost', models.IntegerField(null=True, verbose_name='Cost (euros)', blank=True)),
                ('scheduled_man_days', models.IntegerField(null=True, verbose_name='Scheduled man-days', blank=True)),
                ('optional_man_days', models.IntegerField(null=True, verbose_name='Optional man-days', blank=True)),
                ('effective_man_days', models.IntegerField(null=True, verbose_name='Effective man-days', blank=True)),
                ('old_code', models.CharField(max_length=200, null=True, verbose_name='Old code', blank=True)),
                ('code_patriarche', models.TextField(db_index=True, null=True, verbose_name='Code PATRIARCHE', blank=True)),
                ('fnap_financing', models.FloatField(null=True, verbose_name='Financement FNAP (%)', blank=True)),
                ('fnap_cost', models.IntegerField(null=True, verbose_name='Financement FNAP (\u20ac)', blank=True)),
                ('zoning_prescription', models.NullBooleanField(verbose_name='Prescription on zoning')),
                ('large_area_prescription', models.NullBooleanField(verbose_name='Prescription on large area')),
                ('geoarchaeological_context_prescription', models.NullBooleanField(verbose_name='Prescription on geoarchaeological context')),
                ('negative_result', models.NullBooleanField(verbose_name='R\xe9sultat consid\xe9r\xe9 comme n\xe9gatif')),
                ('cira_date', models.DateField(null=True, verbose_name='Date avis CIRA', blank=True)),
                ('eas_number', models.CharField(max_length=20, null=True, verbose_name="Num\xe9ro de l'EA", blank=True)),
                ('operator_reference', models.CharField(max_length=20, null=True, verbose_name='Operator reference', blank=True)),
                ('common_name', models.TextField(null=True, verbose_name='Generic name', blank=True)),
                ('address', models.TextField(null=True, verbose_name='Address / Locality', blank=True)),
                ('comment', models.TextField(null=True, verbose_name='General comment', blank=True)),
                ('scientific_documentation_comment', models.TextField(null=True, verbose_name='Comment about scientific documentation', blank=True)),
                ('cached_label', models.CharField(max_length=500, null=True, verbose_name='Cached name', blank=True)),
                ('virtual_operation', models.BooleanField(default=False, help_text='If checked, it means that this operation have not been officialy registered.', verbose_name='Virtual operation')),
                ('record_quality', models.CharField(blank=True, max_length=2, null=True, verbose_name='Record quality', choices=[(b'ND', 'Not documented'), (b'A', 'Arbitrary'), (b'R', 'Reliable')])),
                ('abstract', models.TextField(null=True, verbose_name='Abstract', blank=True)),
                ('documentation_deadline', models.DateField(null=True, verbose_name='Deadline for submission of the documentation', blank=True)),
                ('documentation_received', models.NullBooleanField(verbose_name='Documentation received')),
                ('finds_deadline', models.DateField(null=True, verbose_name='Deadline for submission of the finds', blank=True)),
                ('finds_received', models.NullBooleanField(verbose_name='Finds received')),
                ('point', django.contrib.gis.db.models.fields.PointField(srid=4326, null=True, verbose_name='Point', blank=True)),
                ('multi_polygon', django.contrib.gis.db.models.fields.MultiPolygonField(srid=4326, null=True, verbose_name='Multi polygon', blank=True)),
                ('history_id', models.AutoField(serialize=False, primary_key=True)),
                ('history_date', models.DateTimeField()),
                ('history_type', models.CharField(max_length=1, choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')])),
            ],
            options={
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
                'verbose_name': 'historical Operation',
            },
        ),
        migrations.CreateModel(
            name='Operation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(max_length=255, null=True, upload_to=b'upload/', blank=True)),
                ('thumbnail', models.ImageField(max_length=255, null=True, upload_to=b'upload/thumbs/', blank=True)),
                ('creation_date', models.DateField(default=datetime.date.today, verbose_name='Creation date')),
                ('end_date', models.DateField(null=True, verbose_name='Closing date', blank=True)),
                ('start_date', models.DateField(null=True, verbose_name='Start date', blank=True)),
                ('excavation_end_date', models.DateField(null=True, verbose_name='Excavation end date', blank=True)),
                ('report_delivery_date', models.DateField(null=True, verbose_name='Report delivery date', blank=True)),
                ('year', models.IntegerField(null=True, verbose_name='Year', blank=True)),
                ('operation_code', models.IntegerField(null=True, verbose_name='Numeric reference', blank=True)),
                ('surface', models.IntegerField(null=True, verbose_name='Surface (m2)', blank=True)),
                ('cost', models.IntegerField(null=True, verbose_name='Cost (euros)', blank=True)),
                ('scheduled_man_days', models.IntegerField(null=True, verbose_name='Scheduled man-days', blank=True)),
                ('optional_man_days', models.IntegerField(null=True, verbose_name='Optional man-days', blank=True)),
                ('effective_man_days', models.IntegerField(null=True, verbose_name='Effective man-days', blank=True)),
                ('old_code', models.CharField(max_length=200, null=True, verbose_name='Old code', blank=True)),
                ('code_patriarche', models.TextField(db_index=True, null=True, verbose_name='Code PATRIARCHE', blank=True)),
                ('fnap_financing', models.FloatField(null=True, verbose_name='Financement FNAP (%)', blank=True)),
                ('fnap_cost', models.IntegerField(null=True, verbose_name='Financement FNAP (\u20ac)', blank=True)),
                ('zoning_prescription', models.NullBooleanField(verbose_name='Prescription on zoning')),
                ('large_area_prescription', models.NullBooleanField(verbose_name='Prescription on large area')),
                ('geoarchaeological_context_prescription', models.NullBooleanField(verbose_name='Prescription on geoarchaeological context')),
                ('negative_result', models.NullBooleanField(verbose_name='R\xe9sultat consid\xe9r\xe9 comme n\xe9gatif')),
                ('cira_date', models.DateField(null=True, verbose_name='Date avis CIRA', blank=True)),
                ('eas_number', models.CharField(max_length=20, null=True, verbose_name="Num\xe9ro de l'EA", blank=True)),
                ('operator_reference', models.CharField(max_length=20, null=True, verbose_name='Operator reference', blank=True)),
                ('common_name', models.TextField(null=True, verbose_name='Generic name', blank=True)),
                ('address', models.TextField(null=True, verbose_name='Address / Locality', blank=True)),
                ('comment', models.TextField(null=True, verbose_name='General comment', blank=True)),
                ('scientific_documentation_comment', models.TextField(null=True, verbose_name='Comment about scientific documentation', blank=True)),
                ('cached_label', models.CharField(max_length=500, null=True, verbose_name='Cached name', blank=True)),
                ('virtual_operation', models.BooleanField(default=False, help_text='If checked, it means that this operation have not been officialy registered.', verbose_name='Virtual operation')),
                ('record_quality', models.CharField(blank=True, max_length=2, null=True, verbose_name='Record quality', choices=[(b'ND', 'Not documented'), (b'A', 'Arbitrary'), (b'R', 'Reliable')])),
                ('abstract', models.TextField(null=True, verbose_name='Abstract', blank=True)),
                ('documentation_deadline', models.DateField(null=True, verbose_name='Deadline for submission of the documentation', blank=True)),
                ('documentation_received', models.NullBooleanField(verbose_name='Documentation received')),
                ('finds_deadline', models.DateField(null=True, verbose_name='Deadline for submission of the finds', blank=True)),
                ('finds_received', models.NullBooleanField(verbose_name='Finds received')),
                ('point', django.contrib.gis.db.models.fields.PointField(srid=4326, null=True, verbose_name='Point', blank=True)),
                ('multi_polygon', django.contrib.gis.db.models.fields.MultiPolygonField(srid=4326, null=True, verbose_name='Multi polygon', blank=True)),
            ],
            options={
                'ordering': ('cached_label',),
                'verbose_name': 'Operation',
                'verbose_name_plural': 'Operations',
                'permissions': (('view_operation', 'Peut voir toutes les Op\xe9rations'), ('view_own_operation', 'Peut voir sa propre Op\xe9ration'), ('add_own_operation', 'Peut ajouter sa propre Op\xe9ration'), ('change_own_operation', 'Peut modifier sa propre Op\xe9ration'), ('delete_own_operation', 'Peut supprimer sa propre Op\xe9ration'), ('close_operation', 'Peut clore une Op\xe9ration')),
            },
            bases=(archaeological_operations.models.ClosedItem, models.Model, ishtar_common.models.OwnPerms, ishtar_common.models.ValueGetter, ishtar_common.models.ShortMenuItem, ishtar_common.models.DashboardFormItem),
        ),
        migrations.CreateModel(
            name='OperationSource',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(max_length=255, null=True, upload_to=b'upload/', blank=True)),
                ('thumbnail', models.ImageField(max_length=255, null=True, upload_to=b'upload/thumbs/', blank=True)),
                ('title', models.CharField(max_length=300, verbose_name='Title')),
                ('external_id', models.TextField(max_length=300, null=True, verbose_name='External ID', blank=True)),
                ('scale', models.CharField(max_length=30, null=True, verbose_name='Scale', blank=True)),
                ('associated_url', models.URLField(null=True, verbose_name='Numerical ressource (web address)', blank=True)),
                ('receipt_date', models.DateField(null=True, verbose_name='Receipt date', blank=True)),
                ('creation_date', models.DateField(null=True, verbose_name='Creation date', blank=True)),
                ('receipt_date_in_documentation', models.DateField(null=True, verbose_name='Receipt date in documentation', blank=True)),
                ('item_number', models.IntegerField(default=1, verbose_name='Item number')),
                ('reference', models.CharField(max_length=100, null=True, verbose_name='Ref.', blank=True)),
                ('internal_reference', models.CharField(max_length=100, null=True, verbose_name='Internal ref.', blank=True)),
                ('description', models.TextField(null=True, verbose_name='Description', blank=True)),
                ('comment', models.TextField(null=True, verbose_name='Comment', blank=True)),
                ('additional_information', models.TextField(null=True, verbose_name='Additional information', blank=True)),
                ('duplicate', models.BooleanField(default=False, verbose_name='Has a duplicate')),
                ('index', models.IntegerField(null=True, verbose_name='Index', blank=True)),
            ],
            options={
                'verbose_name': 'Operation documentation',
                'verbose_name_plural': 'Operation documentations',
                'permissions': (('view_operationsource', "Peut voir toutes les Documentations d'op\xe9ration"), ('view_own_operationsource', "Peut voir sa propre Documentation d'op\xe9ration"), ('add_own_operationsource', "Peut ajouter sa propre Documentation d'op\xe9ration"), ('change_own_operationsource', "Peut modifier sa propre Documentation d'op\xe9ration"), ('delete_own_operationsource', "Peut supprimer sa propre Documentation d'op\xe9ration")),
            },
            bases=(ishtar_common.models.OwnPerms, models.Model),
        ),
        migrations.CreateModel(
            name='OperationTypeOld',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label', models.CharField(max_length=100, verbose_name='Label')),
                ('txt_idx', models.CharField(unique=True, max_length=100, verbose_name='Textual ID', validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+$'), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')])),
                ('comment', models.TextField(null=True, verbose_name='Comment', blank=True)),
                ('available', models.BooleanField(default=True, verbose_name='Available')),
                ('order', models.IntegerField(default=1, verbose_name='Order')),
                ('preventive', models.BooleanField(default=True, verbose_name='Is preventive')),
            ],
            options={
                'ordering': ['-preventive', 'order', 'label'],
                'verbose_name': 'Operation type old',
                'verbose_name_plural': 'Operation types old',
            },
            bases=(ishtar_common.models.Cached, models.Model),
        ),
        migrations.CreateModel(
            name='Parcel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('history_date', models.DateTimeField(default=datetime.datetime.now)),
                ('year', models.IntegerField(null=True, verbose_name='Year', blank=True)),
                ('section', models.CharField(max_length=4, null=True, verbose_name='Section', blank=True)),
                ('parcel_number', models.CharField(max_length=6, null=True, verbose_name='Parcel number', blank=True)),
                ('public_domain', models.BooleanField(default=False, verbose_name='Public domain')),
                ('external_id', models.CharField(max_length=100, null=True, verbose_name='External ID', blank=True)),
                ('auto_external_id', models.BooleanField(default=False, verbose_name='External ID is set automatically')),
                ('address', models.TextField(null=True, verbose_name='Address - Locality', blank=True)),
            ],
            options={
                'ordering': ('year', 'section', 'parcel_number'),
                'verbose_name': 'Parcel',
                'verbose_name_plural': 'Parcels',
            },
        ),
        migrations.CreateModel(
            name='ParcelOwner',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('history_date', models.DateTimeField(default=datetime.datetime.now)),
                ('start_date', models.DateField(verbose_name='Start date')),
                ('end_date', models.DateField(verbose_name='End date')),
                ('history_creator', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Creator', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('history_modifier', models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Last editor', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'verbose_name': 'Parcel owner',
                'verbose_name_plural': 'Parcel owners',
            },
        ),
        migrations.CreateModel(
            name='Period',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label', models.CharField(max_length=100, verbose_name='Label')),
                ('txt_idx', models.CharField(unique=True, max_length=100, verbose_name='Textual ID', validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+$'), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')])),
                ('comment', models.TextField(null=True, verbose_name='Comment', blank=True)),
                ('available', models.BooleanField(default=True, verbose_name='Available')),
                ('order', models.IntegerField(verbose_name='Order')),
                ('start_date', models.IntegerField(verbose_name='Start date')),
                ('end_date', models.IntegerField(verbose_name='End date')),
                ('parent', models.ForeignKey(verbose_name='Parent period', blank=True, to='archaeological_operations.Period', null=True)),
            ],
            options={
                'ordering': ('order',),
                'verbose_name': 'Type Period',
                'verbose_name_plural': 'Types Period',
            },
            bases=(ishtar_common.models.Cached, models.Model),
        ),
        migrations.CreateModel(
            name='RecordRelations',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('left_record', models.ForeignKey(related_name='right_relations', to='archaeological_operations.Operation')),
            ],
            options={
                'ordering': ('left_record', 'relation_type'),
                'verbose_name': 'Operation record relation',
                'verbose_name_plural': 'Operation record relations',
            },
            bases=(ishtar_common.models.GeneralRecordRelations, models.Model),
        ),
        migrations.CreateModel(
            name='RelationType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label', models.CharField(max_length=100, verbose_name='Label')),
                ('txt_idx', models.CharField(unique=True, max_length=100, verbose_name='Textual ID', validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+$'), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')])),
                ('comment', models.TextField(null=True, verbose_name='Comment', blank=True)),
                ('available', models.BooleanField(default=True, verbose_name='Available')),
                ('order', models.IntegerField(default=1, verbose_name='Order')),
                ('symmetrical', models.BooleanField(verbose_name='Symmetrical')),
                ('tiny_label', models.CharField(max_length=50, null=True, verbose_name='Tiny label', blank=True)),
                ('inverse_relation', models.ForeignKey(verbose_name='Inverse relation', blank=True, to='archaeological_operations.RelationType', null=True)),
            ],
            options={
                'ordering': ('order', 'label'),
                'verbose_name': 'Operation relation type',
                'verbose_name_plural': 'Operation relation types',
            },
            bases=(ishtar_common.models.Cached, models.Model),
        ),
        migrations.CreateModel(
            name='RemainType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label', models.CharField(max_length=100, verbose_name='Label')),
                ('txt_idx', models.CharField(unique=True, max_length=100, verbose_name='Textual ID', validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+$'), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')])),
                ('comment', models.TextField(null=True, verbose_name='Comment', blank=True)),
                ('available', models.BooleanField(default=True, verbose_name='Available')),
            ],
            options={
                'ordering': ('label',),
                'verbose_name': 'Remain type',
                'verbose_name_plural': 'Remain types',
            },
            bases=(ishtar_common.models.Cached, models.Model),
        ),
        migrations.CreateModel(
            name='ReportState',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label', models.CharField(max_length=100, verbose_name='Label')),
                ('txt_idx', models.CharField(unique=True, max_length=100, verbose_name='Textual ID', validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+$'), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')])),
                ('comment', models.TextField(null=True, verbose_name='Comment', blank=True)),
                ('available', models.BooleanField(default=True, verbose_name='Available')),
                ('order', models.IntegerField(verbose_name='Order')),
            ],
            options={
                'ordering': ('order',),
                'verbose_name': 'Type of report state',
                'verbose_name_plural': 'Types of report state',
            },
            bases=(ishtar_common.models.Cached, models.Model),
        ),
        migrations.AddField(
            model_name='recordrelations',
            name='relation_type',
            field=models.ForeignKey(to='archaeological_operations.RelationType'),
        ),
        migrations.AddField(
            model_name='recordrelations',
            name='right_record',
            field=models.ForeignKey(related_name='left_relations', to='archaeological_operations.Operation'),
        ),
    ]
