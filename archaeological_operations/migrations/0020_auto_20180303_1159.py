# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-03-03 11:59
from __future__ import unicode_literals

from django.db import migrations, models
import virtualtime


class Migration(migrations.Migration):

    dependencies = [
        ('archaeological_operations', '0019_auto_20180228_1741'),
    ]

    operations = [
        migrations.AddField(
            model_name='archaeologicalsite',
            name='locality_cadastral',
            field=models.TextField(blank=True, null=True, verbose_name='Cadastral locality'),
        ),
        migrations.AddField(
            model_name='archaeologicalsite',
            name='locality_ngi',
            field=models.TextField(blank=True, null=True, verbose_name='National Geographic Institute locality'),
        ),
        migrations.AddField(
            model_name='archaeologicalsite',
            name='oceanographic_service_localisation',
            field=models.TextField(blank=True, null=True, verbose_name='Oceanographic service localisation'),
        ),
        migrations.AddField(
            model_name='archaeologicalsite',
            name='shipwreck_code',
            field=models.TextField(blank=True, null=True, verbose_name='Shipwreck code'),
        ),
        migrations.AddField(
            model_name='archaeologicalsite',
            name='sinking_date',
            field=models.DateField(blank=True, null=True, verbose_name='Sinking date'),
        ),
        migrations.AlterField(
            model_name='parcel',
            name='history_date',
            field=models.DateTimeField(default=virtualtime.datetime.now),
        ),
        migrations.AlterField(
            model_name='parcelowner',
            name='history_date',
            field=models.DateTimeField(default=virtualtime.datetime.now),
        ),
    ]
