# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('archaeological_operations', '0004_auto_20170802_1557'),
    ]

    operations = [
        migrations.AlterField(
            model_name='operation',
            name='archaeological_sites',
            field=models.ManyToManyField(related_name='operations', verbose_name='Archaeological sites', to='archaeological_operations.ArchaeologicalSite', blank=True),
        ),
    ]
