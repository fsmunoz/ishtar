# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2019-09-03 17:35
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('archaeological_operations', '0059_operation_top_sites'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='archaeologicalsite',
            name='top_operation',
        ),
        migrations.RemoveField(
            model_name='historicalarchaeologicalsite',
            name='top_operation',
        ),
    ]
