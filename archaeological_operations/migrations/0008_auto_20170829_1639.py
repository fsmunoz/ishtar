# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-08-29 16:39
from __future__ import unicode_literals

from django.db import migrations, models
import ishtar_common.models


class Migration(migrations.Migration):

    dependencies = [
        ('archaeological_operations', '0007_auto_20170826_1152'),
    ]

    operations = [
        migrations.AlterField(
            model_name='operation',
            name='image',
            field=models.ImageField(blank=True, max_length=255, null=True, upload_to=ishtar_common.models.get_image_path),
        ),
        migrations.AlterField(
            model_name='operation',
            name='thumbnail',
            field=models.ImageField(blank=True, max_length=255, null=True, upload_to=ishtar_common.models.get_image_path),
        ),
        migrations.AlterField(
            model_name='operationsource',
            name='image',
            field=models.ImageField(blank=True, max_length=255, null=True, upload_to=ishtar_common.models.get_image_path),
        ),
        migrations.AlterField(
            model_name='operationsource',
            name='thumbnail',
            field=models.ImageField(blank=True, max_length=255, null=True, upload_to=ishtar_common.models.get_image_path),
        ),
    ]
