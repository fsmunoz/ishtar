# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-11-28 11:07
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ishtar_common', '0075_auto_20181108_1908'),
        ('archaeological_operations', '0039_auto_20181017_1854'),
    ]

    operations = [
        migrations.AddField(
            model_name='archaeologicalsite',
            name='collaborators',
            field=models.ManyToManyField(blank=True, related_name='site_collaborator', to='ishtar_common.Person', verbose_name='Collaborators'),
        ),
    ]
