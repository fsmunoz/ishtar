# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-02-18 17:08
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ishtar_common', '0029_auto_20180218_1708'),
        ('archaeological_operations', '0016_auto_20180217_2257'),
    ]

    operations = [
        migrations.AddField(
            model_name='archaeologicalsite',
            name='towns',
            field=models.ManyToManyField(blank=True, related_name='sites', to='ishtar_common.Town', verbose_name='Towns'),
        ),
    ]
