# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-03-15 10:00
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('archaeological_operations', '0020_auto_20180303_1159'),
    ]

    operations = [
        migrations.AlterField(
            model_name='operationsource',
            name='associated_url',
            field=models.URLField(blank=True, max_length=1000, null=True, verbose_name='Numerical ressource (web address)'),
        ),
    ]
