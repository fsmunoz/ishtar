# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from archaeological_operations.models import OperationByDepartment


class Migration(migrations.Migration):

    dependencies = [
        ('archaeological_operations', '0002_auto_20170414_2123'),
    ]

    operations = [
        migrations.RunSQL(OperationByDepartment.CREATE_SQL)
    ]
