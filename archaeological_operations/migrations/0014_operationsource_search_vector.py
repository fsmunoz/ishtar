# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-01-19 15:16
from __future__ import unicode_literals

import django.contrib.postgres.search
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('archaeological_operations', '0013_operation_images'),
    ]

    operations = [
        migrations.AddField(
            model_name='operationsource',
            name='search_vector',
            field=django.contrib.postgres.search.SearchVectorField(blank=True, help_text='Auto filled at save', null=True, verbose_name='Search vector'),
        ),
    ]
