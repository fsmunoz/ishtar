"""
Restore find data on destructive import
"""

import sys

from ishtar_common.utils import update_data
from archaeological_finds.models import Find

ln = Find.objects.count()

sys.stdout.write("\n")
for idx, item in enumerate(Find.objects.all()):
    sys.stdout.write("\r* processing {}/{} ({}%)".format(
        idx + 1, ln, (idx + 1)*100.0 / ln))
    sys.stdout.flush()
    data = item.data
    for h in item.history.order_by('-history_modifier_id', '-history_date',
                                   '-history_id').all():
        data = update_data(data, h.data)
    item.data = data
    item.skip_history_when_saving = True
    item.save()
sys.stdout.write("\n")

