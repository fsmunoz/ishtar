#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2010-2016  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

import json

from django.core.urlresolvers import reverse
from django.db.models import Q
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.shortcuts import redirect
from ishtar_common.utils import ugettext_lazy as _

from archaeological_warehouse import models

from archaeological_warehouse.forms import WarehouseForm, ContainerForm, \
    ContainerFormSelection, BasePackagingForm, WarehouseFormSelection, \
    WarehouseModifyForm, SelectedDivisionFormset, WarehouseDeletionForm, \
    MainContainerFormSelection, ContainerModifyForm, LocalisationForm, \
    ContainerDeletionForm, ContainerSelect, WarehouseSelect, \
    MainContainerFormMultiSelection, WarehouseFormMultiSelection
from ishtar_common.forms import FinalForm

from ishtar_common.views import QABaseLockView, wizard_is_available
from ishtar_common.views_item import get_item, show_item, new_qa_item
from archaeological_finds.views import treatment_add

from archaeological_warehouse.wizards import PackagingWizard, WarehouseSearch, \
    WarehouseWizard, WarehouseModificationWizard, WarehouseDeletionWizard, \
    ContainerSearch, ContainerWizard, ContainerModificationWizard, \
    ContainerDeletionWizard

from ishtar_common.utils import put_session_message

get_container = get_item(models.Container, 'get_container', 'container',
                         search_form=ContainerSelect)
show_container = show_item(models.Container, 'container')

get_warehouse = get_item(models.Warehouse, 'get_warehouse', 'warehouse',
                         search_form=WarehouseSelect)
show_warehouse = show_item(models.Warehouse, 'warehouse')

new_warehouse = new_qa_item(models.Warehouse, WarehouseForm)
new_container = new_qa_item(models.Container, ContainerForm)


def autocomplete_warehouse(request):
    if not request.user.has_perm('ishtar_common.view_warehouse',
                                 models.Warehouse)\
       and not request.user.has_perm(
            'ishtar_common.view_own_warehouse', models.Warehouse):
        return HttpResponse(content_type='text/plain')
    if not request.GET.get('term'):
        return HttpResponse(content_type='text/plain')
    q = request.GET.get('term')
    query = Q()
    for q in q.split(' '):
        extra = Q(name__icontains=q) | \
            Q(warehouse_type__label__icontains=q)
        query = query & extra
    limit = 15
    warehouses = models.Warehouse.objects.filter(query)[:limit]
    data = json.dumps([{'id': warehouse.pk, 'value': str(warehouse)}
                       for warehouse in warehouses])
    return HttpResponse(data, content_type='text/plain')


def autocomplete_container(request):
    if not request.user.has_perm('ishtar_common.view_warehouse',
                                 models.Warehouse)\
       and not request.user.has_perm(
            'ishtar_common.view_own_warehouse', models.Warehouse):
        return HttpResponse(content_type='text/plain')
    if not request.GET.get('term'):
        return HttpResponse(content_type='text/plain')
    term = request.GET.get('term')
    limit = 15
    query = Q()
    for q in term.split(' '):
        extra = Q(reference__iexact=q)
        query = query & extra
    containers = list(models.Container.objects.filter(
        query).values('id', 'cached_label')[:limit])
    limit = 15 - len(containers)
    if limit > 0:
        query = Q()
        for q in term.split(' '):
            extra = Q(container_type__label__icontains=q) | \
                Q(container_type__reference__icontains=q) | \
                Q(reference__icontains=q) | \
                Q(location__name=q) | \
                Q(location__town=q)
            query = query & extra
        containers += list(
            models.Container.objects.filter(query).exclude(
                pk__in=[c['id'] for c in containers]
            ).values('id', 'cached_label')[:limit])
    data = json.dumps(
        [{'id': container['id'], 'value': container['cached_label']}
         for container in containers])
    return HttpResponse(data, content_type='text/plain')

warehouse_packaging_wizard = PackagingWizard.as_view([  # AFAC
    ('seleccontainer-packaging', ContainerFormSelection),
    ('base-packaging', BasePackagingForm),
    ('final-packaging', FinalForm)],
    label=_(u"Packaging"),
    url_name='warehouse_packaging',)

warehouse_search_wizard = WarehouseSearch.as_view([
    ('selec-warehouse_search', WarehouseFormSelection)],
    label=_(u"Warehouse search"),
    url_name='warehouse_search',
)

warehouse_creation_steps = [
    ("warehouse-warehouse_creation", WarehouseForm),
    ('divisions-warehouse_creation', SelectedDivisionFormset),
    ('final-warehouse_creation', FinalForm)]


warehouse_creation_wizard = WarehouseWizard.as_view(
    warehouse_creation_steps,
    label=_(u"Warehouse creation"),
    url_name='warehouse_creation',
)

warehouse_modification_wizard = WarehouseModificationWizard.as_view([
    ('selec-warehouse_modification', WarehouseFormSelection),
    ("warehouse-warehouse_modification", WarehouseModifyForm),
    ('divisions-warehouse_modification', SelectedDivisionFormset),
    ('final-warehouse_modification', FinalForm)],
    label=_(u"Warehouse modification"),
    url_name='warehouse_modification',
)


def warehouse_modify(request, pk):
    if not wizard_is_available(warehouse_modification_wizard, request,
                               models.Warehouse, pk):
        return HttpResponseRedirect("/")
    wizard_url = 'warehouse_modification'
    WarehouseModificationWizard.session_set_value(
        request, 'selec-' + wizard_url, 'pk', pk, reset=True)
    return redirect(
        reverse(wizard_url, kwargs={'step': 'warehouse-' + wizard_url}))


warehouse_deletion_wizard = WarehouseDeletionWizard.as_view([
    ('selec-warehouse_deletion', WarehouseFormMultiSelection),
    ('final-warehouse_deletion', WarehouseDeletionForm)],
    label=_(u"Warehouse deletion"),
    url_name='warehouse_deletion',)


def warehouse_delete(request, pk):
    if not wizard_is_available(warehouse_deletion_wizard, request,
                               models.Warehouse, pk):
        return HttpResponseRedirect("/")
    wizard_url = 'warehouse_deletion'
    WarehouseDeletionWizard.session_set_value(
        request, 'selec-' + wizard_url, 'pks', pk, reset=True)
    return redirect(
        reverse(wizard_url, kwargs={'step': 'final-' + wizard_url}))


class QAWarehouseLockView(QABaseLockView):
    model = models.Warehouse
    base_url = "warehouse-qa-lock"


container_search_wizard = ContainerSearch.as_view([
    ('selec-container_search', MainContainerFormSelection)],
    label=_(u"Container search"),
    url_name='container_search',
)

container_creation_steps = [
    ('container-container_creation', ContainerForm),
    ('localisation-container_creation', LocalisationForm),
    ('final-container_creation', FinalForm)]

container_creation_wizard = ContainerWizard.as_view(
    container_creation_steps,
    label=_(u"Container creation"),
    url_name='container_creation',
)

container_modification_wizard = ContainerModificationWizard.as_view([
    ('selec-container_modification', MainContainerFormSelection),
    ('container-container_modification', ContainerModifyForm),
    ('localisation-container_modification', LocalisationForm),
    ('final-container_modification', FinalForm)],
    label=_(u"Container modification"),
    url_name='container_modification',
)


def container_modify(request, pk):
    if not wizard_is_available(container_modification_wizard, request,
                               models.Container, pk):
        return HttpResponseRedirect("/")
    wizard_url = 'container_modification'
    ContainerModificationWizard.session_set_value(
        request, 'selec-' + wizard_url, 'pk', pk, reset=True)
    return redirect(
        reverse(wizard_url, kwargs={'step': 'container-' + wizard_url}))


container_deletion_wizard = ContainerDeletionWizard.as_view([
    ('selec-container_deletion', MainContainerFormMultiSelection),
    ('final-container_deletion', ContainerDeletionForm)],
    label=_(u"Container deletion"),
    url_name='container_deletion',)


def container_delete(request, pk):
    if not wizard_is_available(container_deletion_wizard, request,
                               models.Container, pk):
        return HttpResponseRedirect("/")
    wizard_url = 'container_deletion'
    ContainerDeletionWizard.session_set_value(
        request, 'selec-' + wizard_url, 'pks', pk, reset=True)
    return redirect(
        reverse(wizard_url, kwargs={'step': 'final-' + wizard_url}))


def container_treatment_add(request, pk, current_right=None):
    try:
        container = models.Container.objects.get(pk=pk)
    except models.Container.DoesNotExist:
        raise Http404()
    return treatment_add(
        request, ",".join([str(f.pk) for f in container.finds.all()]))

"""
warehouse_packaging_wizard = ItemSourceWizard.as_view([
         ('selec-warehouse_packaging', ItemsSelection),
         ('final-warehouse_packaging', FinalForm)],
          url_name='warehouse_packaging',)
"""


class QAContainerLockView(QABaseLockView):
    model = models.Container
    base_url = "container-qa-lock"


def reset_wizards(request):
    for wizard_class, url_name in (
            (PackagingWizard, 'warehouse_packaging'),
            (WarehouseWizard, 'warehouse_creation'),
            (WarehouseModificationWizard, 'warehouse_modification'),
            (WarehouseDeletionWizard, 'warehouse_deletion'),
            (ContainerWizard, 'container_creation'),
            (ContainerModificationWizard, 'container_modification'),
            (ContainerDeletionWizard, 'container_deletion'),
    ):
        wizard_class.session_reset(request, url_name)
