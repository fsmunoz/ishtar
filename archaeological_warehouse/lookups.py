from ajax_select import register

from ishtar_common.lookups import LookupChannel

from django.db.models import Q
from django.utils.encoding import force_text
from django.utils.html import escape

from archaeological_warehouse import models


@register('container')
class ContainerLookup(LookupChannel):
    model = models.Container

    def get_query(self, q, request):
        query = Q()
        for term in q.strip().split(' '):
            subquery = (
                Q(reference__icontains=term) |
                Q(container_type__label__icontains=term) |
                Q(cached_label__icontains=term) |
                Q(responsible__name__icontains=term)
            )
            query &= subquery
        return self.model.objects.filter(query).order_by(
            'cached_label')[:20]

    def format_match(self, obj):
        return escape(force_text(obj.cached_label))

    def format_item_display(self, item):
        return u"<span class='ajax-label'>%s</span>" % item.cached_label


@register('warehouse')
class WarehouseLookup(LookupChannel):
    model = models.Warehouse

    def get_query(self, q, request):
        query = Q()
        for term in q.strip().split(' '):
            subquery = (
                Q(warehouse_type__label=term) |
                Q(name__icontains=term)
            )
            query &= subquery
        return self.model.objects.filter(query).order_by('name')[:20]

    def format_item_display(self, item):
        return u"<span class='ajax-label'>%s</span>" % item.name
