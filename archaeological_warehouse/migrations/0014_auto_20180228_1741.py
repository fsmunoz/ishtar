# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-02-28 17:41
from __future__ import unicode_literals

from django.db import migrations, models
import virtualtime


class Migration(migrations.Migration):

    dependencies = [
        ('archaeological_warehouse', '0013_auto_20180131_1551'),
    ]

    operations = [
        migrations.AlterField(
            model_name='collection',
            name='history_date',
            field=models.DateTimeField(default=virtualtime.datetime.now),
        ),
        migrations.AlterField(
            model_name='container',
            name='history_date',
            field=models.DateTimeField(default=virtualtime.datetime.now),
        ),
    ]
