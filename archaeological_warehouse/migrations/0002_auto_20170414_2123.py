# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('archaeological_warehouse', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('ishtar_common', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='warehouse',
            name='imports',
            field=models.ManyToManyField(related_name='imported_archaeological_warehouse_warehouse', to='ishtar_common.Import', blank=True),
        ),
        migrations.AddField(
            model_name='warehouse',
            name='person_in_charge',
            field=models.ForeignKey(related_name='warehouse_in_charge', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Person in charge', blank=True, to='ishtar_common.Person', null=True),
        ),
        migrations.AddField(
            model_name='warehouse',
            name='warehouse_type',
            field=models.ForeignKey(verbose_name='Warehouse type', to='archaeological_warehouse.WarehouseType'),
        ),
        migrations.AddField(
            model_name='containerlocalisation',
            name='container',
            field=models.ForeignKey(related_name='division', verbose_name='Container', to='archaeological_warehouse.Container'),
        ),
        migrations.AddField(
            model_name='containerlocalisation',
            name='division',
            field=models.ForeignKey(verbose_name='Division', to='archaeological_warehouse.WarehouseDivisionLink'),
        ),
        migrations.AddField(
            model_name='container',
            name='container_type',
            field=models.ForeignKey(verbose_name='Container type', to='archaeological_warehouse.ContainerType'),
        ),
        migrations.AddField(
            model_name='container',
            name='history_creator',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Creator', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='container',
            name='history_modifier',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Last editor', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='container',
            name='imports',
            field=models.ManyToManyField(related_name='imported_archaeological_warehouse_container', to='ishtar_common.Import', blank=True),
        ),
        migrations.AddField(
            model_name='container',
            name='location',
            field=models.ForeignKey(related_name='containers', verbose_name='Location (warehouse)', to='archaeological_warehouse.Warehouse'),
        ),
        migrations.AddField(
            model_name='container',
            name='responsible',
            field=models.ForeignKey(related_name='owned_containers', verbose_name='Responsible warehouse', to='archaeological_warehouse.Warehouse'),
        ),
        migrations.AddField(
            model_name='collection',
            name='history_creator',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Creator', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='collection',
            name='history_modifier',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Last editor', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='collection',
            name='imports',
            field=models.ManyToManyField(related_name='imported_archaeological_warehouse_collection', to='ishtar_common.Import', blank=True),
        ),
        migrations.AddField(
            model_name='collection',
            name='warehouse',
            field=models.ForeignKey(related_name='collections', verbose_name='Warehouse', to='archaeological_warehouse.Warehouse'),
        ),
        migrations.AlterUniqueTogether(
            name='warehousedivisionlink',
            unique_together=set([('warehouse', 'division')]),
        ),
        migrations.AlterUniqueTogether(
            name='containerlocalisation',
            unique_together=set([('container', 'division')]),
        ),
        migrations.AlterUniqueTogether(
            name='container',
            unique_together=set([('index', 'location')]),
        ),
    ]
