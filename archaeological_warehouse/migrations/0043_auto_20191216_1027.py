# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2019-12-16 10:27
from __future__ import unicode_literals

import django.contrib.postgres.indexes
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ishtar_common', '0116_create_gist_extension'),
        ('archaeological_warehouse', '0042_auto_20191216_1014'),
    ]

    operations = [
        migrations.AddIndex(
            model_name='warehouse',
            index=django.contrib.postgres.indexes.GinIndex(fields=['data'], name='archaeologi_data_49b6ad_gin'),
        ),
    ]
