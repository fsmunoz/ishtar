# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import django.core.validators
import django.db.models.deletion
from django.conf import settings
import re
import ishtar_common.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Collection',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('history_date', models.DateTimeField(default=datetime.datetime.now)),
                ('name', models.CharField(max_length=200, null=True, verbose_name='Name', blank=True)),
                ('description', models.TextField(null=True, verbose_name='Description', blank=True)),
            ],
            options={
                'ordering': ('name',),
                'verbose_name': 'Collection',
                'verbose_name_plural': 'Collection',
            },
        ),
        migrations.CreateModel(
            name='Container',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(max_length=255, null=True, upload_to=b'upload/', blank=True)),
                ('thumbnail', models.ImageField(max_length=255, null=True, upload_to=b'upload/thumbs/', blank=True)),
                ('history_date', models.DateTimeField(default=datetime.datetime.now)),
                ('reference', models.CharField(max_length=40, verbose_name='Container ref.')),
                ('comment', models.TextField(null=True, verbose_name='Comment', blank=True)),
                ('cached_label', models.CharField(max_length=500, null=True, verbose_name='Localisation', blank=True)),
                ('cached_location', models.CharField(max_length=500, null=True, verbose_name='Cached location', blank=True)),
                ('index', models.IntegerField(default=0, verbose_name='ID')),
                ('external_id', models.TextField(null=True, verbose_name='External ID', blank=True)),
                ('auto_external_id', models.BooleanField(default=False, verbose_name='External ID is set automatically')),
            ],
            options={
                'ordering': ('cached_label',),
                'verbose_name': 'Container',
                'verbose_name_plural': 'Containers',
            },
        ),
        migrations.CreateModel(
            name='ContainerLocalisation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('reference', models.CharField(default=b'', max_length=200, verbose_name='Reference')),
            ],
            options={
                'ordering': ('container', 'division__order'),
                'verbose_name': 'Container localisation',
                'verbose_name_plural': 'Container localisations',
            },
        ),
        migrations.CreateModel(
            name='ContainerType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label', models.CharField(max_length=100, verbose_name='Label')),
                ('txt_idx', models.CharField(unique=True, max_length=100, verbose_name='Textual ID', validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+$'), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')])),
                ('comment', models.TextField(null=True, verbose_name='Comment', blank=True)),
                ('available', models.BooleanField(default=True, verbose_name='Available')),
                ('length', models.IntegerField(null=True, verbose_name='Length (mm)', blank=True)),
                ('width', models.IntegerField(null=True, verbose_name='Width (mm)', blank=True)),
                ('height', models.IntegerField(null=True, verbose_name='Height (mm)', blank=True)),
                ('volume', models.FloatField(null=True, verbose_name='Volume (l)', blank=True)),
                ('reference', models.CharField(max_length=30, verbose_name='Ref.')),
            ],
            options={
                'ordering': ('label',),
                'verbose_name': 'Container type',
                'verbose_name_plural': 'Container types',
            },
            bases=(ishtar_common.models.Cached, models.Model),
        ),
        migrations.CreateModel(
            name='Warehouse',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('address', models.TextField(null=True, verbose_name='Address', blank=True)),
                ('address_complement', models.TextField(null=True, verbose_name='Address complement', blank=True)),
                ('postal_code', models.CharField(max_length=10, null=True, verbose_name='Postal code', blank=True)),
                ('town', models.CharField(max_length=70, null=True, verbose_name='Town', blank=True)),
                ('country', models.CharField(max_length=30, null=True, verbose_name='Country', blank=True)),
                ('alt_address', models.TextField(null=True, verbose_name='Other address: address', blank=True)),
                ('alt_address_complement', models.TextField(null=True, verbose_name='Other address: address complement', blank=True)),
                ('alt_postal_code', models.CharField(max_length=10, null=True, verbose_name='Other address: postal code', blank=True)),
                ('alt_town', models.CharField(max_length=70, null=True, verbose_name='Other address: town', blank=True)),
                ('alt_country', models.CharField(max_length=30, null=True, verbose_name='Other address: country', blank=True)),
                ('phone', models.CharField(max_length=18, null=True, verbose_name='Phone', blank=True)),
                ('phone_desc', models.CharField(max_length=300, null=True, verbose_name='Phone description', blank=True)),
                ('phone2', models.CharField(max_length=18, null=True, verbose_name='Phone description 2', blank=True)),
                ('phone_desc2', models.CharField(max_length=300, null=True, verbose_name='Phone description 2', blank=True)),
                ('phone3', models.CharField(max_length=18, null=True, verbose_name='Phone 3', blank=True)),
                ('phone_desc3', models.CharField(max_length=300, null=True, verbose_name='Phone description 3', blank=True)),
                ('raw_phone', models.TextField(null=True, verbose_name='Raw phone', blank=True)),
                ('mobile_phone', models.CharField(max_length=18, null=True, verbose_name='Mobile phone', blank=True)),
                ('email', models.EmailField(max_length=300, null=True, verbose_name='Email', blank=True)),
                ('alt_address_is_prefered', models.BooleanField(default=False, verbose_name='Alternative address is prefered')),
                ('name', models.CharField(max_length=200, verbose_name='Name')),
                ('comment', models.TextField(null=True, verbose_name='Comment', blank=True)),
                ('external_id', models.TextField(null=True, verbose_name='External ID', blank=True)),
                ('auto_external_id', models.BooleanField(default=False, verbose_name='External ID is set automatically')),
            ],
            options={
                'verbose_name': 'Warehouse',
                'verbose_name_plural': 'Warehouses',
                'permissions': (('view_warehouse', 'Peut voir tous les D\xe9p\xf4ts'), ('view_own_warehouse', 'Peut voir son propre D\xe9p\xf4t'), ('add_own_warehouse', 'Peut ajouter son propre D\xe9p\xf4t'), ('change_own_warehouse', 'Peut modifier son propre D\xe9p\xf4t'), ('delete_own_warehouse', 'Peut supprimer son propre D\xe9p\xf4t')),
            },
            bases=(models.Model, ishtar_common.models.DashboardFormItem, ishtar_common.models.OwnPerms),
        ),
        migrations.CreateModel(
            name='WarehouseDivision',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label', models.CharField(max_length=100, verbose_name='Label')),
                ('txt_idx', models.CharField(unique=True, max_length=100, verbose_name='Textual ID', validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+$'), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')])),
                ('comment', models.TextField(null=True, verbose_name='Comment', blank=True)),
                ('available', models.BooleanField(default=True, verbose_name='Available')),
            ],
            options={
                'verbose_name': 'Warehouse division type',
                'verbose_name_plural': 'Warehouse division types',
            },
            bases=(ishtar_common.models.Cached, models.Model),
        ),
        migrations.CreateModel(
            name='WarehouseDivisionLink',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order', models.IntegerField(default=10, verbose_name='Order')),
                ('division', models.ForeignKey(to='archaeological_warehouse.WarehouseDivision')),
                ('warehouse', models.ForeignKey(to='archaeological_warehouse.Warehouse')),
            ],
            options={
                'ordering': ('warehouse', 'order'),
            },
        ),
        migrations.CreateModel(
            name='WarehouseType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label', models.CharField(max_length=100, verbose_name='Label')),
                ('txt_idx', models.CharField(unique=True, max_length=100, verbose_name='Textual ID', validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+$'), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')])),
                ('comment', models.TextField(null=True, verbose_name='Comment', blank=True)),
                ('available', models.BooleanField(default=True, verbose_name='Available')),
            ],
            options={
                'ordering': ('label',),
                'verbose_name': 'Warehouse type',
                'verbose_name_plural': 'Warehouse types',
            },
            bases=(ishtar_common.models.Cached, models.Model),
        ),
        migrations.AddField(
            model_name='warehouse',
            name='associated_divisions',
            field=models.ManyToManyField(to='archaeological_warehouse.WarehouseDivision', verbose_name='Divisions', through='archaeological_warehouse.WarehouseDivisionLink', blank=True),
        ),
        migrations.AddField(
            model_name='warehouse',
            name='history_creator',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Creator', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='warehouse',
            name='history_modifier',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Last editor', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
