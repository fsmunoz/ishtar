# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('archaeological_warehouse', '0002_auto_20170414_2123'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='warehouse',
            options={'verbose_name': 'Warehouse', 'verbose_name_plural': 'Warehouses', 'permissions': (('view_warehouse', 'Can view all Warehouses'), ('view_own_warehouse', 'Can view own Warehouse'), ('add_own_warehouse', 'Can add own Warehouse'), ('change_own_warehouse', 'Can change own Warehouse'), ('delete_own_warehouse', 'Can delete own Warehouse'))},
        ),
    ]
