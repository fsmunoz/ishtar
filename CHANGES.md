Ishtar changelog
================

v2.1.0 - dev
------------

### Features ###
- manage delayed imports with a background task manager
- parcel is no more mandatory for context records


v2.0.0 (2017-03-01)
-------------------

For release for v2. Many great improvements and bug fixes.

### Main features ###
- migration to Django 1.11
- full rework on the UI (using bootstrap)
- update / change many JS libraries (particularly DataTables for tables and lightgallery for photos)
- real full-txt search (postgres search field) with unaccented search management
- JSON fields (import and display in card)
- new library for managing OpenDocument templates (secretary) - migration script - image management
- conservation module
- underwater module
- site module
- manage relation between towns (particularly to manage INSEE code)
- imports: target key can be associated to current import, current user, current group or to all users

v1.0.0 (2017-09-21)
-------------------

First release!

### Bug fixes ###
- Fix pined search

v0.99.27 (2017-09-09)
---------------------
### Features ###
- Find deletions: delete associated base find
- Finds: delete associated treatment on find deletion
- Sheet: add a specific sheet_address_section
- Find forms: add discovery date
- Context record form/sheet: add opening/closing dates

### Bug fixes ###
- Data importer: allow many split for UnicodeFormater
- Year limit is now 1000
- Fix ParcelField compress method

v0.99.26 (2017-07-26)
---------------------
### Features ###
- Import: add FieldDoesNotExist to possible error on imports
- Add reference documents
- Update fixtures

### Bug fixes ###
- Datings: post-fix of duplicates associated to the same object (usually on imports)
- Tests: fix fixtures management

v0.99.25 (2017-07-20)
---------------------
### Features ###
- Context record sheet: add documentation field

### Bug fixes ###
- Imports: fix skip of lines when import file is not utf-8
- Fix display view

v0.99.24 (2017-07-12)
---------------------
### Features ###
- Find deletion tests

### Bug fixes ###
- CSV Export: add documentation type to models to exports - fail silently if type is not available

v0.99.23 (2017-07-11)
---------------------
### Features ###
- Tests: add show-* tests
- Tests: improve wizard test management

### Bug fixes ###
- Fix find deletion when warehouse module is activated
- Archaeologivcal files: safely manage is_preventive test if preventive type is not created - show-file test
- Account : fix form initialization when raw name is empty

v0.99.22 (2017-06-30)
---------------------
### Features ###
- Imports: explicit message (instead of crash) on misconfiguration of importers
- Sources: change external id type to TextField
- Sheet find: hover text on ID

### Bug fixes ###
- Sources wizard: fix author deletion
- Fix container export - CSV

v0.99.21 (2017-06-14)
---------------------
### Features ###
- OA code is now a string

### Bug fixes ###
- Context record wizard: fix related context record association on creation

v0.99.20 (2017-06-01)
---------------------
### Features ###
- Add the ability to scheduled administration task inside Django admin
- Alert when PROJECT_SLUG or profile slug is set to "default"
- Operation wizard: collaborators in a specific panel
- Autocomplete: on no result return an empty list instead of an empty string

### Bug fixes ###
- Finds: fix full CSV export columns

v0.99.19 (2017-05-16)
---------------------
### Features ###
- Can manage find index by context record
- Sheet find: put a warning when displaying a find with attached downstream treatment
- Find search: search by context records (and within relations)
- Scripts to manage access control import and export
- Add an access control fixture (specific format for ishtar)
- Treament file dashboard: add filter form
- Treament dashboard: add filter form

### Bug fixes ###
- Treatment form: remove "target_is_basket" field in modification form
- Float field: fix clean with native float
- Treatment form: reordering of some fields
- Access control: fix show own item (not *all* items)
- get_item: refactoring of access control check

v0.99.18 (2017-04-11)
---------------------

### Features ###
- Update fixtures
- CSS: update input width
- Manage hierarchical display with select2
- Fix hiearchical display of types

v0.99.17 (2017-04-07)
---------------------

### Features ###
- Operations: add collaborators field
- Operation wizard: an operation cannot be related to herself
- Operation forms: filter some fields if "files" module is not activated
- Replace MultipleAutocompleteField with Select2MultipleField
- JS: new version of select2
- Base finds: bulk update of finds on context record change
- Warehouse sheet: provide statistics
- Sheets: larger tables
- Treatment - Treatment files dashboard: first overview
- Documentation generation: add some variables to treatments
- Shortcut menu: hide/show information saved in the session
- Allow old operations

### Bug fixes ###
- Many fixes on access controls and permissions
- UI: use the url to detect the current action (more accurate)
- UI: prevent menu from dancing (less cool but prevent seasickness)
- Warehouse: divisions forms is readonly if containers are localised in this warehouse
- Warehouse form: name can be up to 200 chars
- Base finds: fix cache generation on context record change
- Fix find modify action
- Finds: do not resize the image and regenerate a thumb on find duplication
- New person form: fix empty title
- Filter autocomplete by own if relevant
- Sources: fix query owns requests
- Widget: fix Select2Multiple initialization with string

v0.99.16 (2017-03-21)
---------------------

### Features ###
- Operation: add a location field
- Operation sheet: add list of containers
- Operation sheet: add INSEE code to towns
- Operation source table: change headers
- Context record: add type of excavation technics
- Context record form/sheet: add documentation types, diameter and depth of appearance
- Context records table: add new fields and reorder
- Find table: add new fields and reorder
- Find search: conditionnal search (warehouse module available) for warehouses and containers
- Find search: search by warehouse (location & responsible)
- Find sheet: link for documents related to finds
- Warehouse form: can add a warehouse responsible if not available
- Warehouse/Container sheet: add a Modify button
- Container: add an image
- Container wizard: display container sheet at the end of the process
- Treatment/Treatment file history: navigation between version and restore action
- Treatment/Treatment file sheet: add source list, administrative act list
- Basket: add a close button
- Sources: ordering author by author type
- Person sheet: link to the attached organization
- Sheets: remove ":" from labels
- Sheets: mark explicitly some fields as translated fields
- Sheets: improve layout
- Sheets: add ishtar ID
- Tables: add missing headers
- Admin: add batch types
- Imports: admin can see all imports
- Imports: add a name
- Imports: clean empty m2m
- Add a default robots.txt

### Bug fixes ###
- Refactoring: EXTRA_FULL_FIELDS_LABELS -> COL_LABELS (with merge)
- Warehouse modification: error message is displayed when putting identical division types
- Container form: fix message when no warehouse is set
- Dashboard: fix area unity and effective operation tables
- Test import of files with exotic encoding

v0.99.15 (2017-03-09)
---------------------
### Features ###
- Export: full path for urls
- Export: get header from colum importer settings
- Export: specific configuration to manage ambiguous column
- Person form: title is not required anymore
- Operation tables: adapt columns
- Person sheet: nicer layout
- Sheets: better display of creation and modification date/user
- Script: translate importer basefind to find
- Fixture: update importers

### Bug fixes ###
- Person: raw name is modified on save
- Export: allow models_finds and models_treatments settings
- Export: manage empty m2m

v0.99.14 (2017-03-06)
---------------------
### Features ###
- Operations: bulk update of base find, finds and context records
- Creation wizard: can jump to the next form with mandatory fields
- UI: explicitly remind the current pinned search above the result table
- Update translations
- Tests: create necessary view(s) on _pre_setup

### Bug fixes ###
- Fix cache operation update - Parcel associated with context records are not removed!
- Fix CSV export
- Update context record label on parcel change
- Simplify empty value for get_types (and potentially fix strange errors)

v0.99.13.1 (2017-02-20)
-----------------------
### Bug fixes ###
- Warehouse: fix warehouse creation with no divisions

v0.99.13 (2017-02-17)
--------------------
### Features ###
- Accounts: initialize new account with default username extract from raw name
- Imports:
  - Prevent creation of new items when data is empty
  - Item keys are now related to the current importer
  - Report bad configuration on importers in a cleaner way

### Bug fixes ###
- Fix password initialization on user creation
- Fix MCC operation import
- Makefile: graph model generation fix
- Finds table: fix columns

v0.99.12.2 (2017-02-10)
-----------------------
### Features ###
- Sheet find: change comment/description position

v0.99.12.1 (2017-02-10)
-----------------------
### Features ###
- Sheet find: add comment

v0.99.12 (2017-02-10)
---------------------
### Features ###
- Admin: add search_fields for archaeological_finds/materialtype search
- Admin: debug and add label for context records search
- Admin: improve importer column form
- Sheets: remove title from Created_by and add link to creator sheet
- Update translations

### Bug fixes ###
- Shortcut menu: fix when current selected find is an integer

v0.99.11 (2017-02-06)
---------------------
### Features ###
- Imports: manage model limitation (don't create items not in a whitelist)
- Harmonize CSV import and CSV exports
- Imports: multi format parse of dates
- Find sheet: display treatment states in treatment table
- Basket sheet: add basket name and basic informations
- Admin: improve search and ordering
- Update labels and translations

### Bug fixes ###
- Fix UI bug: validation button at the bottom of the page

v0.99.10 (2017-02-04)
---------------------

### Features ###
- Admin: many changes on ordering, label and table columns
- Admin: add CSV export to general types
- Admin: add basic tests for general types
- Admin: add title types, formats and support types
- Fixtures: update all fixtures from reference
- Fixtures: add spatial ref system
- Makefile: add format, title type and support type to common fixtures, adapt fixtures
- Test - warehouse: explicit error on container creation form

0.99.9 (2017-02-03)
-------------------

### Features ###
- Admin: add comment columns for general types

### Bug fixes ###
- get_item: deactivate hierarchic dependency when the parent item is a basket
- Fix find-basket for own request

0.99.8 (2017-02-02)
-------------------

### Features ###
- Add a footer with version number
- Finds: batch is now a real type
- Update french translation
- Importer configuration

### Bug fixes ###
- Change field length in DB to match importers
- update-current-item: prevent errors when session is not well initialized

0.99.7.2 (2017-01-30)
---------------------

### Bug fixes ###

- Finds: fix material col in tables

0.99.7.1 (2017-01-30)
---------------------

### Bug fixes ###

- Fix shortcut menu for not owned current items

0.99.7 (2017-01-30)
-------------------

### Features ###

- Spatial ref. system: add auth_name to SRS model
- Improve search tests

### Bug fixes ###

- Fix material type search for finds.
- Fix shortcut menu for baskets.

0.99.6 (2017-01-29)
-------------------

### Features ###

- Containers, warehouses: add external ids
- Importers: add "container type" to available types for type columns

### Bug fixes ###

- Fix performance issues for shortcut menu


0.99.5 (2017-01-24)
-------------------

- UI: short menu don't hide the main content
- Wizard: fix parcel reuse in creation

0.99.4 (2017-01-24)
-------------------

### Features ###

- New specific view to display items (available for finds)
- Add a command to clean Ishtar DB
- Import: new formater types - new fixture
- Admin: add item key

### Bug fixes ###

- Operations: fix deletion of sym-linked operations
- Parcels: on associated operation or file detachment remove the parcel
- Fix inline_formset id in template
- Imports: fix attached files imports


0.99.3 (2017-01-20)
-------------------

### Features ###

- Add a status page
- Disable cache generation when loading from fixture
- Putting color for modules
- Sheet find: limit to 15 related finds in treatment tables
- Container: volume integer -> float
- More tests...

### Bug fixes ###

- Fix treatments views
- Fix container creation
- Find form: fix period saving
- Tables: many fix on column content


0.99.2 (2017-01-12)
-------------------

### Features ###

- Common: manage spatial reference systems
- Common: add a mapping module
- Finds: add explicit x, y, z, point (2d)
- Finds: automatically update point and point_2d when changing explicit coordinates

### Bug fixes ###

- Fix treatments views on cyclic treatments (should not happen but...)
- Fix treatment views migrations


0.99.1 (2017-01-11)
-------------------

### Features ###

- Treatment: add cost fields
- Container: remove old localisation when changing warehouse
- Treatment sheet: list related operations

### Bug fixes ###

- Fix simple form for adding containers
- Treatment file select form: fix treatment file check

0.99.0 (2017-01-10)
-------------------

### Main features ###

- Management of treatments, treatments requests, containers, associated sources and administrative acts

### Features ###

- Performances improvements for tables
- Better cache key management
- Fix tests
- Shortcut menu: put research archaeological files in blue
- Finds: add thickness, point of topographic reference, MNI fields
- Explicit CSV headers

0.98.10 (2016-11-03)
--------------------

### Features ###

- Force regeneration of cached labels when add M2M or changing ids
- Use lazy model evaluation for fixed type constraints
- Imports: add label to column definitions
- Use spatialite for tests
- Change default logging conf

### Bug fixes ###

- JQueryJqGrid: by default sorting is made using ordering in meta (fixes serious
 performance issues)

0.98.9 (2016-10-23)
-------------------

### Bug fixes ###

- Fix bad regexp for "get-by-importer"

0.98.8 (2016-10-22)
-------------------

### Features ###

- Better management of cached label generation:
  - cascading update management
  - relevant history saving
  - force generation on rollback
  - better performance (but can still be slow)
- Manage export with importer's templates
- Sheets: highlight main references
- Update default french fixtures
- Image filename are now up to 255 characters. Length test is done on forms.
- Fixed position for shortcut menu and top bar
- Show/hide shortcut menu
- Context record: move "has furniture" to general panel

### Bug fixes ###

- Operation: fix sort existing parcels
- Fix basket displaying in default menu
- Fix random image choice
- Makefile: fix bad fixture generation rules
- Small CSS fix

0.98.7 (2016-10-17)
-------------------

### Features ###

- Context records: display and export context records relations from operation and context record sheet

0.98.6 (2016-10-11)
-------------------

### Features ###

- Shortcut menu: add a new advanced menu with autocomplete search

### Bug fixes ###

- Operation: add a related name for towns

0.98.5 (2016-09-24)
-------------------

### Features ###

- Cache: immediately refresh cache after types modifications
- Cache: manage long keys
- Fancy display for authors

0.98.4 (2016-09-22)
-------------------

### Features ###

- Remove ISHTAR_LOCAL_PREFIX and FILE_PREFIX (now managed by the profile)

### Bug fixes ###

- Fix autocomplete in new item window
- File: fix label when no external_id is defined

0.98.3 (2016-09-15)
-------------------

### Features ###

- Operation: add comment about scientific documentation
- Finds : rename "Integrity" field into "Integrity / Interest"
- Searches: 6 to 9 visible fields by default
- Find search: add operation numeric index

### Bug fixes ###

- Dashboard operation: fix table layout

0.98.2 (2016-09-13)
-------------------

### Features ###

- Manual merge action
- Dashboard: layout improvement

0.98.1 (2016-09-11)
-------------------

### Bug fixes ###

- Fix right caching

0.98.0 (2016-09-11)
-------------------

### Features ###

- Many changes on shortcut menu:
  - AJAX loading
  - list use chosen library
  - automatic changes of dependent items when selecting on parent item
  - pin (from the sheet) and unpin items (not only our items)
  - default searches now take into account parent items
- Operation: manage deadline for finds and documentation receipt
- Imports: explicit error when conservative is check and no unicity key is defined
- cached labels for context records and finds

### Bug fixes ###

- Fix test initialization
- Operation: fix parcel display in last panel
- Operation sheet: fix stats
- Source: display sheet after creation / modification

0.97.8 (2016-09-08)
-------------------

### Features ###

- Many improvement and cleaning on sheet layout

### Bug fixes ###

- Context records: related parcel are now kept when you modify operation
- Wizards: fix file initialization when navigating between steps

0.97.7 (2016-09-06)
-------------------

### Features ###

- Context record: can change operation
- Context record: display operation code in label
- Person: add salutation field

### Bug fixes ###

- Prevent circular save on import
- Fix person creation with new title management
- Fix context records creation

0.97.6 (2016-09-05)
-------------------

### Bug fixes ###

- Fix random images rights and manage broken images

0.97.5 (2016-09-05)
-------------------

### Features ###

- Add town and parcel to find tables
- Random image on welcome page
- Add images for operation, context records and sources
- Operation: display statistics on the sheet
- Add title types to default fixture

### Bug fixes ###

- Sheets: add missing links to persons
- Source sheet: author link to persons
- Fix form initialization

0.97.4 (2016-09-02)
-------------------

### Features ###

- Non-available types are displayed when modifying old items using theses types
- Title types are no more hard-coded
- Archive item to exclude them from merge detection mechanism (python API only)
- Organization, Person sheets: add link to files
- Install: default nginx conf wait after long uwsgi response

### Bug fixes ###

- Account modify: fix bad initialization of the form
- Imports: delete town when associated import is deleted

0.97.3 (2016-08-29)
-------------------

### Features ###

- Automatically generate raw_name on save for person - formula set in profile
- UI: better simple table layout
- UI: rework on dashboard
- Autocomplete widget: larger and full content on hover
- Import: target form - better display of the key
- Update french translation
- Imports: loading indicator

### Bug fixes ###

- Cache: fix caching key for models with the same name on different app (such as Rela
tionType)
- Fix french translation

0.97.2 (2016-08-25)
-------------------

### Features ###

- Cache: update cache when saving
- Update french translation

0.97.1 (2016-08-24)
-------------------

### Features ###

- UI: new (and nicer) layout for sheet header
- Templatetag: generic header for sheet
- Imports: manage report state
- Administrative act: can modify index
- Treatments: better query for upstream and downstream, fix treatment list on find sheet

0.97.0 (2016-08-23)
-------------------

### Features ###

- Sync external ID on manual insert
- BeautifulSoup: v3 -> v4
- Account management: fix edit - can delete an account
- Person - Organization: add full export
- Person sheet: improve display of documents
- Closing operation or archaeological file: today by default
- Operation: search on "code_patriarche" and "common_name" for autocomplete
- Operation sheet: add link to scientist and to operator
- Operation: check PATRIARCHE unicity inside forms
- Archaeological sites: more explicit label
- Manage operation relation types in context record and find searches
- Parcels: add public domain field - better management of parcel formsets
- Context records: add comments on dating
- Context records: centimeters to meters for dimensions
- Finds: add creation date, creator, modification date, modifier to sheet
- Finds: add remarkability
- Finds: add estimated value. Manage a currency for an Ishtar profile
- Docs: search by operation code
- Docs: display weblink in source tables
- Docs: add search view
- Logo is now a link

### Bug fixes ###

- CSV export: manage data with inappropriate characters for encoding
- Image saving: fix deletion of image
- Archaeological files: floatfield for surface - fix error display on main form
- Operations: DRACAR to old code
- Context records: fix parcel attribution with public domain
- Context record sheet: fix head scientist of the associated operation
- Finds: fix test intitialization
- Admin: do not allow password modification on "ishtar user" + help text


0.96.3 (2016-07-18)
-------------------

### Features ###
- Versioning of CSS and JS files

### Bug fixes ###
- Many fix on dashboard

0.96.2 (2016-07-17)
-------------------

### Features ###
- Overload of columns for specific exports
- Tiny label for relations types.

0.96.1 (2016-07-16)
-------------------

### Features ###
- Test wizards
- Work on treatments (conditional wizard, tests) but kept disabled at the moment

### Bug fixes ###
- Fix fixtures for auth and groups
- Installation: better management of rights of media directories
- Administrative act: fix redirect when no documents are available


0.96 (2016-07-14)
-----------------

### Features ###
- GIS fields in database
- Sources: link to modify in sheet, better presentation of tables
- UI: font-awesome icons
- Documentation: update installation - data loading
- Finds: conservatory and dimensions comments

### Bug fixes ###
- Templatetags: fix https links, urls
- Many fix on imports:
 - year formater
 - better management of files (temporary files have a size)
 - remove File from uniqueness detection
 - forced new are created only one time
- Many fix on fixtures: relation types, report states, importer columns, labels, comments, order, accents

0.95 (2016-06-08)
-----------------

### Features ###
- A welcome page (default and to be defined in site profile)
- Displaying treatments in find sheet
- Many improvements in the find sheet

### Bug fixes ###
- Basket update after a treatment
- Fix operation code for operation with no year (or year 0)
- Fix warehouse creation
- Allow to save a find if no operation is defined
- Add PROJECT_SLUG to default installation

0.94 (2016-06-03)
-----------------

### Features ###
- First management of baskets
- Change context record of a find
- Full exports of sources

### Bug fixes ###
- Many fix in permission management

0.93 (2016-04-19)
-----------------

### Major features ###
- Many fixes in install script
- New fields for persons
- Better layout for searches

### Minor features ###
- Context records: search within relations
- Better management of archaeological sites
- Better display of sheet sources
- Update translations

### Bug fixes ###
- Allow simultanaeous creation of operations
- Fix simultanaeous creation of files
- Shortcut list: choices for finds and context records are now kept
- Parcel rearranging on operation form when file exists

0.92 (2016-02-26)
-----------------

### Major features ###
- New management of modules
- More selective merging process
- Manage relation types in searches
- Use flex layout for searches
- Update translations

### Minor features ###
- Update searches
- Add archaeological site to importers types
- Update operation fixtures
- Update translations
- Add abstract in operations
- Change archaeological sites form

### Bug fixes ###
- Fix cache for multiple instance of Ishtar
- Fix default item after creation
- Fix management of departments
- Labels and values on sheets
- Unicode error on adresses
- Fix dynamic table display in ODT and PDF

0.91 (2016-01-18)
-----------------

- First public release.

