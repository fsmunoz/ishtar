from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _


class TranslationOverload(models.Model):
    message = models.TextField(_("String"))
    context = models.CharField(_("Translation context"), max_length=256,
                               default="", blank=True)
    translation = models.TextField(_("Translation"))
    lang = models.CharField(_("Language"), choices=settings.LANGUAGES,
                            max_length=4)

    class Meta:
        verbose_name = _("Translation overload")
        verbose_name_plural = _("Translations overload")
        unique_together = ("message", "lang", "context")
