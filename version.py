# 3.0.dev.11
VERSION = (3, 0, 'dev', 11)


def get_version():
    return u'.'.join((str(num) for num in VERSION))


__version__ = get_version()
