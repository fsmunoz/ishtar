import sys
import os
import django


app_name, url = sys.argv[1], sys.argv[2]
sys.path.append(os.path.abspath('.'))
sys.path.append(os.path.abspath('./' + app_name))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", app_name + ".settings")
django.setup()

from django.contrib.sites.models import Site
from ishtar_common.models import IshtarSiteProfile

q = Site.objects.filter(domain='example.com')

if q.count():
    site = q.get()
    site.domain = url
    site.name = url
    site.save()

q = IshtarSiteProfile.objects.filter(slug='default')

if q.count():
    profile = q.get()
    profile.label = app_name
    profile.slug = app_name
    profile.save()
