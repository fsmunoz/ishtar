.. -*- coding: utf-8 -*-

============
Installation
============

:Auteur: Étienne Loks
:Date: 2018-10-02
:Copyright: CC-BY 3.0


.. warning::
   Cette documentation concerne la version 1 d'Ishtar. L'installation de la version 2 via paquet n'est pas possible pour l'instant. Un travail est en cours pour que le paquet soit mis à jour d'ici fin 2018. Pour installer la version 2 « à la main », veuillez suivre les instructions sur cette `page`_.

.. _page: https://gitlab.com/iggdrasil/ishtar/blob/develop/INSTALL.md

Ce document présente les instructions d'installation d'Ishtar.

Pour l'instant, elles se limitent à l'installation sous Debian GNU/linux via le paquet Debian.

.. note:: Sauf mention explicite, chaque commande est exécutée en tant qu'utilisateur root. Les utilisateurs de sudo l'ajouteront à chaque commande faite.

Installation sur poste Debian Wheezy
------------------------------------

Un dépôt a été mis en place pour installer sous Debian Wheezy.
Ce dépôt est signé, pour ajouter la clé du dépôt à votre gestionnaire de paquet, lancez la commande ::

    wget -O - http://deb.iggdrasil.net/contact@iggdrasil.net.gpg.key | apt-key add -

Puis, au choix, ajoutez le dépôt à votre /etc/apt/sources.list ::

    deb http://deb.iggdrasil.net/ wheezy main
    deb-src http://deb.iggdrasil.net/ wheezy main

Ou sauvegardez le fichier `iggdrasil.list`_ dans votre répertoire **/etc/apt/sources.list.d/** ::

    wget -O - http://deb.iggdrasil.net/dists/wheezy/iggdrasil.list > /etc/apt/sources.list.d/iggdrasil.list

.. _iggdrasil.list: http://deb.iggdrasil.net/dists/wheezy/iggdrasil.list

Ensuite mettez à jour la base de données de votre gestionnaire de paquet et installez le paquet ::

    apt-get update
    apt-get install python-django-ishtar

Enfin pour créer une nouvelle instance d'Ishtar ::

    INSTANCE=le_nom_de_mon_instance URL=ishtar.my-organization.net ishtar-prepare-instance

INSTANCE est le nom de l'instance et URL le nom du domaine associé (juste l'url pas de http:// ou https://).

.. note:: Le nom de domaine doit bien entendu pointer vers l'adresse IP du serveur. Si à l'issue de l'installation, le service n'est pas joignable, verifiez bien votre configuration DNS ou le cas échéant verifez bien auprès du gestionnaire de nom de domaine que c'est le cas.

D'autres variables sont disponibles :

- **UWSGI_PORT** : par défaut à 8891, faites bien attention d'incrémenter ce nombre pour chaque nouvelle instance.
- **NGINX_PORT** : si vous voulez que le serveur web réponde sur un port différent, par exemple si vous avez déjà Apache installé. Ensuite pour accéder à l'application il faudra mettre clairement le numéro de port dans l'adresse du logiciel. Exemple : http://ishtar.my-organization.net:8000
- **DB_HOST**, **DB_PORT**, **DB_PASSWORD**, **DB_NAME** : si vous souhaitez personnaliser la base de données dans laquelle sera stockée Ishtar. Par défaut, elle sera stockée localement, sur le port par défaut de PostgreSQL (5432), le mot de passe sera généré aléatoirement, le nom de la base de données sera : « ishtar-le_nom_de_mon_instance ».

.. note:: Pour le nom de l'instance, seuls sont permis le tiret bas **_** et les caractères alphanumériques en minuscule. Ce nom doit commencer par un caractères alphabétique. Évitez aussi des noms pouvant entrer en conflit avec des bibliothèques Python existantes (par exemple « test »).


.. warning::
    En terme de serveur Web, cette première version de l'installateur fonctionne avec la configuration que nous considérons comme la plus optimisée qui est le couple nginx / uwsgi. À terme, l'installateur prendra aussi en compte Apache. Pour l'instant, si vous avez des services tournant sous Apache, plusieurs options se présentent à vous :
 
    - faire tourner nginx sur un autre port (cf. la variable NGINX_PORT) ;
    - faire fonctionner vos autres services avec nginx (je vous laisse découvrir l'abondante documentation en ligne en cherchant « nginx + le nom de mon service ») ;
    - configurer Ishtar pour fonctionner avec Apache (référez vous à la `documentation de Django`_).

.. _`documentation de Django`: https://docs.djangoproject.com/fr/1.9/howto/deployment/wsgi/modwsgi/

L'installateur vous demandera un identifiant / mot de passe pour le compte administrateur.
Une fois l'instance préparée une base de données a été crée avec un nom du type ishtar-le_nom_de_mon_instance (ou le nom que vous avez spécifiquement donné), Ishtar est joignable à l'adresse donnée par la variable URL et les données de cette instances sont stockées dans le répertoire /var/lib/python-django-ishtar/le_nom_de_mon_instance.


Initialisation de la base de données
------------------------------------

Ishtar dispose de beaucoup de tables de paramétrage permettant d'avoir un logiciel au plus proche de vos besoins.
Remplir toutes ces tables est fastidieux, c'est pour cela que des jeux de données de base sont disponibles.
Lors de l'installation du paquet, à l'exception des communes (trop lourdes pour être inclues de base), cette initialisation est faite. Si vous avez opté pour cette installation vous pouvez passer tout de suite à l'initialisation des communes.

Initialisation des paramètres de base
*************************************

Pour charger toutes les données par défaut ::

    ISHTAR_PATH=/srv/ishtar # dépend de votre installation
    PROJECT_PATH=$ISHTAR_PATH/mon_instance
    cd $PROJECT_PATH
    for data in $(find ../ -name "initial_*-fr.json"); do
        ./manage.py loaddata $data
    done

Sinon regarder le détail de chaque fichier json et charger individuellement. Exemple ::

    cd $PROJECT_PATH
    ./manage.py loaddata ../ishtar_common/initial_importtypes-fr.json

Initialisation des communes
***************************

Une liste des communes française peut être téléchargée et chargée ::

    cd /tmp
    wget "http://ishtar-archeo.net/fixtures/initial_towns-fr.tar.bz2"
    tar xvjf initial_towns-fr.tar.bz2

    ISHTAR_PATH=/var/lib/python-django-ishtar # dépend de votre installation
                                              # ici pour le paquet Debian
    PROJECT_PATH=$ISHTAR_PATH/mon_instance
    cd $PROJECT_PATH
    ./manage.py loaddata /tmp/initial_towns-fr.json
    rm /tmp/initial_towns-fr.*

Sinon un script est mis à disposition pour charger des communes depuis des données OSM : $ISHTAR_PATH/scripts/import_towns_from_osm.py
Lisez les instructions contenu dans le fichier pour savoir comment procéder.

..
  TODO:
  .. warning:: L'installateur présume que vous avez un nom de domaine dédié pour votre installation. Si cela n'est pas le cas, reportez vous à la section de la documentation concernée.
  paramètres de settings utiles :DEFAULT_FROM_EMAIL = "robot@iggdrasil.net", SERVER_EMAIL EMAIL_HOST  EMAIL_PORT  EMAIL_HOST_USER EMAIL_HOST_PASSWORD EMAIL_USE_TLS
  installation depuis les sources

