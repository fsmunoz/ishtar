.. -*- coding: utf-8 -*-

==============================================================
Annexe 3 - Exemple de flux opérationnel : prêt pour exposition
==============================================================

:Auteur: Étienne Loks
:Date: 2018-12-04
:Copyright: CC-BY 3.0

----------------------------------

Description
===========

Le flux opérationnel « Prêt pour exposition » pourrait par exemple se décomposer suivant ces différentes étapes :

  - pré-sélection du mobilier pour l'exposition par les gestionnaires de mobilier ;
  - sélection du mobilier depuis cette pré-selection par la structure emprunteuse ;
  - édition des documents administratifs : convention de prêt, assurance ;
  - départ effectif du mobilier concerné ;
  - gestion du retour du mobilier.

Pré-requis :

  - les patrons des actes administratifs associés à la demande de prêt ont été créés,
  - la personne en charge du prêt a un compte Ishtar qui dispose au minimum :

     - de droits de lecture et modification sur le mobilier éventuellement concerné par la sélection,
     - d'un droit de création de demande de traitement,
     - d'un droit de création d'acte administratif.
  - les gestionnaires de mobilier concernés ont un compte sur Ishtar qui disposent au minimum de droits de lecture sur le mobilier éventuellement concerné par la sélection.
  - la structure emprunteuse dispose d'un compte Ishtar avec droit de lecture sur le mobilier rattaché.


Pré-sélection du mobilier pour l'exposition par les gestionnaires de mobilier
-----------------------------------------------------------------------------

1. Un des responsables de mobilier créé un panier depuis l'action rapide « Panier » sur les listes de mobilier (par exemple sur *Mobilier › Recherche*) ou depuis *Mobilier › Panier › Ajout*.
2. Il ajoute quelques éléments à ce panier via l'action rapide « Panier » ou *Mobilier › Panier › Gestion des éléments*.
3. Ce responsable partage ce panier en lecture/édition avec les autres gestionnaires de mobilier via *Mobilier › Panier › Modification*. 
4. Les autres gestionnaires peuvent de la même manière ajouter, enlever des éléments à ce panier via l'action rapide « Panier » ou *Mobilier › Panier › Gestion des éléments*.
5. Fin d'étape : tous les gestionnaires ont signifié (oralement, par courriel, etc.) que la pré-sélection actuelle convenait.


Sélection du mobilier depuis cette pré-selection par la structure emprunteuse
-----------------------------------------------------------------------------

1. Si l'on souhaite conserver cette pré-sélection avant partage et modification, elle peut être dupliquée depuis la fiche associée au panier (ouverte par exemple depuis : *Mobilier › Panier › Recherche* ou depuis l'icône « Paniers » sur une des fiches mobilier concernée par ce panier) avec l'icône « Dupliquer ».
2. Le responsable du prêt partage le panier en lecture/édition avec la structure emprunteuse via *Mobilier › Panier › Modification* et l'informe que la pré-selection est disponible (via courriel, téléphone).

.. note:: Un lien direct peut être donné à la structure emprunteuse pour quelle gère son panier, il suffit d'aller sur le panneau de gestion des éléments du panier concerné (*Mobilier › Panier › Gestion des éléments*) et de recopier l'adresse dans la barre d'adresse. La structure emprunteuse devra préalablement s'identifier avant d'accéder à ce lien.

.. warning:: Si le droit de lecture de mobilier de la structure emprunteuse est basé sur le mobilier rattaché via ce panier, enlever un élément du panier lui retire le droit de consulter cet élément. Une astuce peut être de partager le panier dupliqué : une fois en lecture seule, une fois en lecture/édition, ainsi même si un élément est retiré du panier en lecture/édition, il reste « rattaché » à la structure emprunteuse par le panier en lecture seule. Le panier en lecture simple peut aussi recevoir des éléments disponibles pour prêt mais pas forcément retenu dans la sélection proposée.

3. La structure emprunteuse retire du panier les éléments qui ne l'intéresse pas.
4. Fin d'étape : la structure emprunteuse a fini sa sélection et confirme son intérêt pour un prêt (par courriel, ...).


Édition des documents administratifs
------------------------------------

1. Le responsable du prêt enleve le partage en modification du panier (*Mobilier › Panier › Modification*).
2. Le responsable du prêt crée une demande de traitement « Demande de prêt pour exposition » depuis *Demande de traitement › Ajout*. Il est important de bien renseigner les différents champs pour que la génération des documents se passe bien. Le panier concerné doit être associé à cette demande de traitement.
3. Le responsable du prêt crée les actes administratifs correspondant aux documents administratifs attendus, via l'icône « + acte admin. » de la fiche de demande de traitement ou via *Demande de traitement › Acte administratif › Ajout*.
4. Un document est généré pour chaque acte administratif depuis *Demande de traitement › Acte administratif › Documents*. Ceux-ci peuvent alors être transmis à la structure demandeuse.
5. Fin d'étape : les documents ont été retournés signés. Ils sont éventuellement numérisés et ajoutés en tant que documents à la demande de traitement.


Départ effectif du mobilier concerné
------------------------------------

1. Le responsable du prêt crée un traitement « Prêt » depuis la fiche de demande de traitement correspondante avec le bouton « Ajouter le traitement associé ». Un contenant correspondant au lieu d'emprunt doit être spécifié.

.. note:: Le traitement peut être ajouté depuis la fiche panier ou via *Traitement › Traitement simple - Création* mais cela demande de re-sélectionner des éléments du panier ou de la demande de traitement associée. Passer par la fiche de demande de traitement correspondante est moins source d'erreur.

2. Une alerte spécique est créée depuis le listing mobilier pour surveiller le retour du mobilier avec une chaîne de ce type : ::

        panier="Exposition Sein 2018" pret="Oui" fin-de-traitement-avant="aujourdhui+30"

3. Avec cette requête, 30 jours avant la date de retour attendue, l'alerte sera affichée.
4. Fin d'étape : le mobilier a été retourné.


Gestion du retour du mobilier
-----------------------------

1. Sur la fiche panier correspondante, sur les fiches contenants correspondants ou sur chaque fiche mobilier par mobilier, un traitement « Retour de prêt » est fait.

.. note:: Il sera possible d'accéder rapidement aux fiches mobilier ou contenant via le QR-code.

2. Depuis ces fiches, une demande de traitement « Constat d'état » pourra être ajoutée depuis le bouton « Ajouter une demande de traitement ».
