.. -*- coding: utf-8 -*-

===================
Ishtar installation
===================

:Author: Étienne Loks
:Date: 2011-10-12
:Copyright: CC-BY 3.0

This document presents the installation of Ishtar on a machine with GNU/Linux.
Instruction are given for Debian but they are easy to adapt to other distribution.

.. contents::

Requirements
------------

 - `apache <http://www.apache.org/>`_ version 2.x
 - `python <http://www.python.org/>`_ versions 2.6 or superior
 - `django <http://www.djangoproject.com/>`_ version 1.4
 - `postgresql <http://www.postgresql.org/>`_ 9.1
 - `posgis <http://postgis.refractions.net/>`_
 - `gettext <http://www.gnu.org/software/gettext/>`_
 - `psycopg2 <http://freshmeat.net/projects/psycopg/>`_
 - `python-pisa <http://pypi.python.org/pypi/pisa/>`_
 - `django-registration <https://bitbucket.org/ubernostrum/django-registration/>`_
 - `django-simple-history <https://bitbucket.org/q/django-simple-history/src>`_ version 1.0
 - `jquery <http://jquery.com/>`_
 - `jquery-ui <http://jqueryui.com/>`_
 - `xhtml2odt <http://xhtml2odt.org/>`_
 - `python-tidylib <http://countergram.com/open-source/pytidylib/docs/>`_
 - `python-lxml <http://lxml.de/>`_
 - `python-imaging <http://www.pythonware.com/products/pil/>`_

Optionaly:

 - curl (to fetch towns from OSM)


The easier way to obtain most these packages is to get them from your favorite Linux distribution repositories.
For instance the packages for Debian wheezy are get with::

    apt-get install apache2 python python-django python-psycopg2 gettext \
          postgresql-9.1 postgresql-9.1-postgis libjs-jquery libjs-jquery-ui \
          python-pisa python-django-registration python-tidylib python-lxml \
          python-imaging git python-django-south python-psycopg2

If these packages do not exist in your distribution's repository, please refer to applications websites.

django-simple-history is not usually not packaged.::

    LOCAL_SRC_PATH=/usr/local/src

To install django-simple-history (git package is needed)::

    cd $LOCAL_SRC_PATH
    git clone https://github.com/treyhunner/django-simple-history.git@0fd9b8e9c6f36b0141367b502420efe92d4e21ce
    cd django-simple-history
    python setup.py install


If you want to take the bleeding edge version git is necessary. Install the appropriate package.


Database configuration
----------------------

Now that postgres is installed, you need to create a new user for your ishtar instance::

    $ su postgres
    $ createuser --echo --adduser --createdb --encrypted --pwprompt ishtar-user

Then, you have to create the database::


    $ createdb --echo --owner ishtar-user --encoding UNICODE ishtar "My Ishtar database" -T 'template_postgis'

Getting the sources
-------------------

Currently only the git version is available (in the directory you have decided to place sources - for instance /var/local is a good idea)::

    $ cd /var/local/
    $ git clone git://lysithea.proxience.com/git/ishtar.git
    $ cd ishtar
    $ git checkout prod

"prod" is here the chosen version (do "git branch" to view available branches).

Installing the sources
----------------------

In your Ishtar application directory create settings.py to fit to your configuration.
A base template is provided (settings.py.example)::

    $ cd /var/local/ishtar/ishtar/
    $ ISHTAR_DIR=`pwd`
    $ cp settings.py.example settings.py
    $ nano settings.py
    ####
    ROOT_PATH = '/var/local/ishtar/ishtar/'

    URL_PATH = ""
    JQUERY_URL = "/javascript/jquery/jquery.js"
    JQUERY_UI_URL = "/javascript/jquery-ui/"
    XHTML2ODT_PATH = ROOT_PATH + "../xhtml2odt"
    ODT_TEMPLATE = ROOT_PATH + "../static/template.odt"

    LOGIN_REDIRECT_URL = "/" + URL_PATH

    (...)
    DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'ishtar',                      # Or path to database file if using sqlite3.
        'USER': 'user',                      # Not used with sqlite3.
        'PASSWORD': 'pass',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
            'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
    }
    (...)

Compiling languages
-------------------

If your language is available in the locale directory of Ishtar, you will just need to get it compiled. Still being in the Ishtar directory, this can be done with (here, "de" stands for german. Replace it with the appropriate language code)::

    $ cd $ISHTAR_DIR
    $ django-admin compilemessages -l de

If your language is not available, feel free to create the default po files and to submit it, contributions are well appreciated. Procedure is as follows.

You first need to create the default po file (of course, replace "de" according to the language you chose to create)::


    $ django-admin makemessages -l de

There should now be a django.po file in locale/de/LC_MESSAGES. Complete it with your translation.

Now that the translation file is completed, just compile it the same way you would have if the language file was already available.

Database initialisation
-----------------------

Check if you have specific initialisation files for your country (this exemple
for France: "fr")::

    $ cd $ISHTAR_DIR
    $ ls ../database/fr

If a file "initial_data.json" exists copy it to a "fixtures" directory in the ishtar application directory.::

    $ mkdir fixtures
    $ cp ../database/fr/initial_data.json fixtures


Create the appropriate tables (still being in ishtar application directory)::

    ./manage.py syncdb --migrate --noinput


Then you'll need to create an administrator account (administration can be found
at: http://where_is_ishtar/admin)::

    ./manage.py createsuperuser

Then create database views (not automatically created by the admin)::

    $ cd $ISHTAR_DIR
    $ su postgres
    $ psql -U ishtar-user ishtar -f ../database/views.sql

Then generate rights for installed form wizards.::

    ./manage.py generate_rights

If you have specific files for towns and departments (in the "database/$COUNTRY_CODE" directory),
you can now insert them in the database::

    $ cd $ISHTAR_DIR
    $ ./scripts/import_from_csv.py ../database/fr/departements-insee-2010.csv department
    $ ./scripts/import_from_csv.py ../database/fr/communes-insee-2010.csv town

Of course import departments before towns.

Now the database is set, congratulations!

Webserver configuration
-----------------------

Apache configuration with mod_wsgi
**********************************

Install mod_wsgi for apache::

    $ sudo apt-get install libapache2-mod-wsgi

Create and edit a configuration for Ishtar::

    $ sudo mkdir /var/local/ishtar/apache
    $ sudo cp /var/local/ishtar/conf/django.wsgi /var/local/ishtar/apache/django.wsgi
    $ sudo cp /var/local/ishtar/conf/apache-wsgi.conf /etc/apache2/sites-available/ishtar

Adapt the files django.wsgi (with the correct sys path) and ishtar.

To activate the website reload apache.::

    $ sudo a2ensite ishtar
    $ sudo /etc/init.d/apache2 reload

Now that you have gone through ALL this configuration procedure (which was not that hard after all) you can configure the site.



..
    NOTES À NE PAS PRENDRE EN COMPTE
    """
    INSTALL_PATH = "/home/etienne/work/ishtar/xhtml2odt"
    __init__.py
    Import towns:
    curl --location --globoff "http://openstreetmap.us/xapi/api/0.6/node[place=village|town|city][bbox=-5.53711,41.90228,8.96484,51.50874]" -o data.osm
