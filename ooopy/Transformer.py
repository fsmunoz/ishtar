#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (C) 2005-14 Dr. Ralf Schlatterbeck Open Source Consulting.
# Reichergasse 131, A-3411 Weidling.
# Web: http://www.runtux.com Email: office@runtux.com
# All rights reserved
# ****************************************************************************
#
# This library is free software; you can redistribute it and/or modify
# it under the terms of the GNU Library General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
# ****************************************************************************

# import time
# import re
# from xml.etree.ElementTree import dump, SubElement, Element, tostring
from xml.etree.ElementTree import _namespace_map
# from copy import deepcopy
from ooopy.OOoPy import autosuper  # , OOoPy
from ooopy.OOoPy import files, mimetypes, namespace_by_name
# from ooopy.Version           import VERSION


def OOo_Tag(namespace, name, mimetype):
    """
    Return combined XML tag
    """
    return "{%s}%s" % (namespace_by_name[mimetype][namespace], name)


def split_tag(tag):
    """
    Split tag into symbolic namespace and name part -- inverse
    operation of OOo_Tag.
    """
    ns, t = tag.split ('}')
    return _namespace_map [ns [1:]], t


class Transform(autosuper):
    """
    Base class for individual transforms on OOo files. An individual
    transform needs a filename variable for specifying the OOo file
    the transform should be applied to and an optional prio.
    Individual transforms are applied according to their prio
    setting, higher prio means later application of a transform.

    The filename variable must specify one of the XML files which are
    part of the OOo document (see files variable above). As
    the names imply, content.xml contains the contents of the
    document (text and ad-hoc style definitions), styles.xml contains
    the style definitions, meta.xml contains meta information like
    author, editing time, etc. and settings.xml is used to store
    OOo's settings (menu Tools->Configure).
    """
    prio = 100
    textbody_names = { mimetypes[0]: 'body', mimetypes[1]: 'text'}
    paragraph_props = {
        mimetypes[0]: 'properties',
        mimetypes[1]: 'paragraph-properties'
    }
    font_decls = {
        mimetypes[0]: 'font-decls',
        mimetypes[1]: 'font-face-decls'
    }

    def __init__(self, prio=None, transformer=None):
        if prio is not None:
            self.prio = prio
        self.transformer = None
        if transformer:
            self.register(transformer)

    def apply (self, root) :
        """ Apply myself to the element given as root """
        raise NotImplementedError('derived transforms must implement "apply"')

    def apply_all (self, trees) :
        """
        Apply myself to all the files given in trees. The variable
        trees contains a dictionary of ElementTree indexed by the
        name of the OOo File.
        The standard case is that only one file (namely
        self.filename) is used.
        """
        assert self.filename
        self.apply(trees[self.filename].getroot())

    def find_tbody(self, root) :
        """
        Find the node which really contains the text -- different
        for different OOo versions.
        """
        tbody = root
        if tbody.tag != self.textbody_tag:
            tbody = tbody.find('.//' + self.textbody_tag)
        return tbody

    def register(self, transformer) :
        """
        Registering with a transformer means being able to access
        variables stored in the tranformer by other transforms.
        Also needed for tag-computation: The transformer knows which
        version of OOo document we are processing.
        """
        self.transformer = transformer
        mt = self.mimetype = transformer.mimetype
        self.textbody_name = self.textbody_names [mt]
        self.paragraph_props = self.paragraph_props [mt]
        self.properties_tag = self.oootag('style', self.paragraph_props)
        self.textbody_tag = self.oootag('office', self.textbody_name)
        self.font_decls_tag = self.oootag('office', self.font_decls [mt])

    def oootag(self, namespace, name):
        """ Compute long tag version """
        return OOo_Tag(namespace, name, self.mimetype)

    def set(self, variable, value) :
        """ Set variable in our transformer using naming convention. """
        self.transformer [self._varname (variable)] = value

    def _varname (self, name) :
        """
        For fulfilling the naming convention of the transformer
        dictionary (every entry in this dictionary should be prefixed
        with the class name of the transform) we have this
        convenience method.
        Returns variable name prefixed with own class name.
        """
        return ":".join((self.__class__.__name__, name))


class Transformer(autosuper):
    """
    Class for applying a set of transforms to a given ooopy object.
    The transforms are applied to the specified file in priority
    order. When applying transforms we have a mechanism for
    communication of transforms. We give the transformer to the
    individual transforms as a parameter. The transforms may use the
    transformer like a dictionary for storing values and retrieving
    values left by previous transforms.
    As a naming convention each transform should use its class name
    as a prefix for storing values in the dictionary.
    """

    def __init__(self, mimetype, *tf):
        assert (mimetype in mimetypes)
        self.mimetype = mimetype
        self.transforms = {}
        for t in tf:
            self.insert(t)
        self.dictionary = {}
        self.__contains__ = self.has_key
        # 2-tuples of filename, content
        self.appendfiles = []

    def has_key(self, key):
        return key in self.dictionary.keys()

    def insert(self, transform):
        """Insert a new transform"""
        t = transform
        if t.prio not in self.transforms:
            self.transforms[t.prio] = []
        self.transforms[t.prio].append(t)
        t.register(self)

    def transform(self, ooopy):
        """
        Apply all the transforms in priority order.
        Priority order is global over all transforms.
        """
        self.trees = {}
        for f in files:
            self.trees[f] = ooopy.read(f)
        # self.dictionary = {} # clear dict when transforming another ooopy
        prios = list(self.transforms.keys())
        prios.sort()
        for p in prios:
            for t in self.transforms[p]:
                t.apply_all(self.trees)
        for e in self.trees.values():
            e.write()
        for fname, fcontent in self.appendfiles:
            e.ooopy.append_file(fname, fcontent)

    def __getitem__(self, key):
        return self.dictionary[key]

    def __setitem__(self, key, value):
        self.dictionary[key] = value
