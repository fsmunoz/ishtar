from ajax_select import register

from ishtar_common.lookups import LookupChannel

from django.db.models import Q
from archaeological_files.models import File


@register('file')
class FileLookup(LookupChannel):
    model = File

    def get_query(self, q, request):
        query = Q()
        for term in q.strip().split(' '):
            subquery = (
                Q(cached_label__icontains=term)
            )
            query &= subquery
        return self.model.objects.filter(query).order_by('cached_label')[:20]

    def format_item_display(self, item):
        return u"<span class='ajax-label'>%s</span>" % item.cached_label
