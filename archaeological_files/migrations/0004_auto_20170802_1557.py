# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('archaeological_files', '0003_views'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='file',
            options={'ordering': ('cached_label',), 'verbose_name': 'Archaeological file', 'verbose_name_plural': 'Archaeological files', 'permissions': (('view_file', 'Can view all Archaeological files'), ('view_own_file', 'Can view own Archaeological file'), ('add_own_file', 'Can add own Archaeological file'), ('change_own_file', 'Can change own Archaeological file'), ('delete_own_file', 'Can delete own Archaeological file'), ('close_file', 'Can close File'))},
        ),
    ]
