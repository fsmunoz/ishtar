# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('archaeological_files', '0004_auto_20170802_1557'),
    ]

    operations = [
        migrations.AlterField(
            model_name='file',
            name='main_town',
            field=models.ForeignKey(related_name='file_main', verbose_name='Main town', blank=True, to='ishtar_common.Town', null=True),
        ),
        migrations.AlterField(
            model_name='file',
            name='towns',
            field=models.ManyToManyField(related_name='file', verbose_name='Towns', to='ishtar_common.Town', blank=True),
        ),
    ]
