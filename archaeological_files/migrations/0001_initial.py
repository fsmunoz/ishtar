# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import archaeological_operations.models
import ishtar_common.utils
import ishtar_common.models
import re
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='FileByDepartment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'db_table': 'file_department',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='File',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('year', models.IntegerField(default=ishtar_common.utils.get_current_year, verbose_name='Year')),
                ('numeric_reference', models.IntegerField(null=True, verbose_name='Numeric reference', blank=True)),
                ('internal_reference', models.CharField(max_length=60, null=True, verbose_name='Internal reference', blank=True)),
                ('external_id', models.CharField(max_length=120, null=True, verbose_name='External ID', blank=True)),
                ('auto_external_id', models.BooleanField(default=False, verbose_name='External ID is set automatically')),
                ('name', models.TextField(null=True, verbose_name='Name', blank=True)),
                ('raw_general_contractor', models.CharField(max_length=200, null=True, verbose_name='General contractor (raw)', blank=True)),
                ('raw_town_planning_service', models.CharField(max_length=200, null=True, verbose_name='Planning service (raw)', blank=True)),
                ('permit_reference', models.TextField(null=True, verbose_name='Permit reference', blank=True)),
                ('end_date', models.DateField(null=True, verbose_name='Closing date', blank=True)),
                ('creation_date', models.DateField(default=datetime.date.today, null=True, verbose_name='Creation date', blank=True)),
                ('reception_date', models.DateField(null=True, verbose_name='Reception date', blank=True)),
                ('instruction_deadline', models.DateField(null=True, verbose_name='Instruction deadline', blank=True)),
                ('total_surface', models.FloatField(null=True, verbose_name='Total surface (m2)', blank=True)),
                ('total_developed_surface', models.FloatField(null=True, verbose_name='Total developed surface (m2)', blank=True)),
                ('locality', models.CharField(max_length=100, null=True, verbose_name='Locality', blank=True)),
                ('address', models.TextField(null=True, verbose_name='Main address', blank=True)),
                ('postal_code', models.CharField(max_length=10, null=True, verbose_name='Main address - postal code', blank=True)),
                ('comment', models.TextField(null=True, verbose_name='Comment', blank=True)),
                ('research_comment', models.TextField(null=True, verbose_name='Research archaeology comment', blank=True)),
                ('classified_area', models.NullBooleanField(verbose_name='Classified area')),
                ('protected_area', models.NullBooleanField(verbose_name='Protected area')),
                ('cira_advised', models.NullBooleanField(verbose_name='Passage en CIRA')),
                ('mh_register', models.NullBooleanField(verbose_name='Sur Monument Historique class\xe9')),
                ('mh_listing', models.NullBooleanField(verbose_name='Sur Monument Historique inscrit')),
                ('cached_label', models.TextField(null=True, verbose_name='Cached name', blank=True)),
                ('imported_line', models.TextField(null=True, verbose_name='Imported line', blank=True)),
            ],
            options={
                'ordering': ('cached_label',),
                'verbose_name': 'Archaeological file',
                'verbose_name_plural': 'Archaeological files',
                'permissions': (('view_file', 'Peut voir tous les Dossiers'), ('view_own_file', 'Peut voir son propre Dossier'), ('add_own_file', 'Peut ajouter son propre Dossier'), ('change_own_file', 'Peut modifier son propre Dossier'), ('delete_own_file', 'Peut supprimer son propre Dossier'), ('close_file', 'Peut clore un Dossier')),
            },
            bases=(archaeological_operations.models.ClosedItem, models.Model, ishtar_common.models.OwnPerms, ishtar_common.models.ValueGetter, ishtar_common.models.ShortMenuItem, ishtar_common.models.DashboardFormItem),
        ),
        migrations.CreateModel(
            name='FileType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label', models.CharField(max_length=100, verbose_name='Label')),
                ('txt_idx', models.CharField(unique=True, max_length=100, verbose_name='Textual ID', validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+$'), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')])),
                ('comment', models.TextField(null=True, verbose_name='Comment', blank=True)),
                ('available', models.BooleanField(default=True, verbose_name='Available')),
            ],
            options={
                'ordering': ('label',),
                'verbose_name': 'Archaeological file type',
                'verbose_name_plural': 'Archaeological file types',
            },
            bases=(ishtar_common.models.Cached, models.Model),
        ),
        migrations.CreateModel(
            name='HistoricalFile',
            fields=[
                ('id', models.IntegerField(verbose_name='ID', db_index=True, auto_created=True, blank=True)),
                ('year', models.IntegerField(default=ishtar_common.utils.get_current_year, verbose_name='Year')),
                ('numeric_reference', models.IntegerField(null=True, verbose_name='Numeric reference', blank=True)),
                ('internal_reference', models.CharField(max_length=60, null=True, verbose_name='Internal reference', blank=True)),
                ('external_id', models.CharField(max_length=120, null=True, verbose_name='External ID', blank=True)),
                ('auto_external_id', models.BooleanField(default=False, verbose_name='External ID is set automatically')),
                ('name', models.TextField(null=True, verbose_name='Name', blank=True)),
                ('raw_general_contractor', models.CharField(max_length=200, null=True, verbose_name='General contractor (raw)', blank=True)),
                ('raw_town_planning_service', models.CharField(max_length=200, null=True, verbose_name='Planning service (raw)', blank=True)),
                ('permit_reference', models.TextField(null=True, verbose_name='Permit reference', blank=True)),
                ('end_date', models.DateField(null=True, verbose_name='Closing date', blank=True)),
                ('creation_date', models.DateField(default=datetime.date.today, null=True, verbose_name='Creation date', blank=True)),
                ('reception_date', models.DateField(null=True, verbose_name='Reception date', blank=True)),
                ('instruction_deadline', models.DateField(null=True, verbose_name='Instruction deadline', blank=True)),
                ('total_surface', models.FloatField(null=True, verbose_name='Total surface (m2)', blank=True)),
                ('total_developed_surface', models.FloatField(null=True, verbose_name='Total developed surface (m2)', blank=True)),
                ('locality', models.CharField(max_length=100, null=True, verbose_name='Locality', blank=True)),
                ('address', models.TextField(null=True, verbose_name='Main address', blank=True)),
                ('postal_code', models.CharField(max_length=10, null=True, verbose_name='Main address - postal code', blank=True)),
                ('comment', models.TextField(null=True, verbose_name='Comment', blank=True)),
                ('research_comment', models.TextField(null=True, verbose_name='Research archaeology comment', blank=True)),
                ('classified_area', models.NullBooleanField(verbose_name='Classified area')),
                ('protected_area', models.NullBooleanField(verbose_name='Protected area')),
                ('cira_advised', models.NullBooleanField(verbose_name='Passage en CIRA')),
                ('mh_register', models.NullBooleanField(verbose_name='Sur Monument Historique class\xe9')),
                ('mh_listing', models.NullBooleanField(verbose_name='Sur Monument Historique inscrit')),
                ('cached_label', models.TextField(null=True, verbose_name='Cached name', blank=True)),
                ('imported_line', models.TextField(null=True, verbose_name='Imported line', blank=True)),
                ('history_id', models.AutoField(serialize=False, primary_key=True)),
                ('history_date', models.DateTimeField()),
                ('history_type', models.CharField(max_length=1, choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')])),
            ],
            options={
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
                'verbose_name': 'historical Archaeological file',
            },
        ),
        migrations.CreateModel(
            name='PermitType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label', models.CharField(max_length=100, verbose_name='Label')),
                ('txt_idx', models.CharField(unique=True, max_length=100, verbose_name='Textual ID', validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+$'), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')])),
                ('comment', models.TextField(null=True, verbose_name='Comment', blank=True)),
                ('available', models.BooleanField(default=True, verbose_name='Available')),
            ],
            options={
                'ordering': ('label',),
                'verbose_name': 'Permit type',
                'verbose_name_plural': 'Permit types',
            },
            bases=(ishtar_common.models.Cached, models.Model),
        ),
        migrations.CreateModel(
            name='SaisineType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label', models.CharField(max_length=100, verbose_name='Label')),
                ('txt_idx', models.CharField(unique=True, max_length=100, verbose_name='Textual ID', validators=[django.core.validators.RegexValidator(re.compile('^[-a-zA-Z0-9_]+$'), "Enter a valid 'slug' consisting of letters, numbers, underscores or hyphens.", 'invalid')])),
                ('comment', models.TextField(null=True, verbose_name='Comment', blank=True)),
                ('available', models.BooleanField(default=True, verbose_name='Available')),
                ('delay', models.IntegerField(default=30, verbose_name='Delay (in days)')),
            ],
            options={
                'ordering': ('label',),
                'verbose_name': 'Type de saisine',
                'verbose_name_plural': 'Types de saisine',
            },
            bases=(ishtar_common.models.Cached, models.Model, ishtar_common.models.ValueGetter),
        ),
    ]
