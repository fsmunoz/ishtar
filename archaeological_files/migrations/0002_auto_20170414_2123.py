# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('archaeological_files', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('ishtar_common', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicalfile',
            name='corporation_general_contractor',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='ishtar_common.Organization', null=True),
        ),
        migrations.AddField(
            model_name='historicalfile',
            name='file_type',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='archaeological_files.FileType', null=True),
        ),
        migrations.AddField(
            model_name='historicalfile',
            name='general_contractor',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='ishtar_common.Person', null=True),
        ),
        migrations.AddField(
            model_name='historicalfile',
            name='history_creator',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='historicalfile',
            name='history_modifier',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='historicalfile',
            name='history_user',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='historicalfile',
            name='in_charge',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='ishtar_common.Person', null=True),
        ),
        migrations.AddField(
            model_name='historicalfile',
            name='main_town',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='ishtar_common.Town', null=True),
        ),
        migrations.AddField(
            model_name='historicalfile',
            name='organization',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='ishtar_common.Organization', null=True),
        ),
        migrations.AddField(
            model_name='historicalfile',
            name='permit_type',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='archaeological_files.PermitType', null=True),
        ),
        migrations.AddField(
            model_name='historicalfile',
            name='planning_service',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='ishtar_common.Organization', null=True),
        ),
        migrations.AddField(
            model_name='historicalfile',
            name='related_file',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='archaeological_files.File', null=True),
        ),
        migrations.AddField(
            model_name='historicalfile',
            name='requested_operation_type',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='ishtar_common.OperationType', null=True),
        ),
        migrations.AddField(
            model_name='historicalfile',
            name='responsible_town_planning_service',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='ishtar_common.Person', null=True),
        ),
        migrations.AddField(
            model_name='historicalfile',
            name='saisine_type',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='archaeological_files.SaisineType', null=True),
        ),
        migrations.AddField(
            model_name='historicalfile',
            name='scientist',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.DO_NOTHING, db_constraint=False, blank=True, to='ishtar_common.Person', null=True),
        ),
        migrations.AddField(
            model_name='file',
            name='corporation_general_contractor',
            field=models.ForeignKey(related_name='general_contractor_files', on_delete=django.db.models.deletion.SET_NULL, verbose_name='General contractor organization', blank=True, to='ishtar_common.Organization', null=True),
        ),
        migrations.AddField(
            model_name='file',
            name='departments',
            field=models.ManyToManyField(to='ishtar_common.Department', verbose_name='Departments', blank=True),
        ),
        migrations.AddField(
            model_name='file',
            name='file_type',
            field=models.ForeignKey(verbose_name='File type', to='archaeological_files.FileType'),
        ),
        migrations.AddField(
            model_name='file',
            name='general_contractor',
            field=models.ForeignKey(related_name='general_contractor_files', on_delete=django.db.models.deletion.SET_NULL, verbose_name='General contractor', blank=True, to='ishtar_common.Person', null=True),
        ),
        migrations.AddField(
            model_name='file',
            name='history_creator',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Creator', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='file',
            name='history_modifier',
            field=models.ForeignKey(related_name='+', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Last editor', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='file',
            name='imports',
            field=models.ManyToManyField(related_name='imported_archaeological_files_file', to='ishtar_common.Import', blank=True),
        ),
        migrations.AddField(
            model_name='file',
            name='in_charge',
            field=models.ForeignKey(related_name='file_responsability', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Person in charge', blank=True, to='ishtar_common.Person', null=True),
        ),
        migrations.AddField(
            model_name='file',
            name='main_town',
            field=models.ForeignKey(related_name='file_main', verbose_name='Town', blank=True, to='ishtar_common.Town', null=True),
        ),
        migrations.AddField(
            model_name='file',
            name='organization',
            field=models.ForeignKey(related_name='files', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Organization', blank=True, to='ishtar_common.Organization', null=True),
        ),
        migrations.AddField(
            model_name='file',
            name='permit_type',
            field=models.ForeignKey(verbose_name='Permit type', blank=True, to='archaeological_files.PermitType', null=True),
        ),
        migrations.AddField(
            model_name='file',
            name='planning_service',
            field=models.ForeignKey(related_name='planning_service_files', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Planning service organization', blank=True, to='ishtar_common.Organization', null=True),
        ),
        migrations.AddField(
            model_name='file',
            name='related_file',
            field=models.ForeignKey(verbose_name='Related file', blank=True, to='archaeological_files.File', null=True),
        ),
        migrations.AddField(
            model_name='file',
            name='requested_operation_type',
            field=models.ForeignKey(related_name='+', verbose_name='Requested operation type', blank=True, to='ishtar_common.OperationType', null=True),
        ),
        migrations.AddField(
            model_name='file',
            name='responsible_town_planning_service',
            field=models.ForeignKey(related_name='responsible_town_planning_service_files', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Responsible for planning service', blank=True, to='ishtar_common.Person', null=True),
        ),
        migrations.AddField(
            model_name='file',
            name='saisine_type',
            field=models.ForeignKey(verbose_name='Type de saisine', blank=True, to='archaeological_files.SaisineType', null=True),
        ),
        migrations.AddField(
            model_name='file',
            name='scientist',
            field=models.ForeignKey(related_name='scientist', on_delete=django.db.models.deletion.SET_NULL, verbose_name='Scientist in charge', blank=True, to='ishtar_common.Person', null=True),
        ),
        migrations.AddField(
            model_name='file',
            name='towns',
            field=models.ManyToManyField(related_name='file', verbose_name='Towns', to='ishtar_common.Town'),
        ),
    ]
