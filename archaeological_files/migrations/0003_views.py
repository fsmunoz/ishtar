# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from archaeological_files.models import FileByDepartment


class Migration(migrations.Migration):

    dependencies = [
        ('archaeological_files', '0002_auto_20170414_2123'),
    ]

    operations = [
        migrations.RunSQL(FileByDepartment.CREATE_SQL)
    ]
