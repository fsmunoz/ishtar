#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2012 Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from ajax_select import make_ajax_form

from django.conf import settings

from ishtar_common.apps import admin_site
from ishtar_common.admin import HistorizedObjectAdmin, GeneralTypeAdmin

from . import models


class FileAdmin(HistorizedObjectAdmin):
    list_display = ['year', 'numeric_reference', 'file_type', 'name']
    if settings.COUNTRY == 'fr':
        list_display += ['saisine_type', 'permit_reference']
    list_filter = ["file_type", "year"]
    if settings.COUNTRY == 'fr':
        list_filter += ['saisine_type']
    search_fields = ('name', 'towns__name', 'permit_reference')
    form = make_ajax_form(
        models.File, {'in_charge': 'person',
                      'general_contractor': 'person',
                      'corporation_general_contractor': 'organization',
                      'responsible_town_planning_service': 'person',
                      'planning_service': 'organization',
                      'organization': 'organization',
                      'scientist': 'person',
                      'main_town': 'town',
                      'towns': 'town',
                      'related_file': 'file'
                      })
    readonly_fields = HistorizedObjectAdmin.readonly_fields + [
        'raw_general_contractor', 'raw_town_planning_service',
        'cached_label', 'imported_line'
    ]
    model = models.File


admin_site.register(models.File, FileAdmin)


general_models = [models.FileType, models.PermitType]
if settings.COUNTRY == 'fr':
    general_models.append(models.SaisineType)
for model in general_models:
    admin_site.register(model, GeneralTypeAdmin)
